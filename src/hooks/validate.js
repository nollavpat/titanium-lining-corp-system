import ValidationError from '../utils/errors/ValidationError';

function validate(context) {
  const { type, data } = context;

  if (type !== 'before') {
    throw new Error('the \'validate\' hook should only be used as a before \'hook\'');
  }

  let valid = false;

  if (Array.isArray(data)) {
    valid = data.every(datum => datum.isValid);
  } else {
    valid = data.isValid;
  }

  if (!valid) {
    throw new ValidationError(data.errors, 'the \'data\' is not valid');
  }

  return context;
}

export default validate;

function logger(service) {
  return async function (context) {
    const {
      method,
      app,
      params,
      type,
    } = context;
    const info = type === 'before' ? 'data' : 'result';
    const logsService = app.service('api/logs');
    let prevState;

    if (method === 'patch') {
      prevState = await app.service(`api/${service}`).get(context[info]._id.valueOf());
      if (service === 'items') {
        prevState = {
          ...prevState,
          prevQuantity: context[info].quantity,
        };
      }
    } else {
      prevState = context[info];
    }

    await logsService.create({
      method,
      service,
      prevState,
      date: new Date(),
      doer: params.user.name,
    });

    return context;
  };
}

export default logger;

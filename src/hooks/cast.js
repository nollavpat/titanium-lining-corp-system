module.exports = function cast(fields) {
  return function (context) {
    const info = context.type === 'before' ? 'data' : 'result';

    let result;

    if (context[info] instanceof Array) {
      result = context[info].map((data) => {
        let obj = {};

        fields.forEach((field) => {
          obj = { ...obj, [field]: data[field] };
        });

        return obj;
      });
    } else if (context[info] instanceof Object) {
      let obj = {};

      fields.forEach((field) => {
        obj = { ...obj, [field]: context[info][field] };
      });
      result = obj;
    } else {
      return new Error('Transform Hook used wrong');
    }

    return {
      ...context,
      [info]: result,
    };
  };
};

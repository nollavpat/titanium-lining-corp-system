import AuthorizationError from '../utils/errors/AuthorizationError';

import myRoles from '../utils/roles';

const authorization = roles => (
  async function execute(context) {
    const { params, app } = context;
    const usersService = app.service('api/users');
    const { userId } = await app.passport.verifyJWT(params.accessToken);
    const loggedUser = await usersService.get(userId);

    if ((params.meta && params.meta.fromRevert) && loggedUser.role === myRoles.SUPERADMIN) {
      return context;
    }

    if (!roles.includes(loggedUser.role)) {
      throw new AuthorizationError('You are not authorize to perform this action');
    }

    return context;
  }
);

export default authorization;

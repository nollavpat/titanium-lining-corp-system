module.exports = function transform(Class) {
  return function (context) {
    const info = context.type === 'before' ? 'data' : 'result';

    let result;

    if (context[info] instanceof Array) {
      result = context[info].map(data => new Class(data));
    } else if (context[info] instanceof Object) {
      result = new Class(context[info]);
    } else {
      return new Error('Transform Hook used wrong');
    }

    return {
      ...context,
      [info]: result,
    };
  };
};

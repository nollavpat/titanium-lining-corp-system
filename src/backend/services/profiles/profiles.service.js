// Initializes the `profiles` service on path `/api/profiles`
const createService = require('feathers-mongodb');
const hooks = require('./profiles.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/profiles', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/profiles');

  mongoClient.then((db) => {
    service.Model = db.collection('profiles');
  });

  service.hooks(hooks);
};

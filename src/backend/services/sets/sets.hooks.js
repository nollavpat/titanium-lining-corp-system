import { iff } from 'feathers-hooks-common';

import cast from '../../../hooks/cast';
import logger from '../../../hooks/logger';

const { ObjectId } = require('mongodb');

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      _id: ObjectId(datum._id),
      componentsMap: datum.componentsMap.map(c => ({
        ...c,
        itemId: ObjectId(c.itemId),
      })),
    }));
  } else {
    result = {
      ...data,
      _id: ObjectId(data._id),
      componentsMap: data.componentsMap.map(c => ({
        ...c,
        itemId: ObjectId(c.itemId),
      })),
    };
  }

  return {
    ...context,
    data: result,
  };
};

const fields = [
  '_id',
  'description',
  'code',
  'componentsMap',
  'less',
];

const notFromRevert = method => (
  async function (context) {
    const { app, result } = context;
    const { _id } = result;

    const logsService = app.service('api/logs');
    const log = await logsService.find({ query: { method, 'prevState._id': _id } });

    return log.length === 0;
  }
);

const fromRevert = method => (
  async function (context) {
    const { app, result } = context;
    const { _id } = result;

    const logsService = app.service('api/logs');
    const log = await logsService.find({ query: { method, 'prevState._id': _id } });

    return log.length > 0;
  }
);

const cleanItems = async (context) => {
  const { app, data } = context;
  const itemsService = app.service('api/items');

  const componentsMap = await Promise.all(data.componentsMap.map(async (c) => {
    const item = await itemsService.find({ query: { _id: c.itemId } });

    if (item.length !== 0) {
      return c;
    }

    return null;
  }));

  const cleanSet = {
    ...data,
    componentsMap: componentsMap.filter(c => c !== null),
  };

  return {
    ...context,
    data: cleanSet,
  };
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      objectIdfy,
      cast(fields),
      iff(
        fromRevert,
        cleanItems,
      ),
    ],
    update: [
      objectIdfy,
      cast(fields),
    ],
    patch: [
      objectIdfy,
      cast(fields),
      logger('sets'),
    ],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      iff(
        notFromRevert('remove'),
        cast(fields),
        logger('sets'),
      ),
    ],
    update: [],
    patch: [],
    remove: [
      iff(
        notFromRevert('create'),
        cast(fields),
        logger('sets'),
      ),
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

// Initializes the `sets` service on path `/api/sets`
const createService = require('feathers-mongodb');
const hooks = require('./sets.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/sets', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/sets');

  mongoClient.then((db) => {
    service.Model = db.collection('sets');
  });

  service.hooks(hooks);
};

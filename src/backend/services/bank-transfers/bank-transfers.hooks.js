import { fastJoin } from 'feathers-hooks-common';
import logger from '../../../hooks/logger';

const { authenticate } = require('@feathersjs/authentication').hooks;

const bankTransferResolver = {
  joins: {
    banks: () => async (bt, context) => {
      const { app } = context;
      const constantsService = app.service('api/constants');
      const { fromId, toId } = bt;

      const from = await constantsService.get(fromId);
      const to = await constantsService.get(toId);

      /* eslint no-param-reassign: 0 */
      bt.from = from.value;
      bt.to = to.value;
    },
  },
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [
      fastJoin(bankTransferResolver),
    ],
    find: [],
    get: [],
    create: [
      logger('bankTransfers'),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

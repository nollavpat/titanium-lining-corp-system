// Initializes the `bankTransfers` service on path `/api/bankTransfers`
const createService = require('feathers-mongodb');
const hooks = require('./bank-transfers.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/bankTransfers', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/bankTransfers');

  mongoClient.then((db) => {
    service.Model = db.collection('bank-transfers');
  });

  service.hooks(hooks);
};

const dauria = require('dauria');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      function (hook) {
        if (!hook.data.uri && hook.params.file) {
          const { file } = hook.params;
          const uri = dauria.getBase64DataURI(file.buffer, file.mimetype);

          return {
            ...hook,
            data: { uri },
          };
        }

        return hook;
      },
    ],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

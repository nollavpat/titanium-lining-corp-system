const multer = require('multer');
const blobService = require('feathers-blob');
const fs = require('fs-blob-store');
const hooks = require('./uploads.hooks');

const blobStorage = fs('./dist/images');
const multipartMiddleware = multer();

module.exports = function (app) {
  app.use(
    '/api/uploads',
    multipartMiddleware.single('uri'),
    (req, res, next) => {
      req.feathers.file = req.file;
      next();
    },
    blobService({ Model: blobStorage }),
  );

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('/api/uploads');

  service.hooks(hooks);
};

import { ObjectId } from 'mongodb';
import { fastJoin } from 'feathers-hooks-common';

import logger from '../../../hooks/logger';

const { authenticate } = require('@feathersjs/authentication').hooks;


const deliveryReceiptResolver = {
  joins: {
    transactionItems: () => async (dr, context) => {
      const { app } = context;

      const newTi = await Promise.all(dr.transactionItems.map(async (ti) => {
        const itemsService = app.service('api/items');
        const setsService = app.service('api/sets');
        const service = ti.isSet ? setsService : itemsService;
        const item = await service.get(ti.transactionItemId);

        return {
          ...ti,
          ...item,
          items: ti.items,
        };
      }));
      /* eslint no-param-reassign: 0 */
      dr.transactionItems = newTi;
    },
  },
};

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      transactionId: ObjectId(datum.transactionId),
      transactionItems: datum.transactionItems.map(c => ({
        ...c,
        transactionItemId: ObjectId(c.transactionItemId),
      })),
    }));
  } else {
    result = {
      ...data,
      transactionId: ObjectId(data.transactionId),
      transactionItems: data.transactionItems.map(c => ({
        ...c,
        transactionItemId: ObjectId(c.transactionItemId),
      })),
    };
  }

  return {
    ...context,
    data: result,
  };
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      objectIdfy,
    ],
    update: [
      objectIdfy,
    ],
    patch: [
      objectIdfy,
    ],
    remove: [],
  },

  after: {
    all: [
      fastJoin(deliveryReceiptResolver),
    ],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const { transactionId, transactionItems, _id } = result;
        const transactionsService = app.service('api/transactions');
        const itemsService = app.service('api/items');
        const setsService = app.service('api/sets');

        await transactionsService.patch(
          transactionId,
          { $push: { deliveryReceiptIds: _id } },
          { meta: { fromServer: true } },
        );

        await Promise.all(transactionItems.map(async (ti) => {
          const { items, transactionItemId, isSet } = ti;

          if (isSet) {
            const set = await setsService.get(transactionItemId);
            const { componentsMap } = set;

            return Promise.all(componentsMap.map(async (cm) => {
              const { itemId, quantity } = cm;
              const reqItems = quantity * items;

              return itemsService.patch(
                itemId,
                { $inc: { quantity: -reqItems } },
                { meta: { fromServer: true } },
              );
            }));
          }

          return itemsService.patch(
            transactionItemId,
            { $inc: { quantity: -items } },
            { meta: { fromServer: true } },
          );
        }));

        return context;
      },
      logger('deliveryReceipts'),
    ],
    update: [],
    patch: [],
    remove: [
      async (context) => {
        const { result, app } = context;
        const { transactionItems } = result;
        const itemsService = app.service('api/items');
        const setsService = app.service('api/sets');

        await Promise.all(transactionItems.map(async (ti) => {
          const { items, transactionItemId, isSet } = ti;

          if (isSet) {
            const set = await setsService.get(transactionItemId);
            const { componentsMap } = set;

            return Promise.all(componentsMap.map(async (cm) => {
              const { itemId, quantity } = cm;
              const reqItems = quantity * items;

              return itemsService.patch(
                itemId,
                { $inc: { quantity: reqItems } },
                { meta: { fromServer: true } },
              );
            }));
          }

          return itemsService.patch(
            transactionItemId,
            { $inc: { quantity: items } },
            { meta: { fromServer: true } },
          );
        }));

        return context;
      },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

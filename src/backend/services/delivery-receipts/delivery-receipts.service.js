// Initializes the `deliveryReceipts` service on path `/api/deliveryReceipts`
const createService = require('feathers-mongodb');
const hooks = require('./delivery-receipts.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/deliveryReceipts', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/deliveryReceipts');

  mongoClient.then((db) => {
    service.Model = db.collection('deliveryReceipts');
  });

  service.hooks(hooks);
};

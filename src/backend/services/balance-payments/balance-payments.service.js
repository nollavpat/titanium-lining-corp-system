// Initializes the `balancePayments` service on path `/api/balancePayments`
const createService = require('feathers-mongodb');
const hooks = require('./balance-payments.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/balancePayments', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/balancePayments');

  mongoClient.then((db) => {
    service.Model = db.collection('balance-payments');
  });

  service.hooks(hooks);
};

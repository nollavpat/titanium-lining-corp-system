import { ObjectId } from 'mongodb';

import logger from '../../../hooks/logger';
import cast from '../../../hooks/cast';


const { authenticate } = require('@feathersjs/authentication').hooks;

const fields = [
  'date',
  'balanceId',
  'amount',
  '_id',
  'bank',
];

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      _id: ObjectId(datum._id),
      balanceId: ObjectId(datum.balanceId),
    }));
  } else {
    result = {
      ...data,
      _id: ObjectId(data._id),
      balanceId: ObjectId(data.balanceId),
    };
  }

  return {
    ...context,
    data: result,
  };
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      objectIdfy,
      logger('balancePayment'),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const { balanceId, _id } = result;
        const balancesSerice = app.service('api/balances');

        await balancesSerice.patch(
          balanceId,
          { $push: { paymentIds: _id } },
          { meta: { fromServer: true } },
        );

        return context;
      },
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: amount } }, { query: { value: bank } });

        return context;
      },
      cast(fields),
    ],
    update: [],
    patch: [],
    remove: [
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: -amount } }, { query: { value: bank } });

        return context;
      },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

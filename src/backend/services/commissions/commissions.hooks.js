import { fastJoin, iff, isNot } from 'feathers-hooks-common';

import cast from '../../../hooks/cast';
// import logger from '../../../hooks/logger';

const { authenticate } = require('@feathersjs/authentication').hooks;
const { ObjectId } = require('mongodb');

const fields = [
  'percentage',
  'transactionId',
  'role',
  'profileId',
  '_id',
];

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      transactionId: ObjectId(datum.transactionId),
    }));
  } else {
    result = {
      ...data,
      transactionId: ObjectId(data.transactionId),
    };
  }

  return {
    ...context,
    data: result,
  };
};

// const fromServer = (context) => {
//   const { params } = context;

//   if (params.meta && params.meta.fromServer) {
//     return true;
//   }

//   return false;
// };

const commissionResolver = {
  joins: {
    transactionItems: () => async (pa, context) => {
      const { app } = context;
      const profilesService = app.service('api/profiles');
      const cpService = app.service('api/commissionPayments');
      const { profileId, expenseIds } = pa;

      const profile = await profilesService.get(profileId);
      const expenses = await cpService.find({
        query: { _id: { $in: expenseIds.map(p => ObjectId(p)) } },
      });

      /* eslint no-param-reassign: 0 */
      pa.profile = profile;
      pa.expenses = expenses;
    },
  },
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      objectIdfy,
    ],
    update: [
      cast(fields),
    ],
    patch: [
      // iff(
      //   isNot(fromServer),
      //   cast(fields),
      //   logger('commissions'),
      // ),
    ],
    remove: [],
  },

  after: {
    all: [
      fastJoin(commissionResolver),
    ],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const { transactionId, _id } = result;
        const transactionsService = app.service('api/transactions');

        await transactionsService.patch(
          transactionId,
          { $push: { commissionIds: _id } },
          { meta: { fromServer: true } },
        );

        return context;
      },
      // iff(
      //   isNot(fromServer),
      //   logger('commissions'),
      // ),
    ],
    update: [],
    patch: [
      // async (context) => {
      //   const { result, app } = context;
      //   const { bank, amount } = result;
      //   const constantsService = app.service('api/constants');

      //   await constantsService.patch(null, { $inc: { balance: -amount } }, { query: { value: bank } });

      //   return context;
      // },
    ],
    remove: [
      // async (context) => {
      //   const { result, app } = context;
      //   const { bank, amount } = result;
      //   const constantsService = app.service('api/constants');

      //   await constantsService.patch(null, { $inc: { balance: amount } }, { query: { value: bank } });

      //   return context;
      // },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

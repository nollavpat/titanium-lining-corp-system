// Initializes the `commissions` service on path `/api/commissions`
const createService = require('feathers-mongodb');
const hooks = require('./commissions.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/commissions', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/commissions');

  mongoClient.then((db) => {
    service.Model = db.collection('commissions');
  });

  service.hooks(hooks);
};

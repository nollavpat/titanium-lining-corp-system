import { iff, isNot, fastJoin } from 'feathers-hooks-common';

import cast from '../../../hooks/cast';
import logger from '../../../hooks/logger';

// const objectIdfy = (context) => {
//   const { data } = context;
//   let result;

//   if (Array.isArray(data)) {
//     result = data.map(datum => ({
//       ...datum,
//       // borrowedItems: datum.borrowedItems.map(c => ({
//       //   ...c,
//       //   profileId: ObjectId(c.profileId),
//       // })),
//     }));
//   } else {
//     result = {
//       ...data,
//       // borrowedItems: data.borrowedItems.map(c => ({
//       //   ...c,
//       //   profileId: ObjectId(c.profileId),
//       // })),
//     };
//   }

//   return {
//     ...context,
//     data: result,
//   };
// };

const updateSets = async (context) => {
  const { app, result } = context;
  const { _id } = result;

  const setsService = app.service('api/sets');

  const updatedComponent = componentsMap => (
    componentsMap.filter(c => JSON.stringify(c.itemId) !== JSON.stringify(_id))
  );

  const processUpdatedSets = s => Promise.resolve(setsService.update(s._id, s));

  let sets = await setsService.find({ query: { componentsMap: { $elemMatch: { itemId: _id } } } });
  sets = sets.map(set => ({
    ...set,
    componentsMap: updatedComponent(set.componentsMap),
  }));

  await Promise.all(sets.map(processUpdatedSets));

  return context;
};

const fields = [
  'code',
  '_id',
  'description',
  'price',
  'quantity',
  'borrowedItems',
  'prevQuantity',
];

const notFromRevert = method => (
  async function (context) {
    const { app, result } = context;
    const { _id } = result;

    const logsService = app.service('api/logs');
    const log = await logsService.find({ query: { method, 'prevState._id': _id } });

    return log.length === 0;
  }
);

const fromServer = (context) => {
  const { params } = context;

  if (params.meta && params.meta.fromServer) {
    return true;
  }

  return false;
};

const itemResolver = {
  joins: {
    transactionItems: () => async (i, context) => {
      const { app } = context;
      const profilesService = app.service('api/profiles');
      const { borrowedItems } = i;

      if (borrowedItems && borrowedItems.length > 0) {
        /* eslint no-param-reassign: 0 */
        const newB = await Promise.all(borrowedItems.map(async (bi) => {
          const { profileId } = bi;
          const profile = await profilesService.get(profileId);

          return {
            ...bi,
            profile,
          };
        }));

        i.borrowedItems = newB;
      }
    },
  },
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      // objectIdfy,
      cast(fields),
    ],
    update: [
      // objectIdfy,
      cast(fields),
    ],
    patch: [
      iff(
        isNot(fromServer),
        // objectIdfy,
        cast(fields),
        logger('items'),
      ),
    ],
    remove: [],
  },

  after: {
    all: [
      fastJoin(itemResolver),
    ],
    find: [],
    get: [],
    create: [
      iff(
        notFromRevert('remove'),
        cast(fields),
        logger('items'),
      ),
    ],
    update: [],
    patch: [],
    remove: [
      updateSets,
      iff(
        notFromRevert('create'),
        cast(fields),
        logger('items'),
      ),
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

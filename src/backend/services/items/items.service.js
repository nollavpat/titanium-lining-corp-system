// Initializes the `items` service on path `/api/items`
const createService = require('feathers-mongodb');
const hooks = require('./items.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/items', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/items');

  mongoClient.then((db) => {
    service.Model = db.collection('items');
  });

  service.hooks(hooks);
};

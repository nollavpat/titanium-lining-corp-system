// Initializes the `commissionPayments` service on path `/api/commissionPayments`
const createService = require('feathers-mongodb');
const hooks = require('./commission-payments.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/commissionPayments', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/commissionPayments');

  mongoClient.then((db) => {
    service.Model = db.collection('commissionPayments');
  });

  service.hooks(hooks);
};

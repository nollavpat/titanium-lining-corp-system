// import validate from '../../../hooks/validate';
// import transform from '../../../hooks/transform';
import logger from '../../../hooks/logger';
import cast from '../../../hooks/cast';

// import Expense from '../../../models/Expense';

const { authenticate } = require('@feathersjs/authentication').hooks;

const fields = [
  'date',
  'commissionId',
  'amount',
  '_id',
  'bank',
];

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      // transform(Expense),
      // validate,
    ],
    update: [],
    patch: [
      // transform(Expense),
      // validate,
    ],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const { commissionId, _id } = result;
        const cpService = app.service('api/commissions');

        await cpService.patch(
          commissionId,
          { $push: { expenseIds: _id } },
          { meta: { fromServer: true } },
        );

        return context;
      },
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: -amount } }, { query: { value: bank } });

        return context;
      },
      async (context) => {
        const { result, app } = context;
        const { commissionId } = result;
        const cpService = app.service('api/commissions');

        const commission = await cpService.get(commissionId);

        return {
          ...context,
          result: {
            ...result,
            profileId: commission.profileId,
            transactionId: commission.transactionId,
          },
        };
      },
      logger('commissions'),
      cast(fields),
    ],
    update: [],
    patch: [],
    remove: [
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: +amount } }, { query: { value: bank } });

        return context;
      },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

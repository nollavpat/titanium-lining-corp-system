// Initializes the `transactions` service on path `/api/transactions`
const createService = require('feathers-mongodb');
const hooks = require('./transactions.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/transactions', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/transactions');

  mongoClient.then((db) => {
    service.Model = db.collection('transactions');
  });

  service.hooks(hooks);
};

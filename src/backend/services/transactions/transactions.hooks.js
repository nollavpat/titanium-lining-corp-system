import { ObjectId } from 'mongodb';
import { fastJoin, iff, isNot } from 'feathers-hooks-common';

import Item from '../../../models/Item';
import Set from '../../../models/Set';
import Transaction from '../../../models/Transaction';

import validate from '../../../hooks/validate';
import cast from '../../../hooks/cast';
import transform from '../../../hooks/transform';

import { beautifyPropertyName } from '../../../utils/helper';

const fields = [
  'date',
  'transactionNumber',
  'clientId',
  'associateId',
  'seniorConsultantId',
  'distributorId',
  'presentorId',
  'closerId',
  'assistantId',
  'consultantId',
  'transactionItems',
  'terms',
  'paymentIds',
  'commissionIds',
  'deliveryReceiptIds',
  'customTotalPrice',
];

const transactionResolver = {
  joins: {
    transactionItems: () => async (transaction, context) => {
      const { app } = context;
      const { transactionItems } = transaction;

      const newTi = await Promise.all(transactionItems.map(async (ti) => {
        const itemsService = app.service('api/items');
        const setsService = app.service('api/sets');
        const service = ti.isSet ? setsService : itemsService;

        return service.get(ti.transactionItemId);
      }));

      /* eslint no-param-reassign: 0 */
      transaction.transactionItems = await Promise.all(transactionItems.map(async (ti) => {
        const item = newTi.find(i => (
          JSON.stringify(i._id) === JSON.stringify(ti.transactionItemId)
        ));

        if (ti.isSet) {
          const setItems = await Promise.all(item.componentsMap.map(async (c) => {
            const itemsService = app.service('api/items');

            return itemsService.get(c.itemId);
          }));

          const newComponentsMap = item.componentsMap.map(i => new Item({
            ...setItems.find(j => JSON.stringify(j._id) === JSON.stringify(i.itemId)),
            setQuantity: i.quantity,
          }));

          const set = new Set({
            ...item,
            components: newComponentsMap,
          });

          return {
            ...ti,
            // price: set.lessedPrice,
            description: set.description,
          };
        }

        return {
          ...ti,
          // price: item.price,
          description: item.description,
        };
      }));
    },
    profiles: () => async (transaction, context) => {
      const { app } = context;
      const {
        clientId,
        consultantId,
        associateId,
        seniorConsultantId,
        distributorId,
        presentorId,
        closerId,
        assistantId,
      } = transaction;
      const profilesService = app.service('api/profiles');

      if (clientId) {
        transaction.client = await profilesService.get(clientId);
      }

      if (consultantId) {
        transaction.consultant = await profilesService.get(consultantId);
      }

      if (associateId) {
        transaction.associate = await profilesService.get(associateId);
      }

      if (seniorConsultantId) {
        transaction.seniorConsultant = await profilesService.get(seniorConsultantId);
      }

      if (distributorId) {
        transaction.distributor = await profilesService.get(distributorId);
      }

      if (presentorId) {
        transaction.presentor = await profilesService.get(presentorId);
      }

      if (closerId) {
        transaction.closer = await profilesService.get(closerId);
      }

      if (assistantId) {
        transaction.assistant = await profilesService.get(assistantId);
      }
    },
    payments: () => async (transaction, context) => {
      const { app } = context;
      const {
        paymentIds,
      } = transaction;
      const paymentsService = app.service('api/payments');
      const constantsService = app.service('api/constants');

      const payments = await paymentsService.find({
        query: { _id: { $in: paymentIds.map(p => ObjectId(p)) } },
      });

      transaction.payments = await Promise.all(payments.map(async (p) => {
        const { depositedBankId } = p;

        if (!depositedBankId) {
          return p;
        }

        const depositedBank = await constantsService.get(depositedBankId);

        return {
          ...p,
          depositedBank: depositedBank.value,
        };
      }));
    },
    commissions: () => async (transaction, context) => {
      const { app } = context;
      const {
        commissionIds,
      } = transaction;
      const commissionsService = app.service('api/commissions');
      const profilesService = app.service('api/profiles');
      const cpService = app.service('api/commissionPayments');

      const commissions = await commissionsService.find({
        query: { _id: { $in: commissionIds.map(p => ObjectId(p)) } },
      });

      transaction.commissions = await Promise.all(commissions.map(async (p) => {
        const { profileId, expenseIds } = p;

        const profile = await profilesService.get(profileId);
        const expenses = await cpService.find({
          query: { _id: { $in: expenseIds.map(e => ObjectId(e)) } },
        });

        return {
          ...p,
          profile,
          expenses,
        };
      }));
    },
    deliveryReceipts: () => async (transaction, context) => {
      const { app } = context;
      const {
        deliveryReceiptIds,
      } = transaction;
      const drsService = app.service('api/deliveryReceipts');

      const deliveryReceipts = await drsService.find({
        query: { _id: { $in: deliveryReceiptIds.map(p => ObjectId(p)) } },
      });

      const a = await Promise.all(deliveryReceipts.map(async (dr) => {
        const newTi = await Promise.all(dr.transactionItems.map(async (ti) => {
          const itemsService = app.service('api/items');
          const setsService = app.service('api/sets');
          const service = ti.isSet ? setsService : itemsService;
          const item = await service.get(ti.transactionItemId);

          return {
            ...ti,
            ...item,
            items: ti.items,
          };
        }));

        return {
          ...dr,
          transactionItems: newTi,
        };
      }));

      transaction.deliveryReceipts = a;
    },
  },
};

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      clientId: ObjectId(datum.clientId),
      associateId: datum.associateId ? ObjectId(datum.associateId) : null,
      seniorConsultantId: datum.seniorConsultantId ? ObjectId(datum.seniorConsultantId) : null,
      distributorId: datum.distributorId ? ObjectId(datum.distributorId) : null,
      presentorId: datum.presentorId ? ObjectId(datum.presentorId) : null,
      closerId: datum.closerId ? ObjectId(datum.closerId) : null,
      assistant: datum.assistantId ? ObjectId(datum.assistantId) : null,
      commissionIds: datum.commissionIds.map(p => ObjectId(p)),
      consultantId: ObjectId(datum.consultantId),
      paymentIds: datum.paymentIds.map(p => ObjectId(p)),
      deliveryReceiptIds: datum.deliveryReceiptIds.map(p => ObjectId(p)),
      transactionItems: datum.transactionItems.map(c => ({
        ...c,
        transactionItemId: ObjectId(c.transactionItemId),
      })),
    }));
  } else {
    result = {
      ...data,
      clientId: ObjectId(data.clientId),
      associateId: data.associateId ? ObjectId(data.associateId) : null,
      seniorConsultantId: data.seniorConsultantId ? ObjectId(data.seniorConsultantId) : null,
      distributorId: data.distributorId ? ObjectId(data.distributorId) : null,
      presentorId: data.presentorId ? ObjectId(data.presentorId) : null,
      closerId: data.closerId ? ObjectId(data.closerId) : null,
      assistant: data.assistantId ? ObjectId(data.assistantId) : null,
      commissionIds: data.commissionIds.map(p => ObjectId(p)),
      consultantId: ObjectId(data.consultantId),
      paymentIds: data.paymentIds.map(p => ObjectId(p)),
      deliveryReceiptIds: data.deliveryReceiptIds.map(p => ObjectId(p)),
      transactionItems: data.transactionItems.map(c => ({
        ...c,
        transactionItemId: ObjectId(c.transactionItemId),
      })),
    };
  }

  return {
    ...context,
    data: result,
  };
};

const fromServer = (context) => {
  const { params } = context;

  if (params.meta && params.meta.fromServer) {
    return true;
  }

  return false;
};

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      transform(Transaction),
      validate,
      cast(fields),
      objectIdfy,
    ],
    update: [],
    patch: [
      iff(
        isNot(fromServer),
        transform(Transaction),
        validate,
        cast(fields),
        objectIdfy,
      ),
    ],
    remove: [],
  },

  after: {
    all: [
      fastJoin(transactionResolver),
    ],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const commissionsService = app.service('api/commissions');
        const {
          associateId,
          seniorConsultantId,
          distributorId,
          presentorId,
          closerId,
          assistantId,
          consultantId,
          _id,
        } = result;
        const otherProfiles = {
          associateId,
          seniorConsultantId,
          distributorId,
          presentorId,
          closerId,
          assistantId,
          consultantId,
        };
        const finalProfiles = Object.keys(otherProfiles)
          .filter(key => otherProfiles[key])
          .reduce((acc, key) => ({
            ...acc,
            [key]: otherProfiles[key],
          }), {});
        const defaultCommission = {
          percentage: null,
          transactionId: _id,
          expenseIds: [],
        };

        await Promise.all(Object.keys(finalProfiles).map(async (key) => {
          const role = beautifyPropertyName(key.slice(0, key.length - 2));
          const c = await commissionsService.create(
            {
              ...defaultCommission,
              role,
              profileId: finalProfiles[key],
            },
            { meta: { fromServer: true } },
          );

          return c._id;
        }));

        return context;
      },
    ],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

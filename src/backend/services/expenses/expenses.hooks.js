import validate from '../../../hooks/validate';
import transform from '../../../hooks/transform';
import logger from '../../../hooks/logger';
import cast from '../../../hooks/cast';

import Expense from '../../../models/Expense';

const { authenticate } = require('@feathersjs/authentication').hooks;

const fields = [
  'date',
  'tinNumber',
  'establishment',
  'remarks',
  'amount',
  '_id',
  'bank',
];

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      transform(Expense),
      validate,
    ],
    update: [],
    patch: [
      transform(Expense),
      validate,
    ],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { app, result } = context;
        const expensesService = app.service('api/expenses');
        const notificationsService = app.service('api/expenseNotifications');

        if (!Array.isArray(result)) {
          const { tinNumber, establishment } = result;
          const currentDate = new Date();
          const currentYear = currentDate.getFullYear();
          const existingNotifications = await notificationsService.find({ query: { tinNumber } });
          const hasThisYearNotifications = existingNotifications.some((n) => {
            const d = new Date(n.date);

            return d.getFullYear() === currentYear;
          });

          if (hasThisYearNotifications) {
            return context;
          }

          const expenses = await expensesService.find({ query: { tinNumber } });
          const expensesThisYear = expenses
            .filter((e) => {
              const d = new Date(e.date);

              return d.getFullYear() === currentYear;
            });
          const total = expensesThisYear.reduce((acc, e) => acc + e.amount, 0);

          if (expensesThisYear.length === 6) {
            await notificationsService.create({
              tinNumber,
              message: `6 expenses found this year from TIN Number: ${tinNumber} <${establishment}>`,
              cleared: false,
              date: new Date(),
            });
          }

          if (total >= 10000) {
            await notificationsService.create({
              tinNumber,
              message: `TIN Number: ${tinNumber} <${establishment}>, expenses exceeded 10,000`,
              cleared: false,
              date: new Date(),
            });
          }
        }

        return context;
      },
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: -amount } }, { query: { value: bank } });

        return context;
      },
      cast(fields),
      logger('expenses'),
    ],
    update: [],
    patch: [],
    remove: [
      async (context) => {
        const { result, app } = context;
        const { amount, bank } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(null, { $inc: { balance: +amount } }, { query: { value: bank } });

        return context;
      },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

// Initializes the `expenses` service on path `/api/expenses`
const createService = require('feathers-mongodb');
const hooks = require('./expenses.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/expenses', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/expenses');

  mongoClient.then((db) => {
    service.Model = db.collection('expenses');
  });

  service.hooks(hooks);
};

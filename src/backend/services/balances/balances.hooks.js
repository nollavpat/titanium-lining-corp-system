import { ObjectId } from 'mongodb';
import { fastJoin } from 'feathers-hooks-common';

import logger from '../../../hooks/logger';

const { authenticate } = require('@feathersjs/authentication').hooks;

const balanceResolver = {
  joins: {
    payments: () => async (balance, context) => {
      const { app } = context;
      const bpService = app.service('api/balancePayments');
      const { paymentIds } = balance;

      let payments = [];

      if (paymentIds) {
        payments = await bpService.find({
          query: { _id: { $in: paymentIds.map(p => ObjectId(p)) } },
        });
      }

      /* eslint no-param-reassign: 0 */
      balance.payments = payments;
    },
  },
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [
      fastJoin(balanceResolver),
    ],
    find: [],
    get: [],
    create: [
      logger('balance'),
    ],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

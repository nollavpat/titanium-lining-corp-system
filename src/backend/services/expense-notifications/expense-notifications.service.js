// Initializes the `expenseNotifications` service on path `/api/expenseNotifications`
const createService = require('feathers-mongodb');
const hooks = require('./expense-notifications.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/expenseNotifications', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/expenseNotifications');

  mongoClient.then((db) => {
    service.Model = db.collection('notifications');
  });

  service.hooks(hooks);
};

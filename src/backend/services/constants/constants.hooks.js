import logger from '../../../hooks/logger';

const { authenticate } = require('@feathersjs/authentication').hooks;

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [
      async (context) => {
        const {
          method,
          app,
          type,
        } = context;
        const info = type === 'before' ? 'data' : 'result';
        const logsService = app.service('api/logs');

        await logsService.create({
          method,
          prevState: context[info],
          service: 'banks',
          date: new Date(),
        });

        return context;
      },
    ],
    update: [],
    patch: [
      async (context) => {
        const {
          method,
          app,
          type,
        } = context;
        const info = type === 'before' ? 'data' : 'result';
        const logsService = app.service('api/logs');

        await logsService.create({
          method,
          prevState: context[info],
          service: 'banks',
          date: new Date(),
        });

        return context;
      },
    ],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

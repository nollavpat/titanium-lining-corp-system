// Initializes the `constants` service on path `/api/constants`
const createService = require('feathers-mongodb');
const hooks = require('./constants.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/constants', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/constants');

  mongoClient.then((db) => {
    service.Model = db.collection('constants');
  });

  service.hooks(hooks);
};

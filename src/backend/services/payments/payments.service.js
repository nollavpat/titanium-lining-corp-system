// Initializes the `payments` service on path `/api/payments`
const createService = require('feathers-mongodb');
const hooks = require('./payments.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/payments', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/payments');

  mongoClient.then((db) => {
    service.Model = db.collection('payments');
  });

  service.hooks(hooks);
};

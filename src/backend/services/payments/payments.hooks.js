import { fastJoin } from 'feathers-hooks-common';

import logger from '../../../hooks/logger';

const { ObjectId } = require('mongodb');
const { authenticate } = require('@feathersjs/authentication').hooks;

const objectIdfy = (context) => {
  const { data } = context;
  let result;

  if (Array.isArray(data)) {
    result = data.map(datum => ({
      ...datum,
      transactionId: ObjectId(datum.transactionId),
      depositedBankId: datum.depositedBankId ? ObjectId(datum.depositedBankId) : '',
    }));
  } else {
    result = {
      ...data,
      transactionId: ObjectId(data.transactionId),
      depositedBankId: data.depositedBankId ? ObjectId(data.depositedBankId) : '',
    };
  }

  return {
    ...context,
    data: result,
  };
};

const paymentResolver = {
  joins: {
    transactionItems: () => async (p, context) => {
      const { app } = context;
      const constantsService = app.service('api/constants');
      const { depositedBankId } = p;

      if (depositedBankId) {
        const depositedBank = await constantsService.get(depositedBankId);

        /* eslint no-param-reassign: 0 */
        p.depositedBank = depositedBank.value;
      }
    },
  },
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [
      objectIdfy,
      // allowPay,
    ],
    update: [
      async (context) => {
        const { data, app } = context;
        const { amount, _id } = data;
        const paymentsService = app.service('api/payments');
        const prePayment = await paymentsService.get(_id);

        const constantsService = app.service('api/constants');

        await constantsService.patch(prePayment.depositedBankId, { $inc: { balance: -amount } });

        return context;
      },
    ],
    patch: [
      objectIdfy,
      logger('payments'),
    ],
    remove: [],
  },

  after: {
    all: [
      fastJoin(paymentResolver),
    ],
    find: [],
    get: [],
    create: [
      async (context) => {
        const { result, app } = context;
        const { transactionId, _id } = result;
        const transactionsService = app.service('api/transactions');

        await transactionsService.patch(
          transactionId,
          { $push: { paymentIds: _id } },
          { meta: { fromServer: true } },
        );

        return context;
      },
      logger('payments'),
    ],
    update: [],
    patch: [
      async (context) => {
        const { result, app } = context;
        const { depositedBankId, amount } = result;
        const constantsService = app.service('api/constants');

        await constantsService.patch(depositedBankId, { $inc: { balance: amount } });

        return context;
      },
    ],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

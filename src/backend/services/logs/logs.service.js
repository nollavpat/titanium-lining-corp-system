// Initializes the `logs` service on path `/api/logs`
const createService = require('feathers-mongodb');
const hooks = require('./logs.hooks');

module.exports = function (app) {
  const mongoClient = app.get('mongoClient');
  const options = {};

  // Initialize our service with any options it requires
  app.use('/api/logs', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/logs');

  mongoClient.then((db) => {
    service.Model = db.collection('logs');
  });

  service.hooks(hooks);
};

import { fastJoin } from 'feathers-hooks-common';

const { authenticate } = require('@feathersjs/authentication').hooks;

const logResolver = {
  joins: {
    transactions: () => async (l, context) => {
      const { app } = context;
      const transactionsService = app.service('api/transactions');
      const { prevState } = l;

      /* eslint no-param-reassign: 0 */
      if (prevState.transactionId) {
        const transaction = await transactionsService.get(prevState.transactionId);

        l.transaction = transaction;
      }

      if (prevState.fromId && prevState.toId) {
        const constantsService = app.service('api/constants');
        const { fromId, toId } = prevState;

        const from = await constantsService.get(fromId);
        const to = await constantsService.get(toId);

        prevState.from = from.value;
        prevState.to = to.value;
      }

      if (prevState.transactionItems) {
        const newTi = await Promise.all(prevState.transactionItems.map(async (ti) => {
          const itemsService = app.service('api/items');
          const setsService = app.service('api/sets');
          const service = ti.isSet ? setsService : itemsService;
          const item = await service.get(ti.transactionItemId);

          return {
            ...ti,
            ...item,
            items: ti.items,
          };
        }));

        l.prevState.transactionItems = newTi;
      }

      if (prevState.profileId) {
        const profilesService = app.service('api/profiles');
        const profile = await profilesService.get(prevState.profileId);

        l.prevState.profile = { name: `${profile.firstname} ${profile.lastname}` };
      }
    },
  },
};

module.exports = {
  before: {
    all: [authenticate('jwt')],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  after: {
    all: [
      fastJoin(logResolver),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

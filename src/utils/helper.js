export const beautifyPrice = price => (
  price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
);

export const beautifyPropertyName = propertyName => (
  propertyName
    .split(/(?=[A-Z])/)
    .map(s => s.split(' ').map(ss => ss.charAt(0).toUpperCase() + ss.substr(1)).join(' '))
    .join(' ')
);

export const timeStartDate = (date) => {
  const myDate = new Date(+date);

  myDate.setHours(0);
  myDate.setMinutes(0);
  myDate.setSeconds(0);
  myDate.setMilliseconds(0);

  return myDate;
};

export const timeEndDate = (date) => {
  const myDate = new Date(+date);

  myDate.setHours(23);
  myDate.setMinutes(59);
  myDate.setSeconds(0);
  myDate.setMilliseconds(0);

  return myDate;
};

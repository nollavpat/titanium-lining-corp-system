class ValidationError extends Error {
  constructor(errorDetails, ...args) {
    super(...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ValidationError);
    }

    this.errorDetails = errorDetails;
  }
}

export default ValidationError;

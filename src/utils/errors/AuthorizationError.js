class AuthorizationError extends Error {
  constructor(...args) {
    super(...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AuthorizationError);
    }

    this.name = 'NotAuthorizedError';
  }
}

export default AuthorizationError;

class NoRoleError extends Error {
  constructor(...args) {
    super(...args);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, NoRoleError);
    }

    this.name = 'NoRoleError';
  }
}

export default NoRoleError;

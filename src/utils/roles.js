export default {
  NOT_AUTHENTICATED: 'not authenticated',
  NONE: 'none',
  SUPERADMIN: 'super admin',
  ADMIN: 'admin',
  INVENTORY: 'inventory',
  ACCOUNTING: 'accounting',
  SALES: 'sales',
  CLIENT: 'client',
  CONSULTANT: 'consultant',
  TRAINEE: 'trainee',
  TRAINER: 'trainer',
  ASSOCIATE: 'associate',
};

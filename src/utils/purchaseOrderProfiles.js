export default {
  CONSULTANT: 'consultant',
  ASSOCIATE: 'associate',
  SENIOR_CONSULTANT: 'seniorConsultant',
  DISTRIBUTOR: 'distributor',
  PRESENTOR: 'presentor',
  CLOSER: 'closer',
  ASSISTANT: 'assistant',
};

import Joi from 'joi';

class Model {
  constructor(props) {
    Object.assign(this, props);
  }

  static get schema() {
    return {};
  }

  validate = () => (
    Joi.validate(
      this,
      this.constructor.schema,
      {
        stripUnknown: true,
        skipFunctions: true, // skips functions
        abortEarly: false, // get all errors
      },
    )
  );

  get isValid() {
    const { error } = this.validate();

    return error === null;
  }

  get errors() {
    const { error } = this.validate();

    if (error) {
      const { details } = error;
      return details;
    }

    return [];
  }
}

export default Model;

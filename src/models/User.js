import Joi from 'joi';

import Model from './Model';
import roles from '../utils/roles';

class User extends Model {
  static get schema() {
    return {
      username: Joi.string().required(),
      name: Joi.string().required(),
      password: Joi.string().required(),
      role: Joi.string().allow(roles).required(),
    };
  }
}

export default User;

import Joi from 'joi';

import Model from './Model';

class Cash extends Model {
  static get schema() {
    return {
      paymentOption: Joi.string().required(),
      provisionaryNumber: Joi.string().required(),
      amount: Joi.number().positive().required(),
      checkNumber: Joi.string().allow('').default(''),
      bank: Joi.string().allow('').default(''),
      cleared: Joi.boolean().default(false),
      date: Joi.date().iso().required(),
      amortizationDate: Joi.date().allow(null),
      isDownPayment: Joi.boolean().default(false),
    };
  }
}

export default Cash;

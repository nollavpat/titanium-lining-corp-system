import Joi from 'joi';

import Model from './Model';

import { HEX_LENGTH } from '../utils/constants';

class Set extends Model {
  static get schema() {
    return {
      description: Joi.string().required(),
      code: Joi.string().required(),
      less: Joi.number().positive().required(),
      componentsMap: Joi.array().items(Joi.object({
        itemId: Joi.string().hex().length(HEX_LENGTH).required(),
        quantity: Joi.number().integer().positive().required(),
      }).required()).required(),
    };
  }

  get originalPrice() {
    return this.components
      .reduce((accumulator, item) => accumulator + item.setPrice, 0);
  }

  get lessedPrice() {
    return this.originalPrice - this.less;
  }

  get quantity() {
    return Math.min(
      ...this.components.map(c => Math.trunc(c.availableQuantity / c.setQuantity)),
    );
  }
}

export default Set;

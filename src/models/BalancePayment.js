import Joi from 'joi';

import Model from './Model';

import { HEX_LENGTH } from '../utils/constants';

class BalancePayment extends Model {
  static get schema() {
    return {
      date: Joi.date().iso().required(),
      bank: Joi.string().required(),
      balanceId: Joi.string().hex().length(HEX_LENGTH).required(),
      amount: Joi.number().positive().required(),
    };
  }
}

export default BalancePayment;

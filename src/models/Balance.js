import Joi from 'joi';

import Model from './Model';

class Balance extends Model {
  static get schema() {
    return {
      name: Joi.string().required(),
      totalBalance: Joi.number().positive().required(),
    };
  }

  get totalPayment() {
    return this.payments.reduce((acc, payment) => acc + payment.amount, 0);
  }

  get remainingBalance() {
    return this.totalBalance - this.totalPayment;
  }

  get isComplete() {
    return this.remainingBalance <= 0;
  }
}

export default Balance;

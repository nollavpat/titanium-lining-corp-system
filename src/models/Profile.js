import Joi from 'joi';

import Model from './Model';

class Profile extends Model {
  static get schema() {
    return {
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      homeAddressFormat: Joi.object().keys({
        streetNumber: Joi.string().allow(''),
        baranggay: Joi.string().allow(''),
        city: Joi.string().allow(''),
      }),
      businessAddressFormat: Joi.object().keys({
        streetNumber: Joi.string().allow(''),
        baranggay: Joi.string().allow(''),
        city: Joi.string().allow(''),
      }),
      cellphoneNumber: Joi.string(),
      email: Joi.string().allow(''),
      businessName: Joi.string().allow(''),
      position: Joi.string().allow(''),
      businessTelNumber: Joi.string().allow(''),
      residentTelNumber: Joi.string().allow(''),
      role: Joi.string().required(),
      birthday: Joi.date().iso().allow(null),
      picture: Joi.string().allow(null),
    };
  }

  get isBirthmonth() {
    const currentDate = new Date();

    return currentDate.getMonth() === this.birthday.getMonth();
  }

  get isBirthDay() {
    const currentDate = new Date();

    return currentDate.getMonth() === this.birthday.getMonth()
      && currentDate.getDate() === this.birthday.getDate();
  }

  get homeAddress() {
    const {
      streetNumber,
      baranggay,
      city,
    } = this.homeAddressFormat;

    return `${streetNumber} ${baranggay} ${city}`;
  }

  get businessAddress() {
    const {
      streetNumber,
      baranggay,
      city,
    } = this.businessAddressFormat;

    return `${streetNumber} ${baranggay} ${city}`;
  }

  get name() {
    return `${this.firstname} ${this.lastname}`;
  }
}

export default Profile;

import Joi from 'joi';

import Model from './Model';

class Expense extends Model {
  static get schema() {
    return {
      date: Joi.date().iso().required(),
      tinNumber: Joi.string().required(),
      establishment: Joi.string().required(),
      remarks: Joi.string().optional().allow(''),
      amount: Joi.number().positive().required(),
      bank: Joi.string().invalid('').required(),
    };
  }
}

export default Expense;

import Joi from 'joi';

import Model from './Model';

import { HEX_LENGTH } from '../utils/constants';

class Commission extends Model {
  static get schema() {
    return {
      transactionId: Joi.string().hex().length(HEX_LENGTH).required(),
      profileId: Joi.string().hex().length(HEX_LENGTH).required(),
      percentage: Joi.number().positive().required(),
    };
  }

  get amount() {
    return this.expenses.reduce((acc, e) => acc + e.amount, 0);
  }

  get name() {
    return this.profile.name;
  }

  get remainingCommission() {
    return !this.percentage ? 'Percentage unset' : 'percentage set';
  }

  commissionBalance = (basicCommission) => {
    if (!this.percentage) {
      return 'No Percentage';
    }

    return basicCommission * (this.percentage / 100) - this.amount;
  }

  commissionTotal = (possibleCommission) => {
    if (!this.percentage) {
      return 'No Percentage';
    }

    return possibleCommission * (this.percentage / 100);
  }
}

export default Commission;

import Joi from 'joi';

import Model from './Model';

class Cash extends Model {
  static get schema() {
    return {
      type: Joi.string().required(),
      value: Joi.any().invalid(['', 0]).required(),
    };
  }
}

export default Cash;

import Joi from 'joi';

import Model from './Model';

import { HEX_LENGTH } from '../utils/constants';

class BankTransfer extends Model {
  static get schema() {
    return {
      fromBankId: Joi.string().hex().length(HEX_LENGTH).required(),
      toBankId: Joi.string().hex().length(HEX_LENGTH).required(),
      amount: Joi.number().positive().required(),
    };
  }
}

export default BankTransfer;

import Joi from 'joi';

import Model from './Model';
import { HEX_LENGTH } from '../utils/constants';

class Transaction extends Model {
  static get schema() {
    return {
      date: Joi.date().iso().required(),
      transactionNumber: Joi.string().alphanum().required(),
      clientId: (
        Joi
          .string()
          .hex()
          .length(HEX_LENGTH)
          .required()
      ),
      associateId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      seniorConsultantId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      distributorId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      presentorId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      closerId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      assistantId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      consultantId: (
        Joi
          .string().hex().length(HEX_LENGTH).default(null)
          .allow(null)
          .optional()
      ),
      commissionIds: Joi.array().items(Joi.string().hex().length(HEX_LENGTH)).default([]),
      transactionItems: Joi.array().items(Joi.object({
        transactionItemId: Joi.string().hex().length(HEX_LENGTH).required(),
        isGift: Joi.boolean().required(),
        isSet: Joi.boolean().required(),
        items: Joi.number().integer().positive().required(),
        price: Joi.number().integer().positive().required(),
      }).required()).required(),
      terms: Joi.number().integer().positive().required(),
      paymentIds: Joi.array().items(Joi.string().hex().length(HEX_LENGTH)).default([]),
      deliveryReceiptIds: Joi.array().items(Joi.string().hex().length(HEX_LENGTH)).default([]),
    };
  }

  getDRitems = (id) => {
    const drs = this.deliveryReceipts
      .filter((d) => {
        const ids = d.transactionItems.map(t => t.transactionItemId);

        return ids.includes(id);
      })
      .map((d) => {
        const ti = d.transactionItems.find(t => t.transactionItemId === id);

        return ti.items;
      });

    if (drs.length === 0) {
      return 0;
    }

    return drs.reduce((acc, ti) => acc + ti, 0);
  }

  get drItems() {
    return this.transactionItems.map(t => ({
      ...t,
      available: +(t.items - this.getDRitems(t.transactionItemId)),
      items: this.getDRitems(t.transactionItemId),
    }));
  }

  get isDrComplete() {
    return this.transactionItems.every((ti) => {
      const allDR = this.deliveryReceipts
        .filter(dr => dr.transactionItems.map(i => i.transactionItemId)
          .includes(ti.transactionItemId));

      return allDR.reduce((acc, dr) => {
        const { transactionItems } = dr;

        return transactionItems.reduce((acc1, dri) => {
          if (dri.transactionItemId === ti.transactionItemId) {
            return acc1 + dri.items;
          }

          return acc1;
        }, 0) + acc;
      }, 0) === ti.items;
    });
  }

  get isActive() {
    return this.totalPayment > 0;
  }

  get price() {
    return this.transactionItems
      .reduce((accumulator, item) => accumulator + item.finalPrice, 0);
  }

  get isTermsLessThanSixMonths() {
    return this.terms <= 6;
  }

  get downPayments() {
    return this.payments.filter(p => p.isDownPayment);
  }

  get hasDownPayment() {
    return this.downPayments.length !== 0;
  }

  get loanAmortizations() {
    return this.payments.filter(p => !p.isDownPayment);
  }

  get downPayment() {
    return this.downPayments.reduce((accumulator, payment) => accumulator + payment.amount, 0);
  }

  get totalPayment() {
    return this.payments.reduce((accumulator, payment) => accumulator + payment.amount, 0);
  }

  get remainingBalance() {
    return +(this.totalPrice - this.totalPayment).toFixed(2);
  }

  get priceWithNoDownPayment() {
    return this.price - this.downPayment;
  }

  get monthlyPayment() {
    if (this.customTotalPrice) {
      return 0;
    }

    if (this.isTermsLessThanSixMonths) {
      return this.priceWithNoDownPayment / this.terms;
    }

    let interestRate;

    if (this.terms === 9) {
      interestRate = 0.1078;
    } else if (this.terms === 12) {
      interestRate = 0.1059;
    } else if (this.terms === 18) {
      interestRate = 0.10475;
    } else if (this.terms === 24) {
      interestRate = 0.10495;
    }

    const years = this.terms / 12;
    const interest = this.priceWithNoDownPayment * years * interestRate;

    return +((this.priceWithNoDownPayment + interest) / this.terms).toFixed(2);
  }

  get isPaymentComplete() {
    return this.remainingBalance <= 0;
  }

  isVirtualPaymentPaid = amortizationDate => (
    this.loanAmortizations
      .map(p => p.amortizationDate.getMonth())
      .includes(amortizationDate.getMonth())
  )

  get virtualPayments() {
    if (this.isPaymentComplete) {
      return [];
    }
    const payments = [];

    for (let i = 1; i <= this.terms; i += 1) {
      const m = this.date.getMonth() + i;
      const y = this.date.getFullYear();
      const currentDate = new Date(y, m, 1);

      payments.push({
        date: currentDate,
        paymentOption: 'cash', // cash check or pdc
        amount: this.monthlyPayment,
        checkNumber: '',
        bank: '',
        depositedBank: '', // bdo metro bpi
        cleared: false, // checks and pdc defaults
        isDownPayment: false, // if false, means loan amortization
        amortizationDate: currentDate,
      });
    }

    return payments.filter(p => !this.isVirtualPaymentPaid(p.amortizationDate));
  }

  get totalPrice() {
    if (this.customTotalPrice) {
      return this.customTotalPrice;
    }

    if (this.totalPayment >= this.price && this.loanAmortizations.length <= 6) {
      return this.price;
    }
    return (this.monthlyPayment * this.terms) + this.downPayment;
  }

  get basicCommission() {
    if (this.totalPayment === 0) {
      return 0;
    }

    return this.totalPayment * 0.83;
  }

  get possibleCommission() {
    return this.totalPrice * 0.83;
  }

  get hasCurrentMonthPayment() {
    const currentDate = new Date();
    const month = currentDate.getMonth();

    return this.virtualPayments.some(v => v.date.getMonth() === month);
  }

  get hasOverduePayment() {
    const currentDate = new Date();

    return this.virtualPayments.some(v => v.date < currentDate);
  }
}

export default Transaction;

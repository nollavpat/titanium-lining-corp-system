import Model from './Model';

class TransactionItem extends Model {
  get finalPrice() {
    return this.isGift ? 0 : this.price * this.items;
  }
}

export default TransactionItem;

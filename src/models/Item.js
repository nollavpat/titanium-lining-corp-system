import Joi from 'joi';

import Model from './Model';

class Item extends Model {
  /* eslint class-methods-use-this: 0 */
  static get schema() {
    return {
      code: Joi.string().required(),
      description: Joi.string().required(),
      price: Joi.number().positive().required(),
      quantity: Joi.number().integer().required(),
    };
  }

  get totalBorrowedItems() {
    if (!this.borrowedItems) {
      return 0;
    }
    return this.borrowedItems.reduce((acc, bi) => acc + bi.quantity, 0);
  }

  get availableQuantity() {
    return this.quantity - this.totalBorrowedItems;
  }

  get setPrice() {
    return this.price * this.setQuantity;
  }

  get remarks() {
    return [];
  }
}

export default Item;

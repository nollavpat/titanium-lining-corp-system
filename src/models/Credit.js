import Joi from 'joi';

import Model from './Model';

class Credit extends Model {
  static get schema() {
    return {
      paymentOption: Joi.string().required(),
      provisionaryNumber: Joi.string().required(),
      amount: Joi.number().positive().required(),
      checkNumber: Joi.string().required(),
      bank: Joi.string().required(),
      cleared: Joi.boolean().default(false),
      date: Joi.date().iso().required(),
      amortizationDate: Joi.date().allow(null),
      isDownPayment: Joi.boolean().default(false),
    };
  }
}

export default Credit;

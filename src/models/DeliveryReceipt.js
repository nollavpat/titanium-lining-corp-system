import Joi from 'joi';

import Model from './Model';

import { HEX_LENGTH } from '../utils/constants';

class DeliveryReceipt extends Model {
  static get schema() {
    return {
      date: Joi.date().iso().required(),
      transactionItems: Joi.array().items(Joi.object({
        transactionItemId: Joi.string().hex().length(HEX_LENGTH).required(),
        items: Joi.number().integer().positive().required(),
        isSet: Joi.boolean().required(),
      }).required()).required(),
      drNumber: Joi.string().required(),
    };
  }
}

export default DeliveryReceipt;

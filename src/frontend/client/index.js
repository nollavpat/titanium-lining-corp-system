import authenticationClient from '@feathersjs/authentication-client';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import io from 'socket.io-client';

import services from './services';

const url = `${window.location.protocol}//${window.location.host}`;
const socket = io(url);

const client = feathers()
  .configure(authenticationClient({ storage: window.localStorage }))
  .configure(socketio(socket))
  .configure(services);

export default client;

import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Profile from '../../../../models/Profile';

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      date: new Date(data.date),
      transaction: data.transaction ? {
        ...data.transaction,
        client: new Profile(data.transaction.client),
        trainee: data.transaction.trainee ? new Profile(data.transaction.trainee) : null,
        consultant: new Profile(data.transaction.consultant),
        trainer: data.transaction.trainer ? new Profile(data.transaction.trainer) : null,
      } : null,
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      date: new Date(context[info].date),
      transaction: context[info].transaction ? {
        ...context[info].transaction,
        client: new Profile(context[info].transaction.client),
        trainee: context[info].transaction.trainee ? new Profile(context[info].transaction.trainee) : null,
        consultant: new Profile(context[info].transaction.consultant),
        trainer: context[info].transaction.trainer ? new Profile(context[info].transaction.trainer) : null,
      } : null,
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.SUPERADMIN]),
    ],
    update: [
      authorization([roles.SUPERADMIN]),
    ],
    patch: [
      authorization([roles.SUPERADMIN]),
    ],
    remove: [
      authorization([roles.SUPERADMIN]),
    ],
  },

  after: {
    all: [
      customTransform,
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

import hooks from './logs.hooks';

export default function (app) {
  const service = app.service('api/logs');

  service.hooks(hooks);
}

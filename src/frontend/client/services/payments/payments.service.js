import hooks from './payments.hooks';

export default function (app) {
  const service = app.service('api/payments');

  service.hooks(hooks);
}

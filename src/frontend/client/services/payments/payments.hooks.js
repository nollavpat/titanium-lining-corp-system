import { iffElse } from 'feathers-hooks-common';

import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Cash from '../../../../models/Cash';
import Credit from '../../../../models/Credit';

const isCash = context => (context.data.paymentOption === 'cash');

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      date: new Date(data.date),
      amortizationDate: new Date(data.amortizationDate),
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      date: new Date(context[info].date),
      amortizationDate: new Date(context[info].amortizationDate),
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.ACCOUNTING]),
      iffElse(
        isCash,
        [transform(Cash)],
        [transform(Credit)],
      ),
      validate,
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
      iffElse(
        isCash,
        [transform(Cash)],
        [transform(Credit)],
      ),
      validate,
    ],
    remove: [
      authorization([roles.ACCOUNTING]),
    ],
  },

  after: {
    all: [
      customTransform,
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

// import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Commssion from '../../../../models/Commssion';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.ACCOUNTING]),
      // transform(Commssion),
      // validate,
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
    ],
    remove: [
      authorization([roles.ACCOUNTING]),
    ],
  },

  after: {
    all: [
      transform(Commssion),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

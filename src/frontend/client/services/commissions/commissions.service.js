import hooks from './commissions.hooks';

export default function (app) {
  const service = app.service('api/commissions');

  service.hooks(hooks);
}

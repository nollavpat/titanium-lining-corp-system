import hooks from './expenses.hooks';

export default function (app) {
  const service = app.service('api/expenses');

  service.hooks(hooks);
}

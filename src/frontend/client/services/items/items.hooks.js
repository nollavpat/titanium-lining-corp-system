import transform from '../../../../hooks/transform';
import validate from '../../../../hooks/validate';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Item from '../../../../models/Item';
import Profile from '../../../../models/Profile';

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      borrowedItems: data.borrowedItems ? data.borrowedItems.map(bi => ({
        ...bi,
        profile: new Profile(bi.profile),
      })) : [],
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      borrowedItems: context[info].borrowedItems ? context[info].borrowedItems.map(bi => ({
        ...bi,
        profile: new Profile(bi.profile),
      })) : [],
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.INVENTORY]),
      transform(Item),
      validate,
    ],
    update: [
      authorization([roles.INVENTORY]),
      transform(Item),
      validate,
    ],
    patch: [
      authorization([roles.INVENTORY]),
      transform(Item),
      validate,
    ],
    remove: [
      authorization([roles.INVENTORY]),
    ],
  },

  after: {
    all: [
      customTransform,
      transform(Item),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

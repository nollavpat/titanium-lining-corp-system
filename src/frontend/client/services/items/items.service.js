import hooks from './items.hooks';

export default function (app) {
  const service = app.service('api/items');

  service.hooks(hooks);
}

import hooks from './users.hooks';

export default function (app) {
  const service = app.service('api/users');

  service.hooks(hooks);
}

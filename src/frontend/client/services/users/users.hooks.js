import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import User from '../../../../models/User';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      transform(User),
      validate,
    ],
    update: [
      authorization([roles.SUPERADMIN]),
    ],
    patch: [
      authorization([roles.SUPERADMIN]),
      transform(User),
      validate,
    ],
    remove: [
      authorization([roles.SUPERADMIN]),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

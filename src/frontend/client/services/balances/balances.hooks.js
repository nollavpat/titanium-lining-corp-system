import authorization from '../../../../hooks/authorization';
import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';

import roles from '../../../../utils/roles';

import Balance from '../../../../models/Balance';


export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.ACCOUNTING]),
      transform(Balance),
      validate,
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
    ],
    remove: [
      authorization([roles.ACCOUNTING]),
    ],
  },

  after: {
    all: [
      transform(Balance),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

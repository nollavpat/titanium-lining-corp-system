import hooks from './balances.hooks';

export default function (app) {
  const service = app.service('api/balances');

  service.hooks(hooks);
}

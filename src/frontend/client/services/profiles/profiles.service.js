import hooks from './profiles.hooks';

export default function (app) {
  const service = app.service('api/profiles');

  service.hooks(hooks);
}

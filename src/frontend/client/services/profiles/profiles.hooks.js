import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';
import validate from '../../../../hooks/validate';

import roles from '../../../../utils/roles';

import Profile from '../../../../models/Profile';

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      birthday: data.birthday ? new Date(data.birthday) : null,
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      birthday: context[info].birthday ? new Date(context[info].birthday) : null,
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.SALES]),
      transform(Profile),
      validate,
    ],
    update: [
      authorization([roles.SALES]),
      transform(Profile),
      validate,
    ],
    patch: [
      authorization([roles.SALES]),
      transform(Profile),
      validate,
    ],
    remove: [
      authorization([roles.SALES]),
    ],
  },

  after: {
    all: [
      customTransform,
      transform(Profile),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

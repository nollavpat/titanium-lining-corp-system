import items from './items/items.service';
import users from './users/users.service';
import transactions from './transactions/transactions.service';
import profiles from './profiles/profiles.service';
import expenses from './expenses/expenses.service';
import logs from './logs/logs.service';
import sets from './sets/sets.service';
import payments from './payments/payments.service';
import deliveryReceipts from './deliveryReceipts/dr.service';
import constants from './constants/constants.service';
import bankTransfers from './bankTransfers/bt.service';
import commissions from './commissions/commissions.service';
import cm from './commission-payments/cm.service';
import balances from './balances/balances.service';
import bp from './balancePayments/bp.service';

export default function (app) {
  app.configure(users);
  app.configure(items);
  app.configure(profiles);
  app.configure(transactions);
  app.configure(expenses);
  app.configure(logs);
  app.configure(sets);
  app.configure(payments);
  app.configure(deliveryReceipts);
  app.configure(constants);
  app.configure(bankTransfers);
  app.configure(commissions);
  app.configure(cm);
  app.configure(balances);
  app.configure(bp);
}

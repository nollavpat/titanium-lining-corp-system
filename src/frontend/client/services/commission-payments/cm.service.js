import hooks from './cm.hooks';

export default function (app) {
  const service = app.service('api/commissionPayments');

  service.hooks(hooks);
}

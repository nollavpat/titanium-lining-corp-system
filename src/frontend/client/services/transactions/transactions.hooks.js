import Transaction from '../../../../models/Transaction';
import TransactionItem from '../../../../models/TransactionItem';
import Profile from '../../../../models/Profile';
import Payment from '../../../../models/Payment';
import DeliveryReceipt from '../../../../models/DeliveryReceipt';
import Commssion from '../../../../models/Commssion';

import validate from '../../../../hooks/validate';
import cast from '../../../../hooks/cast';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

const fields = [
  'date',
  'transactionNumber',
  'clientId',
  'associateId',
  'seniorConsultantId',
  'distributorId',
  'presentorId',
  'closerId',
  'assistantId',
  'consultantId',
  'transactionItems',
  'terms',
  'paymentIds',
  'commissionIds',
  'deliveryReceiptIds',
  'customTotalPrice',
];

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      date: new Date(data.date),
      client: new Profile(data.client),
      associate: data.associate ? new Profile(data.associate) : null,
      seniorConsultant: data.seniorConsultant ? new Profile(data.seniorConsultant) : null,
      distributor: data.distributor ? new Profile(data.distributor) : null,
      presentor: data.presentor ? new Profile(data.presentor) : null,
      closer: data.closer ? new Profile(data.closer) : null,
      assistant: data.assistantId ? new Profile(data.assistantId) : null,
      commissions: data.commissions ? transform(Commssion)({
        ...context,
        [info]: data.commissions.map(p => ({
          ...p,
          profile: new Profile(p.profile),
        })),
      })[info] : [],
      consultant: new Profile(data.consultant),
      transactionItems: transform(TransactionItem)({
        ...context,
        [info]: data.transactionItems,
      })[info],
      payments: data.payments ? transform(Payment)({
        ...context,
        [info]: data.payments.map(p => ({
          ...p,
          date: new Date(p.date),
          amortizationDate: new Date(p.amortizationDate),
        })),
      })[info] : [],
      deliveryReceipts: data.deliveryReceipts ? transform(DeliveryReceipt)({
        ...context,
        [info]: data.deliveryReceipts.map(p => ({
          ...p,
          date: new Date(p.date),
        })),
      })[info] : [],
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      date: new Date(context[info].date),
      client: new Profile(context[info].client),
      associate: context[info].associate ? new Profile(context[info].associate) : null,
      seniorConsultant: context[info].seniorConsultant
        ? new Profile(context[info].seniorConsultant)
        : null,
      distributor: context[info].distributor ? new Profile(context[info].distributor) : null,
      presentor: context[info].presentor ? new Profile(context[info].presentor) : null,
      closer: context[info].closer ? new Profile(context[info].closer) : null,
      assistant: context[info].assistantId ? new Profile(context[info].assistantId) : null,
      commissions: context[info].commissions ? transform(Commssion)({
        ...context,
        [info]: context[info].commissions.map(p => ({
          ...p,
          profile: new Profile(p.profile),
        })),
      })[info] : null,
      consultant: new Profile(context[info].consultant),
      transactionItems: transform(TransactionItem)({
        ...context,
        [info]: context[info].transactionItems,
      })[info],
      payments: context[info].payments ? transform(Payment)({
        ...context,
        [info]: context[info].payments.map(p => ({
          ...p,
          date: new Date(p.date),
          amortizationDate: new Date(p.amortizationDate),
        })),
      })[info] : null,
      deliveryReceipts: context[info].deliveryReceipts ? transform(DeliveryReceipt)({
        ...context,
        [info]: context[info].deliveryReceipts.map(p => ({
          ...p,
          date: new Date(p.date),
        })),
      })[info] : [],
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.SALES]),
      cast(fields),
      customTransform,
      transform(Transaction),
      validate,
    ],
    update: [
      authorization([roles.SALES, roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.SALES, roles.ACCOUNTING]),
      cast(fields),
      customTransform,
      transform(Transaction),
      validate,
    ],
    remove: [
      authorization([roles.SALES]),
    ],
  },

  after: {
    all: [
      customTransform,
      transform(Transaction),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

import hooks from './transactions.hooks';

export default function (app) {
  const service = app.service('api/transactions');

  service.hooks(hooks);
}

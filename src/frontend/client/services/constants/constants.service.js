import hooks from './constants.hooks';

export default function (app) {
  const service = app.service('api/constants');

  service.hooks(hooks);
}

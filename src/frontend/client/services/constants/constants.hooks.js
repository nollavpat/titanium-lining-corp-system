import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Constant from '../../../../models/Constant';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.SUPERADMIN]),
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
    ],
    remove: [
      authorization([roles.SUPERADMIN]),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

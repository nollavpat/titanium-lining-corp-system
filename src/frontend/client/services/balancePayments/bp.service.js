import hooks from './bp.hooks';

export default function (app) {
  const service = app.service('api/balancePayments');

  service.hooks(hooks);
}

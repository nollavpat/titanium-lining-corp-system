import authorization from '../../../../hooks/authorization';
import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import roles from '../../../../utils/roles';
import BalancePayment from '../../../../models/BalancePayment';


const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      date: new Date(data.date),
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      date: new Date(context[info].date),
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.ACCOUNTING]),
      transform(BalancePayment),
      validate,
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
    ],
    remove: [
      authorization([roles.ACCOUNTING]),
    ],
  },

  after: {
    all: [
      customTransform,
      transform(BalancePayment),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

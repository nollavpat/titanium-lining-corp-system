import hooks from './bt.hooks';

export default function (app) {
  const service = app.service('api/bankTransfers');

  service.hooks(hooks);
}

import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.ACCOUNTING]),
    ],
    update: [
      authorization([roles.ACCOUNTING]),
    ],
    patch: [
      authorization([roles.ACCOUNTING]),
    ],
    remove: [
      authorization([roles.ACCOUNTING]),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

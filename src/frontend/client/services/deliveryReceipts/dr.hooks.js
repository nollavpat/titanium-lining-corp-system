
import validate from '../../../../hooks/validate';
import transform from '../../../../hooks/transform';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import DeliveryReceipt from '../../../../models/DeliveryReceipt';

const customTransform = (context) => {
  const info = context.type === 'before' ? 'data' : 'result';

  let result;

  if (context[info] instanceof Array) {
    result = context[info].map(data => ({
      ...data,
      date: new Date(data.date),
    }));
  } else if (context[info] instanceof Object) {
    result = {
      ...context[info],
      date: new Date(context[info].date),
    };
  }

  return {
    ...context,
    [info]: result,
  };
};

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.SALES]),
      transform(DeliveryReceipt),
      validate,
    ],
    update: [
      authorization([roles.SALES]),
    ],
    patch: [
      authorization([roles.SALES]),
      transform(DeliveryReceipt),
      validate,
    ],
    remove: [
      authorization([roles.SALES]),
    ],
  },

  after: {
    all: [
      customTransform,
      transform(DeliveryReceipt),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

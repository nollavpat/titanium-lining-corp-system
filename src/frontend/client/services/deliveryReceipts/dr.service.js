import hooks from './dr.hooks';

export default function (app) {
  const service = app.service('api/deliveryReceipts');

  service.hooks(hooks);
}

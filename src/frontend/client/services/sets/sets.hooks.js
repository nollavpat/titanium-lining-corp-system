import transform from '../../../../hooks/transform';
import validate from '../../../../hooks/validate';
import authorization from '../../../../hooks/authorization';

import roles from '../../../../utils/roles';

import Set from '../../../../models/Set';

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      authorization([roles.INVENTORY]),
      transform(Set),
      validate,
    ],
    update: [
      authorization([roles.INVENTORY]),
    ],
    patch: [
      authorization([roles.INVENTORY]),
      transform(Set),
      validate,
    ],
    remove: [
      authorization([roles.INVENTORY]),
    ],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

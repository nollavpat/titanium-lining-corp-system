import hooks from './sets.hooks';

export default function (app) {
  const service = app.service('api/sets');

  service.hooks(hooks);
}

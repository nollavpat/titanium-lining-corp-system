import ReactDOM from 'react-dom';
import React from 'react';
import '@babel/polyfill';
import {
  Provider,
} from 'mobx-react';
import {
  configure,
} from 'mobx';

import App from './components/App';

import rootStore from './stores';

configure({ enforceActions: 'observed' });

ReactDOM.render(
  <Provider {...rootStore}>
    <App />
  </Provider>,
  document.getElementById('container'),
);

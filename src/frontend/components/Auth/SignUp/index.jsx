import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Password } from 'primereact/password';
import { InputText } from 'primereact/inputtext';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';


class SignUp extends React.Component {
  @observable user = {
    username: '',
    password: '',
    name: '',
  }

  @action
  updateProperty = (field, value) => {
    this.user = {
      ...this.user,
      [field]: value,
    };
  }

  @action
  clearUser = () => {
    this.user = {
      username: '',
      password: '',
      name: '',
    };
  }

  @action
  handleCreate = async () => {
    const { authStore, viewStore, history } = this.props;

    const result = await authStore.createUser(this.user);

    if (result.success) {
      this.clearUser();
      history.push('/login');
    } else if (result.errorDetails) {
      viewStore.showValidationErrors(result.errorDetails);
    } else {
      viewStore.showError(result.error);
    }
  }

  render() {
    return (
      <div
        style={{
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Card
          title="Titanium Lining Corp"
          style={{
            width: '280px',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '150px',
          }}
        >
          <div style={{ marginBottom: '30px', marginTop: '30px' }}>
            <span className="p-float-label">
              <InputText
                id="float-input"
                type="text"
                value={this.user.username}
                onChange={(e) => { this.updateProperty('username', e.target.value); }}
              />
              <label htmlFor="float-input">Username</label>
            </span>
          </div>
          <div style={{ marginBottom: '30px' }}>
            <span className="p-float-label">
              <Password
                feedback={false}
                id="float-input"
                value={this.user.password}
                onChange={(e) => { this.updateProperty('password', e.target.value); }}
              />
              <label htmlFor="float-input">Password</label>
            </span>
          </div>
          <div style={{ marginBottom: '30px' }}>
            <span className="p-float-label">
              <InputText
                id="float-input"
                type="text"
                value={this.user.name}
                onChange={(e) => { this.updateProperty('name', e.target.value); }}
              />
              <label htmlFor="float-input">Name</label>
            </span>
          </div>
          <div style={{ float: 'right' }}>
            <Button label="Sign up" onClick={this.handleCreate} />
          </div>
        </Card>
      </div>
    );
  }
}

export default inject(
  'authStore',
  'viewStore',
)(observer(SignUp));

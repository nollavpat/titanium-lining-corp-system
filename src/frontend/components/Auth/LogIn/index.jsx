import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Password } from 'primereact/password';
import { InputText } from 'primereact/inputtext';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';


class LogIn extends React.Component {
  @observable user = {
    username: '',
    password: '',
  };

  @action
  componentWillUnmount() {
    this.user = {
      username: '',
      password: '',
    };
  }

  @action
  updateProperty = (field, value) => {
    this.user = {
      ...this.user,
      [field]: value,
    };
  }

  @action
  handleLogin = async () => {
    const { authStore, history } = this.props;

    await authStore.login(this.user);
    history.push('/');
  }

  render() {
    return (
      <div
        style={{
          width: '100%',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Card
          title="Titanium Lining Corp"
          style={{
            width: '280px',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '200px',
          }}
        >
          <div style={{ marginBottom: '30px', marginTop: '30px' }}>
            <span className="p-float-label">
              <InputText
                id="float-input"
                type="text"
                value={this.user.username}
                onChange={(e) => { this.updateProperty('username', e.target.value); }}
              />
              <label htmlFor="float-input">Username</label>
            </span>
          </div>
          <div style={{ marginBottom: '30px' }}>
            <span className="p-float-label">
              <Password
                feedback={false}
                id="float-input"
                value={this.user.password}
                onChange={(e) => { this.updateProperty('password', e.target.value); }}
              />
              <label htmlFor="float-input">Password</label>
            </span>
          </div>
          <div>
            <span style={{ float: 'right' }}><Button label="Log in" onClick={this.handleLogin} /></span>
            <span style={{ float: 'left' }}><a href="/#/signup">Sign up</a></span>
          </div>
        </Card>
      </div>
    );
  }
}

export default inject('authStore')(observer(LogIn));

import React from 'react';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';

import {
  observer,
  inject,
} from 'mobx-react';

import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { Menu } from 'primereact/menu';
import { Growl } from 'primereact/growl';

import {
  HashRouter as Router,
} from 'react-router-dom';

import RouteConfig from './RouteConfig';

class App extends React.Component {
  async componentDidMount() {
    const { authStore } = this.props;

    // try {
    //   if (window.localStorage['feathers-jwt']) {
    //     await authStore.login();
    //   }
    // } catch (error) {
    //   console.error(error);
    // } finally {
    //   await authStore.finishAuthenticating();
    // }
    await authStore.finishAuthenticating();

    window.addEventListener('beforeunload', (ev) => {
      ev.preventDefault();

      return Promise.resolve(authStore.logout());
    });
  }

  render() {
    const { authStore, viewStore } = this.props;

    const items = [
      {
        label: 'Account',
        items: [{
          label: 'Sign Out',
          icon: 'pi pi-fw pi-power-off',
          command: async () => { await authStore.logout(); },
        }],
      },
    ];
    const adminItems = [
      {
        label: 'Pages',
        items: [
          {
            label: 'Admin',
            command: () => { window.location.hash = '/admin'; },
          },
          {
            label: 'Sales',
            command: () => { window.location.hash = '/sales'; },
          },
          {
            label: 'Accounting',
            command: () => { window.location.hash = '/accounting'; },
          },
          {
            label: 'Inventory',
            command: () => { window.location.hash = '/inventory'; },
          },
        ],
      },
    ];
    let menu;

    return (
      <div>
        { authStore.doneAuthenticating && (
          <div>
            {authStore.hasUser && (
              <Toolbar>
                <div className="p-toolbar-group-left">
                  <span
                    style={{
                      fontSize: '24px',
                      margin: '0px',
                      fontWeight: 'bold',
                    }}
                  >
                    {viewStore.title || ''}
                  </span>
                </div>
                <div className="p-toolbar-group-right">
                  <Menu
                    model={authStore.isAdmin ? [...adminItems, ...items] : items}
                    popup
                    ref={(el) => { menu = el; }}
                  />
                  <Button icon="pi pi-user" onClick={(event) => { menu.toggle(event); }} />
                </div>
              </Toolbar>
            )}
            <Growl ref={(el) => { viewStore.growl = el; }} />
            <div style={{ margin: '8px' }}>
              <Router>
                <RouteConfig />
              </Router>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default inject(
  'authStore',
  'inventoryStore',
  'viewStore',
)(observer(App));

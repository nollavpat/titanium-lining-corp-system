import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';

import roles from '../../../../utils/roles';

class ManageUsers extends React.Component {
  @observable user = null;

  @observable showEdit = false;

  @action
  updateProperty = (key, value) => {
    this.user = {
      ...this.user,
      [key]: value,
    };
  };

  @action
  clearUser = () => {
    this.user = null;
    this.showEdit = false;
  }

  render() {
    const { adminStore, viewStore } = this.props;
    const dialogFooter = (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        <Button
          label="Delete"
          icon="pi pi-trash"
          onClick={async () => {
            await adminStore.removeUser(this.user._id);
            this.clearUser();
          }}
        />
        <Button
          label="Save"
          icon="pi pi-check"
          onClick={async () => {
            const result = await adminStore.updateUser(this.user._id, this.user);

            if (result.success) {
              this.clearUser();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );
    const options = [
      { label: roles.NONE, value: roles.NONE },
      { label: roles.INVENTORY, value: roles.INVENTORY },
      { label: roles.ACCOUNTING, value: roles.ACCOUNTING },
      { label: roles.SALES, value: roles.SALES },
      { label: roles.ADMIN, value: roles.ADMIN },
      { label: roles.SUPERADMIN, value: roles.SUPERADMIN },
    ];

    return (
      <div>
        <DataTable
          value={adminStore.users}
          paginator
          rows={10}
          selectionMode="single"
          selection={this.user}
          onSelectionChange={action((e) => { this.user = e.value; })}
          onRowSelect={action(() => {
            this.showEdit = true;
          })}
        >
          <Column
            field="username"
            header="Username"
          />
          <Column
            field="name"
            header="Name"
          />
          <Column
            field="role"
            header="Role"
          />
          <Column
            field="pin"
            header="Password"
          />
        </DataTable>
        <Dialog
          visible={this.showEdit}
          width="200px"
          header={`Edit: ${this.user ? this.user.name : ''}`}
          footer={dialogFooter}
          onHide={action(() => {
            this.showEdit = false;
          })}
        >
          {this.user && (
            <div>
              <div className="p-grid p-fluid">
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Role</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <Dropdown
                    options={options}
                    onChange={(e) => { this.updateProperty('role', e.value); }}
                    value={this.user.role}
                  />
                </div>
              </div>
              <div className="p-grid p-fluid">
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Password</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={this.user.pin}
                    onChange={(e) => { this.updateProperty('pin', e.target.value); }}
                  />
                </div>
              </div>
            </div>
          )}
        </Dialog>
      </div>
    );
  }
}

export default inject(
  'adminStore',
  'viewStore',
)(observer(ManageUsers));

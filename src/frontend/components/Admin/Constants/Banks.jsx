import React from 'react';

import {
  observable,
  action,
  computed,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';

class Banks extends React.Component {
  @observable bank = null;

  @observable showEdit = false;

  @observable title = '';

  @computed
  get details() {
    return {
      ...this.bank,
      balance: +this.bank.balance,
      type: 'BANK',
    };
  }

  @computed
  get isInvalid() {
    if (!this.bank) {
      return true;
    }

    return this.bank.balance < 0 || this.bank.value === '';
  }

  @action
  updateProperty = (key, value) => {
    this.bank = {
      ...this.bank,
      [key]: value,
    };
  };

  @action
  clearBank = () => {
    this.user = null;
    this.showEdit = false;
  }

  render() {
    const { adminStore, viewStore } = this.props;
    const dialogFooter = (
      <div style={{ display: 'flex', justifyContent: 'space-between' }}>
        {this.title === 'Edit Bank' && (
          <Button
            label="Delete"
            icon="pi pi-trash"
            onClick={async () => {
              await adminStore.removeConstant(this.bank._id);
              this.clearBank();
            }}
          />
        )}
        {this.title === 'Add Bank' && (
          <Button
            label={this.title === 'Add Bank' ? 'Add' : 'Save'}
            icon={this.title === 'Add Bank' ? 'pi pi-plus' : 'pi pi-check'}
            disabled={this.isInvalid}
            onClick={async () => {
              let result;

              if (this.title === 'Add Bank') {
                result = await adminStore.addConstant(this.details);
              } else {
                result = await adminStore.updateConstant(this.details._id, this.details);
              }

              if (result.success) {
                this.clearBank();
              } else if (result.errorDetails) {
                viewStore.showValidationErrors(result.errorDetails);
              } else {
                viewStore.showError(result.error);
              }
            }}
          />
        )}
      </div>
    );

    const footer = (
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button
          style={{ float: 'right' }}
          label="Add"
          icon="pi pi-plus"
          onClick={action(() => {
            this.bank = { value: '', amount: 0 };
            this.title = 'Add Bank';
            this.showEdit = true;
          })}
        />
      </div>
    );

    return (
      <div>
        <DataTable
          value={adminStore.adminBanks}
          paginator
          rows={10}
          selectionMode="single"
          selection={this.bank}
          onSelectionChange={action((e) => { this.bank = e.value; })}
          onRowSelect={action(() => {
            this.showEdit = true;
            this.title = 'Edit Bank';
          })}
          footer={footer}
        >
          <Column
            field="value"
            header="Bank"
          />
        </DataTable>
        <Dialog
          visible={this.showEdit}
          width="200px"
          header={this.title}
          footer={dialogFooter}
          onHide={action(() => {
            this.showEdit = false;
          })}
        >
          {this.bank && (
            <div>
              <div className="p-grid p-fluid">
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Name</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={this.bank.value}
                    disabled={this.title === 'Edit Bank'}
                    onChange={(e) => { this.updateProperty('value', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Current Balance</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={this.bank.balance}
                    keyfilter="pnum"
                    disabled={this.title === 'Edit Bank'}
                    onChange={(e) => { this.updateProperty('balance', e.target.value); }}
                  />
                </div>
              </div>
            </div>
          )}
        </Dialog>
      </div>
    );
  }
}

export default inject(
  'adminStore',
  'viewStore',
)(observer(Banks));

import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import Banks from './Banks';

class Constants extends React.Component {
  @observable bank = null;

  render() {
    return (
      <div>
        <Banks />
      </div>
    );
  }
}

export default inject(
  'adminStore',
  'viewStore',
)(observer(Constants));

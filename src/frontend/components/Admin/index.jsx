import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  runInAction,
  observable,
  action,
} from 'mobx';

import {
  TabView,
  TabPanel,
} from 'primereact/tabview';

import ManageUsers from './ManageUsers';
import Constants from './Constants';

class Admin extends React.Component {
  @observable page = null;

  async componentDidMount() {
    const { adminStore, viewStore } = this.props;

    await adminStore.getNonSuperAdmin();

    runInAction(() => {
      const currentPage = window.localStorage.getItem('adminPage') || 0;

      viewStore.changeTitle('Admin');
      this.page = Number(currentPage);
    });
  }

  render() {
    const { adminStore } = this.props;

    return (
      <div>
        {(adminStore.constants && adminStore.users) && (
          <TabView
            className="p-tabview"
            activeIndex={this.page}
            onTabChange={action((e) => {
              this.page = e.index;
              window.localStorage.setItem('adminPage', e.index);
            })}
          >
            <TabPanel header="System Constants">

              <Constants />
            </TabPanel>
            <TabPanel header="Manage Users">
              <ManageUsers />
            </TabPanel>
          </TabView>
        )}
      </div>
    );
  }
}

export default inject(
  'adminStore',
  'viewStore',
)(observer(Admin));

import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { AutoComplete } from 'primereact/autocomplete';

import ItemView from './ItemView';

import { beautifyPrice } from '../../../../utils/helper';

class Items extends React.Component {
  @observable item = null;

  @observable displayDialog = false;

  @observable selectedItem = false;

  @observable willAdd = false;

  @observable bi = null;

  @observable openDialog = false;

  @observable profiles = [];

  @observable viewBorrowers = false;

  @observable openedItem = null;

  @action
  componentDidMount = () => {
    const { salesStore } = this.props;

    this.profiles = salesStore.validProfiles;
  }

  @action
  viewBi = (itemId) => {
    const { inventoryStore } = this.props;
    const item = inventoryStore.inventory.find(i => i._id === itemId);

    this.viewBorrowers = true;
    this.openedItem = item;
  }

  @action
  clearViewBi = () => {
    this.viewBorrowers = false;
    this.openedItem = null;
  }

  @action
  addBi = (_id) => {
    const { inventoryStore } = this.props;
    const item = inventoryStore.inventory.find(i => i._id === _id);
    this.openedItem = item;
    this.bi = {
      itemId: _id,
      profile: null,
      quantity: 0,
      date: new Date(),
    };
    this.openDialog = true;
  }

  @action
  clearBi = () => {
    this.bi = null;
    this.openedItem = null;
    this.openDialog = false;
  }

  @action
  resetState = () => {
    this.displayDialog = false;
    this.item = null;
    this.willAdd = false;
  }

  @action
  addNew = () => {
    this.item = {
      code: '',
      description: '',
      price: 0,
      quantity: 0,
    };
    this.displayDialog = true;
    this.willAdd = true;
  }

  @action
  updateProperty = (property, value) => {
    this.item = {
      ...this.item,
      [property]: value,
    };
  }

  @action
  updateProperty1 = (property, value) => {
    this.bi = {
      ...this.bi,
      [property]: value,
    };
  }

  @action
  deleteItem = async () => {
    const { inventoryStore } = this.props;

    await inventoryStore.deleteItem(this.item._id);

    this.resetState();
  }

  @action
  saveItem = async () => {
    const { inventoryStore, viewStore } = this.props;
    let result;
    if (this.willAdd) {
      result = await inventoryStore.createItem({
        ...this.item,
        price: +this.item.price,
        quantity: +this.item.quantity,
      });
    } else {
      result = await inventoryStore.updateItem(this.item._id, {
        ...this.item,
        price: +this.item.price,
        quantity: +this.item.quantity,
      });
    }

    if (result.success) {
      this.resetState();
    } else if (result.errorDetails) {
      viewStore.showValidationErrors(result.errorDetails);
    } else {
      viewStore.showError(result.error);
    }
  }

  @action
  onItemSelect = (e) => {
    this.item = JSON.parse(JSON.stringify(e.data));
    this.displayDialog = true;
  }

  @action
  hideModal = () => {
    this.displayDialog = false;
    this.willAdd = false;
  }

  @action
  handleComplete = (event) => {
    const { salesStore } = this.props;

    const results = salesStore.validProfiles.filter(p => (
      p.name.toLowerCase().startsWith(event.query.toLowerCase())
    ));

    this.profiles = results;
  }

  itemTemplate = profile => (
    <Button label={profile.name} tooltip={profile.homeAddress} className="p-button-secondary" />
  )

  render() {
    const { inventoryStore, viewStore } = this.props;

    const footer = (
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button style={{ float: 'right' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
      </div>
    );

    const PriceTemplate = ({ price }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(price)}
      </div>
    );

    const RemarksTemplate = ({ borrowedItems, _id, totalBorrowedItems }) => (
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <div>{totalBorrowedItems} borrowed</div>
        <Button
          label="View"
          disabled={borrowedItems.length === 0}
          onClick={() => { this.viewBi(_id); }}
        />
        <Button
          label="Add Borrower"
          onClick={() => { this.addBi(_id); }}
        />
      </div>
    );

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Add"
          disabled={this.bi && (!this.bi.profile || this.bi.quantity <= 0)}
          onClick={async () => {
            const {
              itemId,
              quantity,
              profile,
              date,
            } = this.bi;
            const data = {
              date,
              quantity: +quantity,
              profileId: profile ? profile._id : null,
            };
            const result = await inventoryStore.addBorrower(itemId, data);

            if (result.success) {
              this.clearBi();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <div>
        <DataTable
          value={inventoryStore.inventory}
          paginator
          rows={10}
          footer={footer}
          selectionMode="single"
          onRowSelect={this.onItemSelect}
          resizableColumns
        >
          <Column
            field="code"
            header="Code"
            filter
            filterPlaceholder="Search"
            filterMatchMode="equals"
            style={{ width: '15%' }}
          />
          <Column
            field="description"
            header="Description"
            filter
            filterPlaceholder="Search"
            filterMatchMode="contains"
            style={{ width: '30%' }}
          />
          <Column
            field="price"
            header="Price"
            body={PriceTemplate}
            sortable
            style={{ width: '15%' }}
          />
          <Column
            field="availableQuantity"
            header="Quanitity"
            sortable
            style={{ width: '15%' }}
          />
          <Column
            field="borrowedItems"
            header="Borrowed Items"
            body={RemarksTemplate}
            style={{ width: '25%' }}
          />
        </DataTable>
        <Dialog
          visible={this.openDialog}
          style={{ width: '300px' }}
          header="Add Borrower"
          modal
          footer={dialogFooter}
          onHide={action(() => { this.openDialog = false; })}
        >
          {this.bi && (
            <div>
              <div
                style={{
                  marginBottom: '8px',
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div style={{ fontWeight: 700 }}>Borrower: </div>
                <AutoComplete
                  field="name"
                  suggestions={this.profiles}
                  value={this.bi.profile}
                  onChange={action((e) => { this.updateProperty1('profile', e.value); })}
                  completeMethod={this.handleComplete}
                  itemTemplate={this.itemTemplate}
                />
              </div>
              <div
                style={{
                  marginBottom: '8px',
                  display: 'flex',
                  justifyContent: 'space-between',
                }}
              >
                <div style={{ fontWeight: 700 }}>Quantity: </div>
                <InputText
                  keyfilter="num"
                  value={this.bi.quantity}
                  onChange={(e) => {
                    if (+e.target.value <= this.openedItem.availableQuantity) {
                      this.updateProperty1('quantity', e.target.value);
                    }
                  }}
                />
              </div>
            </div>
          )}
        </Dialog>
        <Dialog
          visible={this.viewBorrowers}
          style={{ width: '500px' }}
          header="View Borrowers"
          modal
          onHide={action(() => { this.viewBorrowers = false; })}
        >
          {this.openedItem && (
            <div>
              {this.openedItem.borrowedItems.map((bi, i) => (
                <div
                  key={parseInt(i.toString(), 2)}
                  style={{
                    padding: '4px',
                    borderTop: i > 0 ? '0.5px solid #c8c8c8' : '',
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <div>
                    <div style={{ fontWeight: 600 }}>
                      {bi.profile.name} borrowed {bi.quantity} pcs
                    </div>
                    <div style={{ marginLeft: '8px' }}>{bi.profile.cellphoneNumber}</div>
                    <div style={{ marginLeft: '8px' }}>{bi.profile.homeAddress}</div>
                  </div>
                  <Button
                    label="Return"
                    onClick={async () => {
                      const a = this.openedItem.borrowedItems.slice();
                      a.splice(i, 1);

                      const data = a.map(b => ({
                        quantity: b.quantity,
                        profileId: b.profileId,
                      }));
                      const itemId = this.openedItem._id;

                      const result = await inventoryStore.removeBorrower(itemId, data);

                      if (result.success) {
                        this.clearViewBi();
                      } else if (result.errorDetails) {
                        viewStore.showValidationErrors(result.errorDetails);
                      } else {
                        viewStore.showError(result.error);
                      }
                    }}
                  />
                </div>
              ))}
            </div>
          )}
        </Dialog>
        <ItemView
          item={this.item}
          displayDialog={this.displayDialog}
          willAdd={this.willAdd}
          resetState={this.resetState}
          deleteItem={this.deleteItem}
          saveItem={this.saveItem}
          updateProperty={this.updateProperty}
        />
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'viewStore',
  'salesStore',
)(observer(Items));

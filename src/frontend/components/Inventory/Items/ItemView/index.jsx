import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';

class ItemView extends React.Component {
  @observable quantity = 0;

  @action
  handleChange = (e) => {
    this.quantity = e.target.value;
  };

  render() {
    const {
      item,
      displayDialog,
      willAdd,
      saveItem,
      deleteItem,
      updateProperty,
      resetState,
    } = this.props;

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label="Save"
          icon="pi pi-check"
          onClick={() => {
            saveItem();
          }}
        />
        {
          !willAdd
            && <Button label="Delete" icon="pi pi-times" onClick={deleteItem} />
        }
      </div>
    );

    return (
      <Dialog
        visible={displayDialog}
        width="300px"
        header="Item"
        modal
        footer={dialogFooter}
        onHide={resetState}
      >
        {item && (
          <div className="p-grid p-fluid">
            <div className="p-col-4" style={{ padding: '.75em' }}>
              <label>Code</label>
            </div>
            <div className="p-col-8" style={{ padding: '.5em' }}>
              <InputText
                onChange={(e) => { updateProperty('code', e.target.value); }}
                value={item.code}
              />
            </div>
            <div className="p-col-4" style={{ padding: '.75em' }}>
              <label>Description</label>
            </div>
            <div className="p-col-8" style={{ padding: '.5em' }}>
              <InputTextarea
                rows={2}
                cols={30}
                onChange={(e) => { updateProperty('description', e.target.value); }}
                value={item.description}
              />
            </div>
            <div className="p-col-4" style={{ padding: '.75em' }}>
              <label>Price</label>
            </div>
            <div className="p-col-8" style={{ padding: '.5em' }}>
              <InputText
                keyfilter="int"
                onChange={(e) => { updateProperty('price', e.target.value); }}
                value={item.price}
              />
            </div>
            {(() => {
              if (willAdd) {
                return (
                  <div>
                    <div className="p-col-4" style={{ padding: '.75em' }}>
                      <label>Quanitity</label>
                    </div>
                    <div className="p-col-8" style={{ padding: '.5em' }}>
                      <InputText
                        keyfilter="int"
                        id="quantity"
                        onChange={(e) => { updateProperty('quantity', e.target.value); }}
                        value={item.quantity}
                      />
                    </div>
                  </div>
                );
              }

              return (
                <div>
                  <div className="p-col-4" style={{ padding: '.75em' }}>
                    <label>Quanitity: {item.quantity}</label>
                  </div>
                  <div className="p-col-12 p-md-4">
                    <div className="p-inputgroup" style={{ padding: '.5em' }}>
                      <InputText
                        keyfilter="int"
                        onChange={this.handleChange}
                        value={this.quantity}
                      />
                      <Button
                        icon="pi pi-plus"
                        className="p-button-primary"
                        onClick={() => {
                          updateProperty('quantity', Number(item.quantity) + Number(this.quantity));
                        }}
                      />
                      <Button
                        icon="pi pi-minus"
                        className="p-button-danger"
                        onClick={() => {
                          updateProperty('quantity', Number(item.quantity) - Number(this.quantity));
                        }}
                      />
                    </div>
                  </div>
                </div>
              );
            })()}
          </div>
        )}
      </Dialog>
    );
  }
}

export default inject('viewStore')(observer(ItemView));

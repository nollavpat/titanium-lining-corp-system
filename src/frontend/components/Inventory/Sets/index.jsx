import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

import SetView from './SetView';

class Sets extends React.Component {
  @observable displayDialog = false;

  @observable set = undefined;

  @observable selectedSet = undefined;

  @action
  hideModal = () => {
    this.set = undefined;
    this.selectedSet = undefined;
    this.displayDialog = false;
    this.willAdd = false;
  }

  @action
  addNew = () => {
    this.set = {
      description: '',
      code: '',
      componentsMap: [],
      less: 0,
    };

    this.displayDialog = true;
    this.willAdd = true;
  }

  @action
  saveSet = async () => {
    const { inventoryStore, viewStore } = this.props;
    let result;

    if (this.willAdd) {
      result = await inventoryStore.createSet({
        ...this.set,
        less: +this.set.less,
      });
    } else {
      result = await inventoryStore.updateSet(this.set._id, {
        ...this.set,
        less: +this.set.less,
      });
    }

    if (result.success) {
      this.hideModal();
    } else if (result.errorDetails) {
      viewStore.showValidationErrors(result.errorDetails);
    } else {
      viewStore.showError(result.error);
    }
  }

  @action
  deleteSet = async () => {
    const { inventoryStore } = this.props;

    await inventoryStore.deleteSet(this.set._id);

    this.hideModal();
  }

  @action
  updateProperty = (property, value) => {
    this.set = {
      ...this.set,
      [property]: value,
    };
  }

  render() {
    const { inventoryStore } = this.props;

    const footer = (
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button style={{ float: 'right' }} label="Add" icon="pi pi-plus" onClick={this.addNew} />
      </div>
    );

    const PriceTemplate = ({ setPrice }) => (
      <div style={{ textAlign: 'right' }}>
        {setPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
      </div>
    );

    const calculateSetTotal = (setId) => {
      if (!inventoryStore.sets) return 0;

      const {
        originalPrice,
        lessedPrice,
        less,
      } = inventoryStore.sets.find(set => set._id === setId);

      return `${originalPrice} => ${lessedPrice} [lessed by ${less}]`;
    };

    const headerTemplate = (item) => {
      const {
        code,
        description,
        quantity,
      } = inventoryStore.sets.find(set => set._id === item.setId);

      return (
        <div>
          <span style={{ fontWeight: '800' }}>[{code}]</span>
          <span style={{ marginLeft: '6px', fontWeight: '800' }}>{description}</span>
          <span style={{ float: 'right' }}>
            <span
              style={{
                fontWeight: '800',
                marginLeft: '6px',
              }}
            >
              {quantity}
            </span>
            <span
              style={{
                marginLeft: '4px',
                fontWeight: '800',
              }}
            >
              left
            </span>
          </span>
        </div>);
    };

    const footerTemplate = item => ([
      <td
        key={`${item._id}-label`}
        colSpan="3"
        style={{ textAlign: 'right', fontWeight: '800' }}
      >
        Total Price
      </td>,
      <td key={`${item._id}-value`}>
        <PriceTemplate setPrice={calculateSetTotal(item.setId)} />
      </td>,
    ]);
    return (
      <div>
        <DataTable
          value={inventoryStore.inventoryInSets}
          rowGroupMode="subheader"
          sortField="setId"
          sortOrder={1}
          groupField="setId"
          footer={footer}
          rowGroupFooterTemplate={footerTemplate}
          rowGroupHeaderTemplate={headerTemplate}
          onSelectionChange={action((e) => {
            const item = e.value;

            this.set = JSON.parse(
              JSON.stringify(inventoryStore.sets.find(set => set._id === item.setId)),
            );
            this.displayDialog = true;
          })}
          selectionMode="single"
        >
          <Column field="code" header="Code" style={{ width: '20%' }} />
          <Column field="description" header="Description" style={{ width: '30%' }} />
          <Column field="setQuantity" header="Set Quantity" style={{ width: '20%' }} />
          <Column field="setPrice" header="Price" body={PriceTemplate} style={{ width: '30%' }} />
        </DataTable>

        <SetView
          {...this}
          displayDialog={this.displayDialog}
          hideModal={this.hideModal}
          updateProperty={this.updateProperty}
          saveSet={this.saveSet}
          deleteSet={this.deleteSet}
        />
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'viewStore',
)(observer(Sets));

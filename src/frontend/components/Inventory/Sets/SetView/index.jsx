import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { MultiSelect } from 'primereact/multiselect';

import Item from './Item';

class SetView extends React.Component {
  @observable items = [];

  @observable itemQuantity = [];

  @observable initialLoad = false;

  @action
  resetState = () => {
    this.items = [];
    this.itemQuantity = [];
    this.initialLoad = false;
  }

  @action
  handleMultipleSelect = (e) => {
    this.itemQuantity = e.value.map((item) => {
      const currentItem = this.itemQuantity.find(i => i.itemId === item.itemId);
      if (currentItem) {
        return currentItem;
      }

      return {
        itemId: item.itemId,
        quantity: 0,
      };
    });

    this.items = e.value;
  }

  @action
  updateQuantity = (id, quantity) => {
    this.itemQuantity = this.itemQuantity.map((item) => {
      if (id === item.itemId) {
        return {
          ...item,
          quantity,
        };
      }

      return item;
    });
  }

  @action
  setItemQuantity = (itemOptions, componentsMap) => {
    const values = itemOptions.map(i => i.value);
    const cIds = componentsMap.map(c => c.itemId);

    const f = values.filter(v => cIds.includes(v.itemId));

    this.itemQuantity = f.map((item) => {
      const currentItem = this.itemQuantity.find(i => i.itemId === item.itemId);
      if (currentItem) {
        return currentItem;
      }

      return {
        itemId: item.itemId,
        quantity: componentsMap.find(c => c.itemId === item.itemId).quantity,
      };
    });

    this.items = f;
    this.initialLoad = true;
  }

  render() {
    const {
      displayDialog,
      hideModal,
      inventoryStore,
      updateProperty,
      set,
      saveSet,
      willAdd,
      deleteSet,
    } = this.props;

    const itemOptions = inventoryStore.inventory.map(i => ({
      label: i.description,
      value: {
        itemId: i._id,
        description: i.description,
      },
    }));

    if (set && !this.initialLoad) {
      this.setItemQuantity(itemOptions, set.componentsMap);
    }

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label="Save"
          icon="pi pi-check"
          onClick={async () => {
            updateProperty('componentsMap', this.itemQuantity);
            await saveSet();
            this.resetState();
          }}
        />
        {
          !willAdd && (
            <Button
              label="Delete"
              icon="pi pi-times"
              onClick={async () => {
                await deleteSet();
                this.resetState();
              }}
            />
          )
        }
      </div>
    );

    return (
      <Dialog
        visible={displayDialog}
        width="630px"
        header="Set"
        modal
        onHide={() => {
          hideModal();
          this.resetState();
        }}
        footer={dialogFooter}
      >
        {set && (
          <div style={{ display: 'flex' }}>
            <div className="p-col p-grid p-fluid">
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Code</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputText
                  onChange={(e) => { updateProperty('code', e.target.value); }}
                  value={set.code}
                />
              </div>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Description</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputTextarea
                  rows={2}
                  cols={30}
                  onChange={(e) => { updateProperty('description', e.target.value); }}
                  value={set.description}
                />
              </div>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Less</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputText
                  keyfilter="int"
                  onChange={(e) => { updateProperty('less', e.target.value); }}
                  value={set.less}
                />
              </div>
            </div>
            <div className="p-col p-grid p-fluid">
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Components</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <MultiSelect
                  options={itemOptions}
                  filter
                  value={this.items}
                  onChange={this.handleMultipleSelect}
                  style={{
                    width: '288px',
                    whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',
                  }}
                />
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <div
                  style={{
                    overflowY: 'scroll',
                    height: '230px',
                    border: '1px solid #a6a6a6',
                    width: '288px',
                  }}
                >
                  {
                    this.items.map(item => (
                      <Item
                        key={item.itemId}
                        item={item}
                        quantity={this.itemQuantity.find(i => i.itemId === item.itemId).quantity}
                        updateQuantity={this.updateQuantity}
                      />
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
        )}
      </Dialog>
    );
  }
}

export default inject('inventoryStore')(observer(SetView));

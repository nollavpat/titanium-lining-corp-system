import React from 'react';

import { observer } from 'mobx-react';

import { InputText } from 'primereact/inputtext';

const Item = ({ item, updateQuantity, quantity }) => (
  <div
    style={{ display: 'flex', alignItems: 'flex-start', padding: '8px' }}
  >
    <InputText
      keyfilter="int"
      style={{ width: '36px' }}
      value={quantity}
      onChange={(e) => {
        updateQuantity(item.itemId, e.target.value);
      }}
    />
    <div style={{ marginLeft: '6px', borderBottom: '1px solid #a6a6a6' }}>
      {item.description}
    </div>
  </div>
);

export default observer(Item);

import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import {
  TabView,
  TabPanel,
} from 'primereact/tabview';

import Items from './Items';
import Sets from './Sets';
import Reports from './Reports';
import Logs from '../Logs';

class Inventory extends React.Component {
  @observable page = 0;

  @action
  componentDidMount = () => {
    const { viewStore } = this.props;
    const currentPage = window.localStorage.getItem('inventoryPage') || 0;

    viewStore.changeTitle('Inventory');
    this.page = Number(currentPage);
  }

  render() {
    const { inventoryStore, authStore } = this.props;
    const { inventory } = inventoryStore;

    return (
      <div>
        { (inventory) && (
          <TabView
            className="p-tabview"
            activeIndex={this.page}
            onTabChange={action((e) => {
              this.page = e.index;
              window.localStorage.setItem('inventoryPage', e.index);
            })}
          >
            <TabPanel header="Items">
              <Items />
            </TabPanel>
            {/* <TabPanel header="Sets">
              <Sets />
            </TabPanel> */}
            <TabPanel header="Reports">
              <Reports />
            </TabPanel>
            <TabPanel header="Logs" disabled={!authStore.isAdmin}>
              <Logs myLogs={['items', 'sets']} full={false} />
            </TabPanel>
          </TabView>
        )}
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'viewStore',
  'authStore',
)(observer(Inventory));

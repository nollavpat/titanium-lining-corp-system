import React from 'react';
import ReactToPrint from 'react-to-print';

import {
  inject,
  observer,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Calendar } from 'primereact/calendar';
import { Toolbar } from 'primereact/toolbar';

import { timeEndDate, timeStartDate } from '../../../../utils/helper';

// const d1 = new Date('February 11, 2019');
// const d2 = new Date('February 28, 2019');


class Reports extends React.Component {
  @observable range = null;

  @computed
  get rangeLogs() {
    const { adminStore } = this.props;
    const [d1, d2] = this.range;

    return adminStore.logs.filter(l => l.date > timeStartDate(d1) && l.date < timeEndDate(d2));
  }

  @computed
  get rangeDr() {
    const { salesStore } = this.props;
    const [d1, d2] = this.range;

    return salesStore.transactions.reduce((acc, t) => {
      const drs = t.deliveryReceipts.filter(d => d.date > timeStartDate(d1) && d.date < timeEndDate(d2));

      return [...acc, ...drs];
    }, []);
  }

  @computed
  get finalInventoryReport() {
    const { inventoryStore } = this.props;
    const [d1, d2] = this.range;

    return inventoryStore.inventory.map((i) => {
      let begInv = this.rangeLogs.reduce((acc, l) => {
        if (l.prevState._id === i._id && (l.method === 'create' || l.method === 'patch')) {
          if (acc === null) {
            return l.prevState.quantity;
          }

          if (acc.date > l.date) {
            return l.prevState.quantity;
          }
        }

        return acc;
      }, null);
      begInv = !begInv ? i.availableQuantity : begInv;
      const stocksReceived = this.rangeLogs.reduce((acc, l) => {
        if (l.prevState._id === i._id && l.method === 'patch') {
          if (acc.date > l.date) {
            return l.prevState.quantity;
          }
          return acc + l.prevState.prevQuantity - l.prevState.quantity;
        }

        return acc;
      }, 0);
      const sales = this.rangeDr.reduce((acc, l) => {
        let total = 0;

        const ti = l.transactionItems.find(t => t.transactionItemId === i._id && !t.isGift);

        if (ti) {
          total += ti.items;
        }

        return acc + total;
      }, 0);
      const setSales = this.rangeDr.reduce((acc, l) => {
        let total = 0;

        const cms = l.transactionItems
          .filter(t => t.isSet)
          .reduce((acc1, tii) => [
            ...acc1,
            ...tii.componentsMap.map(cmti => ({ ...cmti, total: cmti.quantity * tii.items })),
          ], []);
        if (cms.length === 0) {
          return acc;
        }
        const myItem = cms.find(m => m.itemId === i._id);
        if (myItem) {
          total += myItem.total;
        }

        return acc + total;
      }, 0);
      const freeGift = this.rangeDr.reduce((acc, l) => {
        let total = 0;
        const ti = l.transactionItems.find(t => t.transactionItemId === i._id && t.isGift);

        if (ti) {
          total += ti.items;
        }

        return acc + total;
      }, 0);
      const borrowedItems = i.borrowedItems
        .filter(b => new Date(b.date) > timeStartDate(d1) && new Date(b.date) < timeEndDate(d2))
        .reduce((acc, b) => acc + b.quantity, 0);

      const total = setSales + sales + freeGift + borrowedItems;

      return {
        ...i,
        begInv,
        freeGift,
        borrowedItems,
        sales,
        setSales,
        stocksReceived,
        total,
        endInv: begInv + stocksReceived - total,
      };
    });
  }

  render() {
    const SaleTemplate = ({ sales, setSales }) => (
      <div>
        {`${sales + setSales}  `}
        {setSales !== 0 && <span>{`[${setSales} from sets]`}</span>}
      </div>
    );

    return (
      <div>
        <Toolbar>
          <div className="p-toolbar-group-right">
            <Calendar
              value={this.range}
              onChange={action((e) => {
                this.range = e.value;
              })}
              selectionMode="range"
              readOnlyInput
              dateFormat="MM dd, yy"
              inputStyle={{ width: '300px', textAlign: 'center' }}
              placeholder="Date From - Date To"
              showButtonBar
            />
          </div>
        </Toolbar>
        {this.range && (
          <div>
            <ReactToPrint
              trigger={() => <a href="#">Print this out!</a>}
              content={() => this.componentRef}
            />
            <DataTable
              value={this.finalInventoryReport}
              ref={(el) => { this.componentRef = el; }}
            >
              <Column field="code" header="Code" style={{ width: '10%' }} />
              <Column field="description" header="Description" style={{ width: '20%' }} />
              <Column field="begInv" header="Beg Inv." />
              <Column field="stocksReceived" header="Stock Received" />
              <Column field="sales" header="Sales" body={SaleTemplate} />
              <Column field="freeGift" header="Free Gift" />
              <Column field="borrowedItems" header="Borrowed Items" />
              <Column field="total" header="Items Out" />
              <Column field="endInv" header="Ending Inv." />
            </DataTable>
          </div>
        )}
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'adminStore',
  'salesStore',
)(observer(Reports));

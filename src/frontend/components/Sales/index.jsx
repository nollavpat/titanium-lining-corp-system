import React from 'react';

import {
  observable,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import {
  TabView,
  TabPanel,
} from 'primereact/tabview';

import PurchaseOrder from './PurchaseOrder';
import Profiles from './Profiles';
import Birthdays from './Birthdays';
import Transactions from './Transactions';

import Logs from '../Logs';


class Sales extends React.Component {
  @observable page = null;

  @action
  componentDidMount = () => {
    const { viewStore } = this.props;
    const currentPage = window.localStorage.getItem('salesPage') || 0;

    viewStore.changeTitle('Sales');

    this.page = Number(currentPage);
  }

  render() {
    const {
      inventoryStore,
      history,
      salesStore,
      authStore,
    } = this.props;
    const willRender = (
      inventoryStore.inventory && salesStore.profiles && this.page >= 0
    );

    return (
      <div>
        {willRender && (
          <TabView
            className="p-tabview"
            activeIndex={this.page}
            onTabChange={action((e) => {
              this.page = e.index;
              window.localStorage.setItem('salesPage', e.index);
            })}
          >
            <TabPanel header="Purchase Order">
              <PurchaseOrder history={history} />
            </TabPanel>
            <TabPanel header="Transactions">
              <Transactions
                history={history}
                isComplete={false}
                transactionsUsed={salesStore.incompletelyPayedTransactions}
              />
            </TabPanel>
            <TabPanel header="Completed Transactions">
              <Transactions
                history={history}
                isComplete
                transactionsUsed={salesStore.completelyPayedTransactions}
              />
            </TabPanel>
            <TabPanel header="Archived Transactions">
              <Transactions
                history={history}
                isComplete
                transactionsUsed={salesStore.archivedTransactions}
              />
            </TabPanel>
            <TabPanel header="Profiles">
              <Profiles history={history} />
            </TabPanel>
            <TabPanel header="Birthdays">
              <Birthdays history={history} />
            </TabPanel>
            <TabPanel header="Logs" disabled={!authStore.isAdmin}>
              <Logs myLogs={['deliveryReceipts', 'commissions']} full />
            </TabPanel>
          </TabView>
        )}
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'salesStore',
  'viewStore',
  'authStore',
)(observer(Sales));

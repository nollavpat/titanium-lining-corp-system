import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import {
  TabView,
  TabPanel,
} from 'primereact/tabview';
import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';

import DeliveryReceipt from '../DeliveryReceipt';

import { beautifyPrice } from '../../../../../utils/helper';

const DR = ({ dr }) => (
  <div>
    <div
      style={{
        paddingLeft: '12px',
        paddingRight: '12px',
        paddingTop: '8px',
        paddingBottom: '8px',
        border: '0.5px solid #c8c8c8',
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '8px',
      }}
    >
      <div
        style={{
          display: 'flex',
          width: '100%',
        }}
      >
        <div style={{ width: '50%' }}>
          <div
            style={{
              border: '0.5px solid #c8c8c8',
              padding: '6px',
              borderRadius: '5em',
              background: '#f4f4f4',
              width: '88px',
              textAlign: 'center',
            }}
          >
            <span>No. {dr.drNumber}</span>
          </div>
        </div>
        <div style={{ width: '50%', textAlign: 'right' }}>
          {dr.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
        </div>
      </div>
      <div
        style={{
          width: '100%',
          marginTop: '16px',
          paddingLeft: '14px',
        }}
      >
        {dr.transactionItems.map((t => (
          <div
            key={t._id}
            style={{
              padding: '4px',
              border: '0.5px solid #c8c8c8',
              marginBottom: '2px',
              width: '50%',
            }}
          >
            <span style={{ fontWeight: 600, marginRight: '8px' }}>{t.description}:</span>
            <span style={{ fontWeight: 600 }}>{t.items} delivered</span>
          </div>
        )))}
      </div>
    </div>
  </div>
);

class DeliveryReceipts extends React.Component {
  @observable transaction = null;

  @observable openedDr = null;

  @observable myAction = null;

  @observable openReceipt = false;

  @observable receiptTitle = '';

  @action
  componentDidMount() {
    this.fetchTransaction();
  }

  @action
  fetchTransaction = () => {
    const { salesStore, match } = this.props;

    this.transaction = salesStore.transactions.find(t => t._id === match.params.id);
  }

  @action
  add = () => {
    this.openedDr = {};
    this.myAction = 'add';
    this.openReceipt = true;
    this.receiptTitle = 'Add Delivery Receipt';
  }

  @action
  closeReceipt = () => {
    this.myAction = null;
    this.openReceipt = false;
    this.receiptTitle = '';
  }

  render() {
    const { history } = this.props;

    return (
      <div>
        {this.transaction && (
          <TabView className="p-tabview">
            <TabPanel
              header={`Delivery Receipts - Transaction Number: ${this.transaction.transactionNumber}`}
            >
              <Toolbar>
                <div className="p-toolbar-group-left">
                  <Button
                    label="Back"
                    icon="pi pi-angle-left"
                    onClick={() => { history.push('/sales'); }}
                  />
                </div>
                <div className="p-toolbar-group-right">
                  <Button
                    label="Add Delivery Reciept"
                    onClick={this.add}
                    disabled={this.transaction.isDrComplete}
                  />
                </div>
              </Toolbar>
              <div
                style={{
                  width: '100%',
                  marginBottom: '8px',
                  marginTop: '8px',
                  display: 'flex',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  paddingBottom: '8px',
                  borderBottom: '0.5px solid #c8c8c8',
                }}
              >
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Total Balance: </b>{beautifyPrice(this.transaction.totalPrice)}
                </div>
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Total Payment: </b>{beautifyPrice(this.transaction.totalPayment)}
                </div>
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Remaining Balance: </b>{beautifyPrice(this.transaction.remainingBalance)}
                </div>
              </div>
              <div style={{ overflowY: 'scroll', height: '60vh' }}>
                <h3>Delivery Receipts</h3>
                {
                  this.transaction.deliveryReceipts.length > 0
                    ? (this.transaction.deliveryReceipts.map(v => (
                      <DR
                        key={v._id}
                        dr={v}
                      />
                    )))
                    : <div style={{ marginLeft: '8px' }}>no records found</div>
                }
                {this.openReceipt && (
                  <DeliveryReceipt
                    dr={this.openedDr}
                    transactionItems={this.transaction.drItems}
                    openReceipt={this.openReceipt}
                    closeReceipt={this.closeReceipt}
                    transactionId={this.transaction._id}
                    callback={this.fetchTransaction}
                    title={this.receiptTitle}
                    myAction={this.myAction}
                  />
                )}
              </div>
            </TabPanel>
          </TabView>
        )}
      </div>
    );
  }
}

export default inject('salesStore')(observer(DeliveryReceipts));

import React from 'react';

import {
  observable,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Button } from 'primereact/button';
import { Menu } from 'primereact/menu';
// import { OverlayPanel } from 'primereact/overlaypanel';

import { beautifyPrice } from '../../../../../utils/helper';


class Transaction extends React.Component {
  @observable openDialog = false;

  render() {
    const { transaction, history, salesStore } = this.props;
    const items = [
      {
        label: transaction.archived ? 'Unarchive' : 'Archive',
        icon: 'pi pi-inbox',
        command: async () => {
          await salesStore.updateTransaction(
            transaction._id,
            {
              ...transaction,
              archived: !transaction.archived,
            },
            false,
          );
        },
      },
    ];
    let menu;

    return (
      <div>
        <div
          style={{
            paddingLeft: '12px',
            paddingRight: '12px',
            paddingTop: '8px',
            paddingBottom: '8px',
            border: '0.5px solid #c8c8c8',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div
            style={{
              display: 'flex',
              width: '100%',
            }}
          >
            <div style={{ width: '50%' }}>
              <div
                style={{
                  border: '0.5px solid #c8c8c8',
                  padding: '6px',
                  borderRadius: '5em',
                  background: '#f4f4f4',
                  width: '88px',
                  textAlign: 'center',
                }}
              >
                <span>No. {transaction.transactionNumber}</span>
              </div>
            </div>
            <div style={{ width: '50%', textAlign: 'right' }}>
              {transaction.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
              <span style={{ textAlign: 'left', marginLeft: '16px' }}>
                <Menu
                  model={items}
                  popup
                  ref={(el) => { menu = el; }}
                />
                <Button icon="pi pi-ellipsis-v" onClick={(event) => { menu.toggle(event); }} />
              </span>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              width: '100%',
              marginTop: '16px',
            }}
          >
            <div style={{ width: '50%', fontSize: '16px' }}>
              <div><b>Client: </b>{transaction.client.name}</div>
              <div style={{ marginTop: '16px' }}><b>Terms: </b>{`${transaction.terms} months`}</div>
              {/* <div><b>Commission: </b>
                {transaction.commission
                  ? (
                    <div style={{ display: 'inline' }}>
                      <Button
                        label="Claimed (Open Details)"
                        onClick={(e) => { this.op.toggle(e); }}
                      />
                      <OverlayPanel ref={(el) => { this.op = el; }}>
                        <div style={{ display: 'flex', width: '300px' }}>
                          <div style={{ width: '60%' }}>
                            <div style={{ marginTop: '16px' }}><b>Bank: </b> {transaction.commission.bank}</div>
                            <div><b>Amount: </b> {transaction.commission.amount}</div>
                          </div>
                          <div style={{ width: '40%' }}>
                            {transaction.commission.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
                          </div>
                        </div>
                      </OverlayPanel>
                    </div>
                  )
                  : 'Unclaimed'
                }
              </div> */}
            </div>
            <div style={{ width: '50%', fontSize: '16px' }}>
              <div><b>Consultant: </b>{transaction.consultant.name}</div>
              { transaction.trainer
                && <div><b>Trainer: </b>{transaction.trainer.name}</div>
              }
              { transaction.trainee
                && <div><b>Trainee: </b>{transaction.trainee.name}</div>
              }
            </div>
          </div>
        </div>
        <div
          style={{
            width: '100%',
            marginBottom: '8px',
            paddingLeft: '12px',
            paddingRight: '12px',
            paddingTop: '8px',
            paddingBottom: '8px',
            display: 'flex',
            alignItems: 'center',
            borderRight: '0.5px solid #c8c8c8',
            borderLeft: '0.5px solid #c8c8c8',
            borderBottom: '0.5px solid #c8c8c8',
            background: '#eaeaea',
          }}
        >
          <div style={{ width: '25%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline-flex',
                flexDirection: 'column',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <span><b>Total Balance: </b>{beautifyPrice(transaction.totalPrice)}</span>
              {!transaction.isTermsLessThanSixMonths && (
                <span><b>Base Price: </b>{beautifyPrice(transaction.price)}</span>
              )}
            </div>
          </div>
          <div style={{ width: '25%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <b>Total Payment: </b>{beautifyPrice(transaction.totalPayment)}
            </div>
          </div>
          <div style={{ width: '25%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <b>Remaining Balance: </b>{beautifyPrice(transaction.remainingBalance)}
            </div>
          </div>
          <div
            style={{
              borderLeft: '0.5px solid #c8c8c8',
              width: '25%',
              display: 'flex',
              flexDirection: 'row-reverse',
            }}
          >
            <Button
              label="View Delivery Receipts"
              onClick={() => { history.push(`/sales/transaction-delivery-receipts/${transaction._id}`); }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default inject(
  'accountingStore',
  'salesStore',
)(observer(Transaction));

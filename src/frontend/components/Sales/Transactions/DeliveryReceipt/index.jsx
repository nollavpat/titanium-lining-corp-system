import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { MultiSelect } from 'primereact/multiselect';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

import { beautifyPrice } from '../../../../../utils/helper';

class Receipt extends React.Component {
  @observable dr = {
    drNumber: '',
    date: new Date(),
  };

  @observable items = [];

  get itemOptions() {
    const { transactionItems } = this.props;

    return transactionItems
      .filter(t => t.available > 0)
      .map(i => ({
        label: i.description,
        value: i,
      }));
  }

  get willEdit() {
    const { myAction } = this.props;

    return myAction === 'edit';
  }

  @computed
  get details() {
    return {
      ...this.dr,
      transactionItems: this.items.map(i => ({
        items: +i.items,
        transactionItemId: i.transactionItemId,
        isSet: i.isSet,
        isGift: i.isGift,
      })),
    };
  }

  @action
  updateProperty = (key, value) => {
    this.dr = {
      ...this.dr,
      [key]: value,
    };
  }

  @action
  clear = () => {
    this.dr = {
      drNumber: '',
      date: new Date(),
    };
  }

  itemsEditor = props => (
    <InputText
      type="text"
      value={props.rowData.items}
      onChange={action((e) => {
        const is = [...props.value];
        const value = Number(e.target.value);

        if (value <= is[props.rowIndex].available) {
          is[props.rowIndex].items = Number(e.target.value);
        }

        this.items = is;
      })}
      keyfilter="pint"
    />
  )

  render() {
    const {
      openReceipt,
      closeReceipt,
      transactionId,
      callback,
      title,
      salesStore,
      viewStore,
    } = this.props;

    const ItemTemplate = option => (
      <div>
        {option.label}
        <span style={{ marginLeft: '6px', color: '#f6aa06', fontWeight: 700 }}>
          ({option.value.available} to be delivered)
        </span>
      </div>
    );

    const SelectedItemTemplate = value => (
      <span>
        {
          (value)
            ? (value.description)
            : ('Choose')
        }
      </span>
    );

    const PriceTemplate = ({ price }) => (
      <div>
        {beautifyPrice(price)}
      </div>
    );

    const GiftTemplate = ({ isGift }) => (
      <div>
        {isGift ? 'gifted' : ''}
      </div>
    );

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label={this.willEdit ? 'Save' : 'Add'}
          icon={this.willEdit ? 'pi pi-check' : 'pi pi-plus'}
          onClick={async () => {
            let result;

            if (this.willEdit) {
              result = await salesStore.updateDR(
                this.dr._id,
                {
                  ...this.details,
                  transactionId,
                },
              );
            } else {
              result = await salesStore.createDR({
                ...this.details,
                transactionId,
              });
            }

            if (result.success) {
              this.clear();
              callback();
              closeReceipt();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <Dialog
        visible={openReceipt}
        style={{ width: '600px' }}
        header={title}
        footer={dialogFooter}
        onHide={() => {
          this.clear();
          closeReceipt();
        }}
      >
        <div className="p-grid p-fluid">
          <div style={{ display: 'flex' }}>
            <div style={{ width: '50%', padding: '0.5em' }} />
            <div style={{ width: '50%', padding: '0.5em' }}>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <div style={{ fontWeight: 600 }}>DR No.: </div>
                <InputText
                  keyfilter="pnum"
                  style={{ width: '75%' }}
                  value={this.dr.drNumber}
                  onChange={(e) => { this.updateProperty('drNumber', e.target.value); }}
                />
              </div>
            </div>
          </div>
          <div style={{ width: '50%' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '0.5em',
              }}
            >
              <div style={{ fontWeight: 600, marginRight: '8px' }}>Date: </div>
              <div style={{ width: '75%' }}>
                <Calendar
                  panelStyle={{ marginTop: '200px' }}
                  value={this.dr.date}
                  onChange={(e) => { this.updateProperty('date', e.value); }}
                  dateFormat="MM dd, yy"
                  readOnlyInput
                />
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '0.5em',
              }}
            >
              <MultiSelect
                options={this.itemOptions}
                filter
                value={this.items}
                onChange={action((e) => { this.items = e.value; })}
                style={{
                  width: '288px',
                  whiteSpace: 'nowrap',
                  textOverflow: 'ellipsis',
                }}
                itemTemplate={ItemTemplate}
                selectedItemTemplate={SelectedItemTemplate}
              />
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: '0.5em',
            }}
          >
            <DataTable
              value={this.items}
            >
              <Column field="items" header="ITEM" editor={this.itemsEditor} />
              <Column field="description" header="ITEM DESCRIPTION" />
              <Column field="price" body={PriceTemplate} header="RETAIL PRICE" />
              <Column field="isGift" header="TODAYS DISCOUNT" body={GiftTemplate} />
            </DataTable>
          </div>
        </div>
      </Dialog>
    );
  }
}

export default inject(
  'salesStore',
  'viewStore',
)((observer(Receipt)));

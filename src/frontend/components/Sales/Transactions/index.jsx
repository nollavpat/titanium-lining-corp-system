import React from 'react';

import {
  observable,
  action,
  computed,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Toolbar } from 'primereact/toolbar';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Calendar } from 'primereact/calendar';
import { TriStateCheckbox } from 'primereact/tristatecheckbox';

import {
  timeEndDate,
  timeStartDate,
} from '../../../../utils/helper';

import Transaction from './Transaction';

const ALL = 'all';

class Transactions extends React.Component {
  @observable search = '';

  @observable searchFilter = 'client';

  @observable filter = ALL;

  @observable range = null;

  @observable page = null;

  @observable isActive = null;

  @computed
  get filteredTransaction() {
    const { transactionsUsed } = this.props;

    let transactions = transactionsUsed;

    if (this.filter === ALL) {
      transactions = transactionsUsed;
    } else if (this.filter === 'current month') {
      transactions = transactionsUsed.filter(t => t.hasCurrentMonthPayment);
    } else if (this.filter === 'overdue') {
      transactions = transactionsUsed.filter(t => t.hasOverduePayment);
    }

    if (this.range) {
      const [d1, d2] = this.range;

      transactions = transactionsUsed.filter(t => (
        t.date < timeEndDate(d2) && t.date > timeStartDate(d1)
      ));
    }

    if (this.isActive !== null) {
      transactions = transactions.filter(t => t.isActive === this.isActive);
    }

    return transactions;
  }

  @computed
  get result() {
    const profiles = ['client', 'consultant', 'trainer', 'trainee'];

    if (this.search === '') {
      return this.filteredTransaction;
    }

    if (profiles.includes(this.searchFilter)) {
      return this.filteredTransaction.filter(t => (
        t[this.searchFilter]
        && t[this.searchFilter].name.toLowerCase().includes(this.search.toLowerCase())
      ));
    }

    return this.filteredTransaction.filter(t => (
      String(t[this.searchFilter]).toLowerCase().includes(this.search.toLowerCase())
    ));
  }

  @action
  updateProperty = (key, value) => {
    this.commission = {
      ...this.commission,
      [key]: value,
    };
  }

  render() {
    const {
      history,
      isComplete,
    } = this.props;

    const searchOptions = [
      { label: 'client', value: 'client' },
      { label: 'consultant', value: 'consultant' },
      { label: 'trainer', value: 'trainer' },
      { label: 'trainee', value: 'trainee' },
      { label: 'transaction number', value: 'transactionNumber' },
      { label: 'terms', value: 'terms' },
    ];

    const filterOptions = [
      { label: ALL, value: ALL },
      { label: 'current month', value: 'current month' },
      { label: 'overdue', value: 'overdue' },
    ];

    const complete = (
      <div className="p-inputgroup" style={{ display: 'flex', alignItems: 'center' }}>
        <InputText
          value={this.search}
          onChange={action((e) => { this.search = e.target.value; })}
          placeholder="Search"
        />
        <Dropdown
          value={this.searchFilter}
          options={searchOptions}
          onChange={action((e) => { this.searchFilter = e.value; })}
        />
      </div>
    );

    const incomplete = (
      <div className="p-inputgroup" style={{ display: 'flex', alignItems: 'center' }}>
        <InputText
          value={this.search}
          onChange={action((e) => { this.search = e.target.value; })}
          placeholder="Search"
        />
        <Dropdown
          value={this.searchFilter}
          options={searchOptions}
          onChange={action((e) => { this.searchFilter = e.value; })}
        />
        <span
          className="p-inputgroup-addon"
          style={{ marginLeft: '32px' }}
        >
          <b>Filter:</b>
        </span>
        <Dropdown
          value={this.filter}
          options={filterOptions}
          onChange={action((e) => { this.filter = e.value; })}
        />
        <TriStateCheckbox
          style={{ marginLeft: '32px' }}
          value={this.isActive}
          onChange={action((e) => { this.isActive = e.value; })}
        />
        <span
          style={{ marginLeft: '8px', fontWeight: 600 }}
        >
          {(() => {
            if (this.isActive === true) {
              return 'All Active';
            }

            if (this.isActive === false) {
              return 'All Inactive';
            }

            return 'All';
          })()}
        </span>
      </div>
    );

    return (
      <div>
        <Toolbar>
          <div className="p-toolbar-group-left">
            <div className="p-col-12 p-md-4">
              {isComplete ? complete : incomplete}
            </div>
          </div>
          <div className="p-toolbar-group-right">
            <Calendar
              value={this.range}
              onChange={action((e) => {
                this.range = e.value;
                this.filter = ALL;
              })}
              selectionMode="range"
              readOnlyInput
              dateFormat="MM dd, yy"
              inputStyle={{ width: '300px', textAlign: 'center' }}
              placeholder="Date From - Date To"
              showButtonBar
            />
          </div>
        </Toolbar>
        <div style={{ overflowY: 'scroll', height: '70vh' }}>
          {this.result.length > 0
            ? (this.result.map(transaction => (
              <Transaction
                transaction={transaction}
                key={transaction._id}
                history={history}
              />
            )))
            : <div style={{ marginLeft: '8px', marginTop: '8px' }}>no records found</div>
          }
        </div>
      </div>
    );
  }
}

export default inject(
  'inventoryStore',
  'salesStore',
  'viewStore',
  'adminStore',
)(observer(Transactions));

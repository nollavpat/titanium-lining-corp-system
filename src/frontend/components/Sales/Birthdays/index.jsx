import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  computed,
} from 'mobx';

const Profile = ({ profile }) => (
  <div
    style={{
      padding: '12px',
      border: '0.5px solid #c8c8c8',
      marginBottom: '4px',
      display: 'flex',
      alignItems: 'flex-start',
    }}
  >
    <div style={{ width: '10%' }}>
      <img
        src={profile.picture ? `images/${profile.picture}` : 'assets/default.jpeg'}
        alt=""
        width="90"
        height="90"
        style={{ borderRadius: '5em' }}
      />
    </div>
    <div style={{ width: '40%' }}>
      <div style={{ marginBottom: '14px' }}><b>Role: </b>{profile.role.toUpperCase()}</div>
      <div><b>Name: </b><u>{profile.name.toUpperCase()}</u></div>
      <div><b>Home Address: </b>{profile.homeAddress}</div>
    </div>
    <div style={{ width: '40%' }}>
      <div><b>Cellphone Number: </b>{profile.cellphoneNumber}</div>
      <div><b>Email: </b>{profile.email}</div>
      <div><b>Birthday: </b>{profile.birthday.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}</div>
    </div>
  </div>
);

class Profiles extends React.Component {
  @computed
  get birthmonthProfiles() {
    const { salesStore } = this.props;

    return salesStore.profiles
      .filter(profile => profile.birthday)
      .filter(profile => profile.isBirthmonth && !profile.isBirthDay);
  }

  @computed
  get birthdayProfiles() {
    const { salesStore } = this.props;

    return salesStore.profiles
      .filter(profile => profile.birthday)
      .filter(profile => profile.isBirthDay);
  }

  render() {
    return (
      <div
        style={{
          height: '400px',
          padding: '8px',
          overflowY: 'scroll',
          marginTop: '4px',
        }}
      >
        <h3>Birthday Celebrants</h3>
        {this.birthdayProfiles.length > 0
          ? (this.birthdayProfiles.map(profile => (
            <Profile
              key={profile._id}
              profile={profile}
            />)))
          : 'No records found'
        }
        <h3>Birth Month Celebrants</h3>
        {this.birthmonthProfiles.length > 0
          ? (this.birthmonthProfiles.map(profile => (
            <Profile
              key={profile._id}
              profile={profile}
            />)))
          : 'No records found'
        }
      </div>
    );
  }
}


export default inject('salesStore')(observer(Profiles));

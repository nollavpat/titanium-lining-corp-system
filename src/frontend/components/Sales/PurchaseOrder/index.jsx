import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import { InputText } from 'primereact/inputtext';
import { AutoComplete } from 'primereact/autocomplete';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { MultiSelect } from 'primereact/multiselect';
import { Calendar } from 'primereact/calendar';
import { Checkbox } from 'primereact/checkbox';
import { Dropdown } from 'primereact/dropdown';

import roles from '../../../../utils/roles';
import purchaseOrderProfiles from '../../../../utils/purchaseOrderProfiles';
import { beautifyPrice, beautifyPropertyName } from '../../../../utils/helper';

const style = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  width: '50%',
};

const cellTopRightStyle = {
  borderTop: '0.5px solid #c8c8c8',
  borderRight: '0.5px solid #c8c8c8',
  padding: '4px',
};

const cellTopLeftStyle = {
  borderTop: '0.5px solid #c8c8c8',
  borderRight: '0.5px solid #c8c8c8',
  borderLeft: '0.5px solid #c8c8c8',
  padding: '4px',
  display: 'flex',
  alignItems: 'center',
};

const cellTopStyle = {
  borderRight: '0.5px solid #c8c8c8',
  borderLeft: '0.5px solid #c8c8c8',
  padding: '4px',
  display: 'flex',
  alignItems: 'center',
};

class PurchaseOrder extends React.Component {
  @observable client = null;

  @observable consultant = null;

  @observable associate = null;

  @observable seniorConsultant = null;

  @observable distributor = null;

  @observable presentor = null;

  @observable closer = null;

  @observable assistant = null;

  @observable profiles = [];

  @observable items = [];

  @observable date = new Date();

  @observable transactionNumber = '';

  @observable terms = '';

  @observable isSixMonthsLess = false;

  @observable poProfiles = [];

  @action
  componentDidMount() {
    const { salesStore } = this.props;

    this.profiles = salesStore.validProfiles;
  }

  @computed
  get hasSelectedProfiles() {
    return (this.consultant && this.consultant._id)
      || (this.seniorConsultant && this.seniorConsultant._id);
  }

  @computed
  get clients() {
    return this.profiles.filter(p => p.role === roles.CLIENT);
  }

  @computed
  get associates() {
    return this.profiles.filter(p => p.role === roles.ASSOCIATE);
  }

  @computed
  get consultants() {
    return this.profiles.filter(p => p.role === roles.CONSULTANT);
  }

  @computed
  get transactionDetails() {
    return {
      date: this.date,
      transactionNumber: this.transactionNumber,
      clientId: this.client ? this.client._id : null,
      associateId: this.associate ? this.associate._id : null,
      seniorConsultantId: this.seniorConsultant ? this.seniorConsultant._id : null,
      distributorId: this.distributor ? this.distributor._id : null,
      presentorId: this.presentor ? this.presentor._id : null,
      closerId: this.closer ? this.closer._id : null,
      assistantId: this.assistant ? this.assistant._id : null,
      consultantId: this.consultant ? this.consultant._id : null,
      commissionIds: [],
      transactionItems: this.items.map(i => ({
        transactionItemId: i.transactionItemId,
        isGift: i.isGift,
        isSet: i.isSet,
        items: i.items,
        price: i.price,
      })),
      terms: this.terms,
      paymentIds: [],
      deliveryReceiptIds: [],
    };
  }

  @action
  clearAll = () => {
    this.client = null;
    this.consultant = null;
    this.associate = null;
    this.seniorConsultant = null;
    this.distributor = null;
    this.presentor = null;
    this.closer = null;
    this.assistant = null;
    this.profiles = [];
    this.items = [];
    this.poProfiles = [];
    this.date = new Date();
    this.transactionNumber = '';
    this.terms = '';
  }

  @action
  handleComplete = (event) => {
    const { salesStore } = this.props;

    const results = salesStore.validProfiles.filter(p => (
      p.name.toLowerCase().startsWith(event.query.toLowerCase())
    ));

    this.profiles = results;
  }

  itemTemplate = profile => (
    <Button label={profile.name} tooltip={profile.homeAddress} className="p-button-secondary" />
  )

  itemsEditor = props => (
    <InputText
      type="text"
      value={props.rowData.items}
      onChange={action((e) => {
        const is = [...props.value];
        const value = Number(e.target.value);

        if (value <= is[props.rowIndex].currentQuantity) {
          is[props.rowIndex].items = Number(e.target.value);
        }

        this.items = is;
      })}
      keyfilter="pint"
    />
  )

  giftEditor = props => (
    <div style={{ display: 'flex' }}>
      <Checkbox
        checked={props.rowData.isGift}
        style={{ width: '20px' }}
        onChange={action((e) => {
          const items = [...props.value];

          items[props.rowIndex].isGift = e.checked;

          this.items = items;
        })}
      />
      <label style={{ marginLeft: '4px' }}>Gift</label>
    </div>
  )

  render() {
    const { salesStore, viewStore } = this.props;

    const itemOptions = salesStore.transactionItems.map(i => ({
      label: i.description,
      value: i,
    }));

    const profileOptions = Object.values(purchaseOrderProfiles).map(p => ({
      label: p,
      value: p,
    }));

    const termOptions = [
      { label: 9, value: 9 },
      { label: 12, value: 12 },
      { label: 18, value: 18 },
      { label: 24, value: 24 },
    ];

    const ItemTemplate = option => (
      <div>
        {option.label}
        <span style={{ marginLeft: '6px', color: '#f6aa06', fontWeight: 700 }}>
          ({option.value.currentQuantity} remaining)
        </span>
      </div>
    );

    const SelectedItemTemplate = value => (
      <span>
        {
          (value)
            ? (value.description)
            : ('Choose')
        }
      </span>
    );

    const PoTemplate = option => (
      <div>
        {beautifyPropertyName(option.label)}
      </div>
    );

    const SelectedPoTemplate = value => (
      <span>
        {
          (value)
            ? `${beautifyPropertyName(value)},`
            : ('Choose')
        }
      </span>
    );

    const header = (
      <div style={{ textAlign: 'left' }}>
        <MultiSelect
          options={itemOptions}
          filter
          value={this.items}
          onChange={action((e) => { this.items = e.value; })}
          style={{
            width: '288px',
            whiteSpace: 'nowrap',
            textOverflow: 'ellipsis',
          }}
          itemTemplate={ItemTemplate}
          selectedItemTemplate={SelectedItemTemplate}
        />
      </div>
    );

    const GiftTemplate = ({ isGift }) => (
      <div>
        {isGift ? 'gifted' : ''}
      </div>
    );
    const calculateGroupTotal = () => {
      let total = 0;

      this.items.forEach((i) => {
        total += i.finalPrice;
      });

      return beautifyPrice(total);
    };

    const PriceTemplate = ({ price }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(+price)}
      </div>
    );

    const FinalPriceTemplate = ({ price }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(+price)}
      </div>
    );

    return (
      <div>
        <div style={{ display: 'flex', flexDirection: 'row-reverse' }}>
          <div className="p-col-12 p-md-4">
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">No.</span>
              <InputText
                value={this.transactionNumber}
                onChange={action((e) => { this.transactionNumber = e.target.value; })}
                keyfilter="pint"
              />
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            marginTop: '4px',
          }}
        >
          <div style={style}>
            <div style={cellTopLeftStyle}>
              <b style={{ marginRight: '4px' }}>{'CLIENT\'S NAME: '}</b>
              <AutoComplete
                field="name"
                suggestions={this.clients}
                value={this.client}
                onChange={action((e) => { this.client = e.value; })}
                completeMethod={this.handleComplete}
                itemTemplate={this.itemTemplate}
              />
            </div>
            <div style={cellTopLeftStyle}>
              <b>HOME ADDRESS: </b>{this.client && this.client.homeAddress}
            </div>
            <div style={cellTopLeftStyle}>
              <b>BUSINESS/FIRM NAME: </b>{this.client && this.client.businessName}
            </div>
            <div style={cellTopLeftStyle}>
              <b>BUSINESS ADDRESS: </b>{this.client && this.client.businessAddress}
            </div>
            <div style={cellTopLeftStyle}>
              <b>BUS. TEL. NO.: </b>{this.client && this.client.businessTelNumber}
            </div>
          </div>
          <div style={style}>
            <div style={cellTopRightStyle}>
              <b>DATE: </b>
              <Calendar
                value={this.date}
                onChange={action((e) => { this.date = e.value; })}
                dateFormat="MM dd, yy"
                readOnlyInput
              />
            </div>
            <div style={cellTopRightStyle}>
              <b>CELLPHONE NO.: </b>{this.client && this.client.cellphoneNumber}
            </div>
            <div style={cellTopRightStyle}>
              <b>RES. TEL NO.: </b>{this.client && this.client.residentTelNumber}
            </div>
            <div style={cellTopRightStyle}>
              <b>E-MAIL ADDRESS: </b>{this.client && this.client.email}
            </div>
            <div style={cellTopRightStyle}>
              <b>POSITION: </b>{this.client && this.client.position}
            </div>
          </div>
        </div>
        <DataTable
          value={this.items}
          header={header}
        >
          <Column field="items" header="ITEM" editor={this.itemsEditor} />
          <Column field="description" header="ITEM DESCRIPTION" />
          <Column field="price" body={PriceTemplate} header="RETAIL PRICE" />
          <Column field="isGift" header="TODAYS DISCOUNT" body={GiftTemplate} editor={this.giftEditor} />
          <Column field="finalPrice" body={FinalPriceTemplate} header="YOUR PRICE" />
        </DataTable>
        {
          this.items.length > 0 && (
            <div
              style={{
                display: 'flex',
                borderBottom: '0.5px solid #c8c8c8',
                borderRight: '0.5px solid #c8c8c8',
                borderLeft: '0.5px solid #c8c8c8',
              }}
            >
              <div
                style={{
                  width: '80%',
                  textAlign: 'right',
                  padding: '12px',
                  fontWeight: '800',
                  borderRight: '0.5px solid #c8c8c8',
                }}
              >
                TOTAL PRICE:
              </div>
              <div style={{ width: '20%', padding: '12px', textAlign: 'right' }}>
                {calculateGroupTotal()}
              </div>
            </div>
          )
        }
        <div>
          <div style={cellTopStyle}>
            <b style={{ marginRight: '4px' }}>TERMS: </b>
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <b>Is less than 6 mo.: </b>
                <Checkbox
                  checked={this.isSixMonthsLess}
                  onChange={action(() => {
                    this.isSixMonthsLess = !this.isSixMonthsLess;
                    this.terms = 0;
                  })}
                />
              </span>
              {
                this.isSixMonthsLess
                  ? (
                    <InputText
                      keyfilter="pint"
                      value={this.terms}
                      onChange={action((e) => {
                        const input = Number(e.target.value);

                        if (input <= 6) {
                          this.terms = input;
                        }
                      })}
                    />
                  )
                  : (
                    <Dropdown
                      value={this.terms}
                      options={termOptions}
                      onChange={action((e) => { this.terms = e.value; })}
                    />
                  )
              }
            </div>
          </div>
        </div>
        <div>
          <div
            style={{
              borderTop: '0.5px solid #c8c8c8',
              borderRight: '0.5px solid #c8c8c8',
              borderLeft: '0.5px solid #c8c8c8',
              borderBottom: '0.5px solid #c8c8c8',
              padding: '4px',
            }}
          >
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
              }}
            >
              <b style={{ marginRight: '4px' }}>PROFILES: </b>
              <MultiSelect
                options={profileOptions}
                filter
                value={this.poProfiles}
                onChange={action((e) => { this.poProfiles = e.value; })}
                style={{
                  width: '288px',
                  whiteSpace: 'nowrap',
                  textOverflow: 'ellipsis',
                }}
                itemTemplate={PoTemplate}
                selectedItemTemplate={SelectedPoTemplate}
              />
            </div>
            <h4 style={{ paddingLeft: '8px' }}>SELECTED PROFILES:</h4>
            <div style={{ paddingLeft: '16px' }}>
              {/* <div
                style={{
                  display: 'flex',
                  width: '410px',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <b>CONSULTANT&#39;S NAME: </b>
                <AutoComplete
                  field="name"
                  suggestions={this.consultants}
                  value={this.consultant}
                  onChange={action((e) => { this.consultant = e.value; })}
                  completeMethod={this.handleComplete}
                  itemTemplate={this.itemTemplate}
                />
              </div> */}
              {this.poProfiles.map(p => (
                <div
                  key={p}
                  style={{
                    marginTop: '8px',
                    display: 'flex',
                    width: '410px',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <b>{`${beautifyPropertyName(p).toUpperCase()}'S NAME: `}</b>
                  <AutoComplete
                    field="name"
                    suggestions={p === 'associate' ? this.associates : this.consultants}
                    value={this[p]}
                    onChange={action((e) => { this[p] = e.value; })}
                    completeMethod={this.handleComplete}
                    itemTemplate={this.itemTemplate}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div style={{ display: 'flex', flexDirection: 'row-reverse' }}>
          <div>
            <Button
              label="Create"
              style={{ marginTop: '16px' }}
              disabled={!this.hasSelectedProfiles}
              onClick={async () => {
                const result = await salesStore.createTransaction({ ...this.transactionDetails });

                if (result.success) {
                  this.clearAll();
                } else if (result.errorDetails) {
                  viewStore.showValidationErrors(result.errorDetails);
                } else {
                  viewStore.showError(result.error);
                }
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default inject(
  'salesStore',
  'inventoryStore',
  'viewStore',
)(observer(PurchaseOrder));

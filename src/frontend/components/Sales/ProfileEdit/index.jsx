import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  toJS,
} from 'mobx';

import { Card } from 'primereact/card';
import { Fieldset } from 'primereact/fieldset';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { SelectButton } from 'primereact/selectbutton';
import { Calendar } from 'primereact/calendar';

import roles from '../../../../utils/roles';

const style = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
  width: '50%',
};

const rolesOption = [
  { label: roles.CLIENT, value: roles.CLIENT },
  { label: roles.CONSULTANT, value: roles.CONSULTANT },
  { label: roles.ASSOCIATE, value: roles.ASSOCIATE },
];

class ProfileEdit extends React.Component {
  @observable profile = {
    firstname: '',
    lastname: '',
    homeAddressFormat: {
      streetNumber: '',
      baranggay: '',
      city: '',
    },
    businessAddressFormat: {
      streetNumber: '',
      baranggay: '',
      city: '',
    },
    cellphoneNumber: '',
    email: '',
    businessName: '',
    position: '',
    businessTelNumber: '',
    residentTelNumber: '',
    role: roles.CLIENT,
    picture: null,
    birthday: null,
  };

  @observable reader = new FileReader();

  @action
  componentDidMount() {
    const { viewStore, match, salesStore } = this.props;
    const foundProfile = salesStore.profiles.find(profile => profile._id === match.params.id);

    if (foundProfile) {
      this.profile = {
        ...this.profile,
        ...toJS(foundProfile),
      };
    }

    viewStore.changeTitle('Sales');

    this.reader.addEventListener('load', async () => {
      const a = await salesStore.uploadImage(this.reader);

      this.updateProperty('picture', a.id);
    }, false);
  }

  @action
  updateProperty = (property, value) => {
    this.profile = {
      ...this.profile,
      [property]: value,
    };
  }

  @action
  updatePropertyOfProperty = (property1, property2, value) => {
    this.profile = {
      ...this.profile,
      [property1]: {
        ...this.profile[property1],
        [property2]: value,
      },
    };
  }

  render() {
    const { history, salesStore, viewStore } = this.props;
    const {
      firstname,
      lastname,
      email,
      cellphoneNumber,
      residentTelNumber,
      homeAddressFormat,
      businessName,
      position,
      businessAddressFormat,
      businessTelNumber,
      role,
      birthday,
      picture,
    } = this.profile;

    const footer = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label="Save"
          style={{ marginLeft: '4px', float: 'right' }}
          onClick={async () => {
            const result = await salesStore.updateProfile(this.profile._id, {
              firstname,
              lastname,
              email,
              cellphoneNumber,
              residentTelNumber,
              homeAddressFormat,
              businessName,
              position,
              businessAddressFormat,
              businessTelNumber,
              role,
              birthday,
              picture,
            });

            if (result.success) {
              history.push('/sales');
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
        <Button
          label="Cancel"
          className="p-button-secondary"
          onClick={() => { history.push('/sales'); }}
          style={{ float: 'right' }}
        />
      </div>
    );

    const header = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label="Delete"
          style={{ marginTop: '8px', marginRight: '8px', float: 'right' }}
          onClick={async () => {
            const result = await salesStore.deleteProfile(this.profile._id, {
              firstname,
              lastname,
              email,
              cellphoneNumber,
              residentTelNumber,
              homeAddressFormat,
              businessName,
              position,
              businessAddressFormat,
              businessTelNumber,
              role,
              birthday,
              picture,
              isRemoved: true,
            });
            if (result.success) {
              history.push('/sales');
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <Card title="Profile Edit" footer={footer} header={header}>
        <div style={{ marginTop: '16px' }}>
          <SelectButton options={rolesOption} value={role} onChange={(e) => { this.updateProperty('role', e.value); }} />
        </div>
        <div style={{ marginTop: '16px' }}>
          <span style={{ fontWeight: 700 }}>Profile Picture: </span>
          {!this.profile.picture
            ? (
              <input
                type="file"
                onChange={action((e) => {
                  const file = e.target.files[0];

                  this.reader.readAsDataURL(file);
                })}
              />
            )
            : (
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <img
                  src={`images/${this.profile.picture}`}
                  alt=""
                  width="100"
                  height="100"
                />
                <Button
                  icon="pi pi-times"
                  onClick={() => { this.updateProperty('picture', null); }}
                  className="p-button-danger"
                  style={{ marginLeft: '8px' }}
                />
              </div>
            )
          }
        </div>
        <Fieldset legend="Personal">
          <div style={{ display: 'flex' }}>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label><b>Name</b></label>
              </div>
              <div style={{ marginLeft: '16px' }}>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>First Name</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={firstname}
                    onChange={(e) => { this.updateProperty('firstname', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Last Name</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={lastname}
                    onChange={(e) => { this.updateProperty('lastname', e.target.value); }}
                  />
                </div>
              </div>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label><b>Birthday</b></label>
              </div>
              <div className="p-col-8" style={{ marginLeft: '16px', padding: '0.5em' }}>
                <Calendar
                  value={birthday}
                  onChange={(e) => { this.updateProperty('birthday', e.target.value); }}
                  monthNavigator
                  yearNavigator
                  yearRange="1900:2019"
                  readOnlyInput
                />
              </div>
            </div>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label><b>Contact Info</b></label>
              </div>
              <div style={{ marginLeft: '16px' }}>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Email</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={email}
                    onChange={(e) => { this.updateProperty('email', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Cellphone Number</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={cellphoneNumber}
                    onChange={(e) => { this.updateProperty('cellphoneNumber', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Res. Tel. Number</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={residentTelNumber}
                    onChange={(e) => { this.updateProperty('residentTelNumber', e.target.value); }}
                  />
                </div>
              </div>
            </div>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label><b>Address</b></label>
              </div>
              <div style={{ marginLeft: '16px' }}>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Street Number</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={homeAddressFormat.streetNumber}
                    onChange={(e) => { this.updatePropertyOfProperty('homeAddressFormat', 'streetNumber', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Baranggay</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={homeAddressFormat.baranggay}
                    onChange={(e) => { this.updatePropertyOfProperty('homeAddressFormat', 'baranggay', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>City</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={homeAddressFormat.city}
                    onChange={(e) => { this.updatePropertyOfProperty('homeAddressFormat', 'city', e.target.value); }}
                  />
                </div>
              </div>
            </div>
          </div>
        </Fieldset>
        <Fieldset legend="Business">
          <div style={{ display: 'flex' }}>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Business/Firm Name</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputText
                  value={businessName}
                  onChange={(e) => { this.updateProperty('businessName', e.target.value); }}
                />
              </div>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Position</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputText
                  value={position}
                  onChange={(e) => { this.updateProperty('position', e.target.value); }}
                />
              </div>
            </div>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label>Bus. Tel. Number</label>
              </div>
              <div className="p-col-8" style={{ padding: '.5em' }}>
                <InputText
                  value={businessTelNumber}
                  onChange={(e) => { this.updateProperty('businessTelNumber', e.target.value); }}
                />
              </div>
            </div>
            <div style={style}>
              <div className="p-col-4" style={{ padding: '.75em' }}>
                <label><b>Address</b></label>
              </div>
              <div style={{ marginLeft: '16px' }}>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Street Number</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={businessAddressFormat.streetNumber}
                    onChange={(e) => { this.updatePropertyOfProperty('businessAddressFormat', 'streetNumber', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>Baranggay</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={businessAddressFormat.baranggay}
                    onChange={(e) => { this.updatePropertyOfProperty('businessAddressFormat', 'baranggay', e.target.value); }}
                  />
                </div>
                <div className="p-col-4" style={{ padding: '.75em' }}>
                  <label>City</label>
                </div>
                <div className="p-col-8" style={{ padding: '.5em' }}>
                  <InputText
                    value={businessAddressFormat.city}
                    onChange={(e) => { this.updatePropertyOfProperty('businessAddressFormat', 'city', e.target.value); }}
                  />
                </div>
              </div>
            </div>
          </div>
        </Fieldset>
      </Card>
    );
  }
}

export default inject(
  'viewStore',
  'salesStore',
)(observer(ProfileEdit));

import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import { Toolbar } from 'primereact/toolbar';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';

import roles from '../../../../utils/roles';

const ALL = 'all';

const style = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  width: '50%',
};

const Profile = ({ profile, viewProfile, history }) => (
  <div
    style={{
      padding: '12px',
      border: '0.5px solid #c8c8c8',
      marginBottom: '4px',
      display: 'flex',
      alignItems: 'flex-start',
    }}
  >
    <div style={{ width: '10%' }}>
      <img
        src={profile.picture ? `images/${profile.picture}` : 'assets/default.jpeg'}
        alt=""
        width="90"
        height="90"
        style={{ borderRadius: '5em' }}
      />
    </div>
    <div style={{ width: '40%' }}>
      <div style={{ marginBottom: '14px' }}><b>Role: </b>{profile.role.toUpperCase()}</div>
      <div><b>Name: </b><u>{profile.name.toUpperCase()}</u></div>
      <div><b>Home Address: </b>{profile.homeAddress}</div>
    </div>
    <div style={{ width: '40%' }}>
      <div><b>Cellphone Number: </b>{profile.cellphoneNumber}</div>
      <div><b>Email: </b>{profile.email}</div>
      <div><b>Birthday: </b>
        {
          profile.birthday
            ? profile.birthday.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })
            : ''
        }
      </div>
    </div>
    <div style={{ display: 'flex', flexDirection: 'row-reverse', width: '10%' }}>
      <Button
        icon="pi pi-pencil"
        onClick={() => { history.push(`/sales/profile-edit/${profile._id}`); }}
        style={{ marginLeft: '4px' }}
        label="Edit"
      />
      <Button icon="pi pi-eye" label="View" onClick={() => { viewProfile(profile); }} />
    </div>
  </div>
);

class Profiles extends React.Component {
  @observable profile = null;

  @observable nameSearch = '';

  @observable displayDialog = false;

  @observable roleFilter = ALL;

  @observable searchFilter = 'name';

  @observable myTransactions = [];

  @observable page = 0;

  @computed
  get filteredProfiles() {
    const { salesStore } = this.props;

    if (this.roleFilter === ALL) {
      return salesStore.validProfiles;
    }

    return salesStore.validProfiles.filter(profile => profile.role === this.roleFilter);
  }

  @computed
  get result() {
    if (this.nameSearch === '') {
      return this.filteredProfiles;
    }

    return this.filteredProfiles.filter(profile => (
      profile[this.searchFilter].toLowerCase().includes(this.nameSearch.toLowerCase())
    ));
  }

  @action
  hideModal = () => {
    this.displayDialog = false;
    this.profile = null;
  }

  @action
  viewProfile = (profile) => {
    this.profile = profile;
    this.displayDialog = true;
  }

  @action
  onChange = (e) => {
    this.nameSearch = e.target.value;
  }

  @action
  selectFilter = (roleFilter) => {
    this.roleFilter = roleFilter;
  }

  render() {
    const { history } = this.props;

    const roleOptions = [
      { label: ALL, value: ALL },
      { label: roles.CLIENT, value: roles.CLIENT },
      { label: roles.CONSULTANT, value: roles.CONSULTANT },
      { label: roles.TRAINER, value: roles.TRAINER },
      { label: roles.TRAINEE, value: roles.TRAINEE },
    ];

    const searchOptions = [
      { label: 'name', value: 'name' },
      { label: 'home address', value: 'homeAddress' },
      { label: 'cellphone number', value: 'cellphoneNumber' },
      { label: 'email', value: 'email' },
    ];

    return (
      <div>
        <Toolbar>
          <div className="p-toolbar-group-left">
            <div className="p-col-12 p-md-4">
              <div className="p-inputgroup">
                <InputText
                  value={this.nameSearch}
                  onChange={this.onChange}
                  placeholder="Search"
                />
                <Dropdown
                  value={this.searchFilter}
                  options={searchOptions}
                  onChange={action((e) => { this.searchFilter = e.value; })}
                />
                <span
                  className="p-inputgroup-addon"
                  style={{ marginLeft: '32px' }}
                >
                  <b>Role Filter:</b>
                </span>
                <Dropdown
                  value={this.roleFilter}
                  options={roleOptions}
                  onChange={(e) => { this.selectFilter(e.value); }}
                />
              </div>
            </div>
          </div>
          <div className="p-toolbar-group-right">
            <Button
              label="Add"
              icon="pi pi-plus"
              onClick={() => {
                history.push('/sales/profile-add');
              }}
            />
          </div>
        </Toolbar>
        <div
          style={{
            height: '400px',
            padding: '8px',
            overflowY: 'scroll',
            marginTop: '4px',
          }}
        >
          {this.result.map(profile => (
            <Profile
              key={profile._id}
              profile={profile}
              viewProfile={this.viewProfile}
              history={history}
            />))}
        </div>
        {this.profile && (
          <Dialog
            header="Profile Details"
            visible={this.displayDialog}
            onHide={this.hideModal}
            style={{ width: '800px' }}
          >
            <div>
              <img
                src={this.profile.picture ? `images/${this.profile.picture}` : 'assets/default.jpeg'}
                alt=""
                width="100"
                height="100"
                style={{ borderRadius: '5em', marginLeft: '340px' }}
              />
              <div style={{ display: 'flex' }}>
                <div style={style}>
                  <div>
                    <b>Name: </b>{this.profile.name}
                  </div>
                  <div>
                    <b>Birthday: </b>
                    {
                      this.profile.birthday
                        ? this.profile.birthday.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })
                        : ''
                    }
                  </div>
                  <div>
                    <b>Role: </b>{this.profile.role}
                  </div>
                  <div>
                    <b>Home Address: </b>{this.profile.homeAddress}
                  </div>
                  <div>
                    <b>Business/Firm Name: </b>{this.profile.businessName}
                  </div>
                  <div>
                    <b>Business Address: </b>{this.profile.businessAddress}
                  </div>
                </div>
                <div style={style}>
                  <div>
                    <b>Cellphone No.: </b>{this.profile.cellphoneNumber}
                  </div>
                  <div>
                    <b>Resident Tel No.: </b>{this.profile.residentTelNumber}
                  </div>
                  <div>
                    <b>Email: </b>{this.profile.email}
                  </div>
                  <div>
                    <b>Position: </b>{this.profile.position}
                  </div>
                  <div>
                    <b>Business Tel No.: </b>{this.profile.businessTelNumber}
                  </div>
                </div>
              </div>
            </div>
          </Dialog>
        )}
      </div>
    );
  }
}


export default inject('salesStore')(observer(Profiles));


import React from 'react';

import {
  observable,
  action,
  computed,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { SelectButton } from 'primereact/selectbutton';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { TabView, TabPanel } from 'primereact/tabview';

import Receipt from '../Receipt';
import { beautifyPrice } from '../../../../utils/helper';


class Transaction extends React.Component {
  @observable downPaymentDialog = false;

  @observable paymentsDialog = false;

  @observable commission = null;

  @observable openDialog = false;

  @observable openedTransaction = null;

  @observable expense = null;

  @observable commissionIndex = 0;

  @computed
  get profiles() {
    if (!this.openedTransaction) {
      return [];
    }

    return this.openedTransaction.commissions.map(c => ({
      label: c.name,
      value: {
        ...c,
        percentage1: 0,
      },
    }));
  }

  @computed
  get isPercentageInvalid() {
    if (!this.commission) {
      return true;
    }

    const p = +this.commission.percentage1;

    return p <= 0 || p > 100;
  }

  @computed
  get isPercentageUpdateInvalid() {
    if (!this.commission) {
      return true;
    }

    const p = +this.commission.percentage;

    return p <= 0 || p > 100;
  }

  @computed
  get isExpenseInvalid() {
    if (!this.expense) {
      return true;
    }

    return (this.expense.amount <= 0 || this.expense.amount > this.commission.commissionBalance(this.openedTransaction.basicCommission))
      || this.expense.bank === null;
  }

  @computed
  get label() {
    let l = '';
    if (this.commission && this.commission.percentage) {
      if (this.commissionIndex === 0) {
        l = 'Add Commission';
      } else {
        l = 'Update Percentage';
      }
    } else {
      l = 'Add Percentage';
    }

    return l;
  }

  @computed
  get willDisable() {
    if (this.commission && this.commission.percentage) {
      if (this.commissionIndex === 0) {
        return this.isExpenseInvalid;
      }
      return this.isPercentageUpdateInvalid;
    }

    return this.isPercentageInvalid;
  }

  @action
  addCommission = (transaction) => {
    this.openedTransaction = transaction;
    this.expense = {
      date: new Date(),
      amount: 0,
      bank: null,
    };
    this.openDialog = true;
  }

  @action
  clearCommission = () => {
    this.openedTransaction = null;
    this.openDialog = false;
    this.commission = false;
    this.expense = null;
  }

  @action
  updateProperty = (key, value) => {
    this.commission = {
      ...this.commission,
      [key]: value,
    };
  }

  @action
  updateProperty1 = (key, value) => {
    this.expense = {
      ...this.expense,
      [key]: value,
    };
  }

  render() {
    const {
      transaction,
      history,
      viewStore,
      salesStore,
      adminStore,
    } = this.props;

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label={this.label}
          disabled={this.willDisable}
          onClick={async () => {
            const c = {
              // bank: this.commission.bank,
              percentage: this.commission.percentage
                ? +this.commission.percentage
                : +this.commission.percentage1,
              // amount: +this.commission.amount,
              _id: this.commission._id,
              transactionId: this.commission.transactionId,
              role: this.commission.role,
              profileId: this.commission.profileId,
            };
            const e = {
              ...this.expense,
              amount: +this.expense.amount,
              commissionId: this.commission._id,
            };
            let result;
            if (this.commission && this.commission.percentage) {
              if (this.commissionIndex === 0) {
                result = await salesStore.createCommissionExpense(e);
              } else {
                result = await salesStore.updateCommission(this.commission._id, c);
              }
            } else {
              result = await salesStore.updateCommission(this.commission._id, c);
            }

            if (result.success) {
              this.clearCommission();
              await adminStore.getConstants();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    const bankOptions = adminStore.constants.filter(c => c.type === 'BANK').map(b => ({
      value: b.value,
      label: b.value,
    }));

    const AmountTemplate = ({ amount }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(amount)}
      </div>
    );

    const BalaceTemplate = ({ commissionBalance }) => (
      <div style={{ textAlign: 'right' }}>
        {(() => {
          if (!this.openedTransaction) {
            return '';
          }

          const myBalance = commissionBalance(this.openedTransaction.basicCommission);

          if (+myBalance) {
            return beautifyPrice(myBalance);
          }

          return myBalance;
        })()}
      </div>
    );

    const TotalTemplate = ({ commissionTotal }) => (
      <div style={{ textAlign: 'right' }}>
        {(() => {
          if (!this.openedTransaction) {
            return '';
          }

          const myBalance = commissionTotal(this.openedTransaction.possibleCommission);

          if (+myBalance) {
            return beautifyPrice(myBalance);
          }

          return myBalance;
        })()}
      </div>
    );

    return (
      <div>
        <div
          style={{
            paddingLeft: '12px',
            paddingRight: '12px',
            paddingTop: '8px',
            paddingBottom: '8px',
            border: '0.5px solid #c8c8c8',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div
            style={{
              display: 'flex',
              width: '100%',
            }}
          >
            <div style={{ width: '50%' }}>
              <div
                style={{
                  border: '0.5px solid #c8c8c8',
                  padding: '6px',
                  borderRadius: '5em',
                  background: '#f4f4f4',
                  width: '88px',
                  textAlign: 'center',
                }}
              >
                <span>No. {transaction.transactionNumber}</span>
              </div>
            </div>
            <div style={{ width: '50%', textAlign: 'right' }}>
              {transaction.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              width: '100%',
              marginTop: '16px',
            }}
          >
            <div style={{ width: '50%', fontSize: '16px' }}>
              <div><b>Client: </b>{transaction.client.name}</div>
              <div style={{ marginTop: '16px' }}><b>Terms: </b>{`${transaction.terms} months`}</div>
            </div>
            <div style={{ width: '50%', fontSize: '16px' }}>
              <div><b>Consultant: </b>{transaction.consultant.name}</div>
              { transaction.trainer
                && <div><b>Trainer: </b>{transaction.trainer.name}</div>
              }
              { transaction.trainee
                && <div><b>Trainee: </b>{transaction.trainee.name}</div>
              }
            </div>
          </div>
        </div>
        <div
          style={{
            width: '100%',
            marginBottom: '8px',
            paddingLeft: '12px',
            paddingRight: '12px',
            paddingTop: '8px',
            paddingBottom: '8px',
            display: 'flex',
            alignItems: 'center',
            borderRight: '0.5px solid #c8c8c8',
            borderLeft: '0.5px solid #c8c8c8',
            borderBottom: '0.5px solid #c8c8c8',
            background: '#eaeaea',
          }}
        >
          <div style={{ width: '23.34%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline-flex',
                flexDirection: 'column',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <span><b>Total Balance: </b>{beautifyPrice(transaction.totalPrice)}</span>
              {!transaction.isTermsLessThanSixMonths && (
                <span><b>Base Price: </b>{beautifyPrice(transaction.price)}</span>
              )}
            </div>
          </div>
          <div style={{ width: '23.34%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <b>Total Payment: </b>{beautifyPrice(transaction.totalPayment)}
            </div>
          </div>
          <div style={{ width: '23.34%' }}>
            <div
              style={{
                border: '0.5px solid #c8c8c8',
                borderRadius: '3px',
                display: 'inline',
                padding: '8px',
                background: '#f4f4f4',
              }}
            >
              <b>Remaining Balance: </b>{beautifyPrice(transaction.remainingBalance)}
            </div>
          </div>
          <div
            style={{
              borderLeft: '0.5px solid #c8c8c8',
              width: '30%',
              display: 'flex',
              flexDirection: 'row-reverse',
            }}
          >
            <Button
              label="View Payments"
              icon="pi pi-money-bill"
              onClick={() => { history.push(`/accounting/transaction-payments/${transaction._id}`); }}
            />
            <Button
              label="View Commissions"
              icon="pi pi-money-bill"
              style={{ marginRight: '4px' }}
              onClick={() => { this.addCommission(transaction); }}
            />
          </div>
        </div>
        <Receipt
          downPaymentDialog={this.downPaymentDialog}
          hideDownPaymentDialog={action(() => { this.downPaymentDialog = false; })}
          transactionId={transaction._id}
          allowReset
        />
        <Dialog
          visible={this.openDialog}
          style={{ width: '700px' }}
          header="Commissions"
          modal
          footer={this.commission ? dialogFooter : null}
          onHide={this.clearCommission}
        >
          <div>
            <DataTable
              value={transaction.commissions}
            >
              <Column field="name" header="Name" />
              <Column field="role" header="Role" />
              <Column field="percentage" header="Percentage (%)" />
              <Column field="amount" header="Claimed Amount" body={AmountTemplate} />
              <Column field="" header="Balance" body={BalaceTemplate} />
              <Column field="" header="Total" body={TotalTemplate} />
            </DataTable>
            <div style={{ borderBottom: '0.5px solid #c8c8c8', marginTop: '8px' }} />
            {this.commission
              ? (
                <div style={{ width: '320px', marginTop: '8px' }}>
                  {
                    this.commission.percentage === null
                      ? (
                        <div
                          style={{
                            marginBottom: '8px',
                            display: 'flex',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}
                        >
                          <div style={{ fontWeight: 700 }}>Percentage: </div>
                          <div className="p-inputgroup">
                            <InputText
                              keyfilter="pnum"
                              value={this.commission.percentage1}
                              onChange={(e) => {
                                const { value } = e.target;

                                this.updateProperty('percentage1', value);
                              }}
                            />
                            <span className="p-inputgroup-addon">%</span>
                          </div>
                        </div>)
                      : (
                        <TabView
                          activeIndex={this.commissionIndex}
                          onTabChange={action((e) => { this.commissionIndex = e.index; })}
                        >
                          <TabPanel header="Add Commision">
                            <div
                              style={{
                                marginBottom: '8px',
                                display: 'flex',
                                justifyContent: 'space-between',
                              }}
                            >
                              <div style={{ fontWeight: 700 }}>Amount: </div>
                              <InputText
                                keyfilter="pnum"
                                value={this.expense.amount}
                                onChange={(e) => { this.updateProperty1('amount', e.target.value); }}
                              />
                            </div>
                            <div
                              style={{
                                marginBottom: '8px',
                                display: 'flex',
                                justifyContent: 'space-between',
                              }}
                            >
                              <div style={{ fontWeight: 700 }}>Bank: </div>
                              <SelectButton
                                value={this.expense.bank}
                                options={bankOptions}
                                onChange={(e) => {
                                  this.updateProperty1('bank', e.value);
                                }}
                              />
                            </div>
                          </TabPanel>
                          <TabPanel header="Update percentage">
                            <div
                              style={{
                                marginBottom: '8px',
                                display: 'flex',
                                justifyContent: 'space-between',
                              }}
                            >
                              <div style={{ fontWeight: 700 }}>Amount: </div>
                              <InputText
                                keyfilter="pnum"
                                value={this.commission.percentage}
                                onChange={(e) => { this.updateProperty('percentage', e.target.value); }}
                              />
                            </div>
                          </TabPanel>
                        </TabView>)
                  }
                </div>)
              : (
                <Dropdown
                  value={this.commission}
                  options={this.profiles}
                  onChange={action((e) => { this.commission = e.value; })}
                  placeholder="Select a Profile"
                  style={{ width: '300px', marginTop: '8px' }}
                />)
              }
          </div>
        </Dialog>
      </div>
    );
  }
}

export default inject(
  'accountingStore',
  'viewStore',
  'salesStore',
  'adminStore',
)(observer(Transaction));

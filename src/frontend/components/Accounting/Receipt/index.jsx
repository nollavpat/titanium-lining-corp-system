import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { SelectButton } from 'primereact/selectbutton';
import { Calendar } from 'primereact/calendar';
import { Checkbox } from 'primereact/checkbox';

class Receipt extends React.Component {
  @observable payment = {
    provisionaryNumber: '',
    paymentOption: 'cash', // cash check or pdc
    amount: 0,
    checkNumber: '',
    bank: '',
    depositedBank: '', // bdo metro bpi
    cleared: false, // checks and pdc defaults
    date: new Date(),
    isDownPayment: false, // if false, means loan amortization
    amortizationDate: null, // null if down payment
  };

  @action
  componentDidMount() {
    const { payment } = this.props;

    if (payment) {
      this.payment = {
        ...this.payment,
        ...payment,
      };
    }
  }

  @computed
  get isCredit() {
    return this.payment.paymentOption !== 'cash';
  }

  get willEdit() {
    const { paymentAction } = this.props;

    return paymentAction === 'edit';
  }

  @computed
  get details() {
    const { adminStore } = this.props;
    const {
      provisionaryNumber,
      paymentOption,
      amount,
      checkNumber,
      bank,
      depositedBank,
      cleared,
      date,
      isDownPayment,
      amortizationDate,
      _id,
    } = this.payment;

    const ba = adminStore.banks.find(b => b.value === depositedBank);
    const depositedBankId = ba ? ba._id : '';
    let obj;

    if (this.isCredit) {
      obj = {
        paymentOption,
        provisionaryNumber,
        amount,
        checkNumber,
        bank,
        depositedBankId,
        cleared,
        date,
        amortizationDate,
        isDownPayment,
      };
    } else {
      obj = {
        paymentOption,
        provisionaryNumber,
        amount,
        depositedBankId,
        date,
        amortizationDate,
        isDownPayment,
      };
    }

    if (this.willEdit) {
      obj = { ...obj, _id };
    }

    return obj;
  }

  @action
  resetPayment = () => {
    this.payment = {
      ...this.payment,
      checkNumber: '',
      bank: '',
    };
  }

  @action
  updateProperty = (key, value) => {
    this.payment = {
      ...this.payment,
      [key]: value,
    };
  }

  render() {
    const {
      openReceipt,
      closeReceipt,
      accountingStore,
      transactionId,
      callback,
      title,
      viewStore,
      adminStore,
    } = this.props;

    const bankOptions = adminStore.paymentBanks.map(b => ({
      value: b.value,
      label: b.value,
    }));

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          label={this.willEdit ? 'Save' : 'Add'}
          icon={this.willEdit ? 'pi pi-check' : 'pi pi-plus'}
          onClick={async () => {
            let result;

            if (this.willEdit) {
              result = await accountingStore.updatePayment(
                this.payment._id,
                {
                  ...this.details,
                  amount: +this.payment.amount,
                  transactionId,
                },
              );
            } else {
              result = await accountingStore.createPayment({
                ...this.details,
                amount: +this.payment.amount,
                transactionId,
              });
            }

            if (result.success) {
              callback();
              this.resetPayment();
              closeReceipt();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    const checkFiels = (
      <div>
        <div style={{ display: 'flex' }}>
          <div
            style={{
              width: '50%',
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: '0.5em',
            }}
          >
            <div style={{ fontWeight: 600, marginRight: '8px' }}>Bank: </div>
            <InputText
              value={this.payment.bank}
              style={{ width: '75%' }}
              onChange={(e) => { this.updateProperty('bank', e.target.value); }}
              disabled={this.willEdit}
            />
          </div>
          <div
            style={{
              width: '50%',
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: '0.5em',
            }}
          >
            <div style={{ fontWeight: 600, marginRight: '8px' }}>Check No.: </div>
            <InputText
              style={{ width: '75%' }}
              value={this.payment.checkNumber}
              onChange={(e) => { this.updateProperty('checkNumber', e.target.value); }}
              disabled={this.willEdit}
            />
          </div>
        </div>
        {this.willEdit && (
          <div style={{ display: 'flex', padding: '0.5em' }}>
            <div style={{ fontWeight: 600, marginRight: '8px' }}>Cleared: </div>
            <Checkbox
              checked={this.payment.cleared}
              onChange={(e) => { this.updateProperty('cleared', e.checked); }}
            />
          </div>
        )}
      </div>
    );

    const paymentOptions = [
      { label: 'Cash', value: 'cash' },
      { label: 'Check', value: 'check' },
    ];

    return (
      <Dialog
        visible={openReceipt}
        style={{ width: '600px' }}
        header={title}
        footer={dialogFooter}
        onHide={() => { closeReceipt(); }}
      >
        <div className="p-grid p-fluid">
          {!this.willEdit && (
            <div
              style={{
                padding: '0.5em',
              }}
            >
              <SelectButton
                value={this.payment.paymentOption}
                options={paymentOptions}
                onChange={(e) => {
                  this.updateProperty('paymentOption', e.value);
                  this.resetPayment();
                }}
              />
            </div>
          )}
          <div style={{ display: 'flex' }}>
            <div style={{ width: '50%', padding: '0.5em' }} />
            <div style={{ width: '50%', padding: '0.5em' }}>
              <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <div style={{ fontWeight: 600 }}>Prov. No.: </div>
                <InputText
                  keyfilter="pnum"
                  disabled={this.willEdit}
                  style={{ width: '75%' }}
                  value={this.payment.provisionaryNumber}
                  onChange={(e) => { this.updateProperty('provisionaryNumber', e.target.value); }}
                />
              </div>
            </div>
          </div>
          <div style={{ width: '50%' }}>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '0.5em',
              }}
            >
              <div style={{ fontWeight: 600, marginRight: '8px' }}>Date: </div>
              <div style={{ width: '75%' }}>
                <Calendar
                  panelStyle={{ marginTop: '200px' }}
                  value={this.payment.date}
                  disabled={this.willEdit}
                  onChange={(e) => { this.updateProperty('date', e.value); }}
                  dateFormat="MM dd, yy"
                  readOnlyInput
                />
              </div>
            </div>
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                padding: '0.5em',
              }}
            >
              <div style={{ fontWeight: 600, marginRight: '8px' }}>Amount: </div>
              <InputText
                keyfilter="pnum"
                disabled={this.willEdit}
                style={{ width: '75%' }}
                value={this.payment.amount}
                onChange={(e) => { this.updateProperty('amount', e.target.value); }}
              />
            </div>
          </div>
          {
            this.isCredit && checkFiels
          }
          {this.willEdit && (
            <div
              style={{
                padding: '0.5em',
              }}
            >
              <div style={{ fontWeight: 600, marginRight: '8px' }}>Bank Deposited: </div>
              <SelectButton
                style={{ marginTop: '8px' }}
                value={this.payment.depositedBank}
                options={bankOptions}
                onChange={(e) => { this.updateProperty('depositedBank', e.value); }}
              />
            </div>
          )}
        </div>
      </Dialog>
    );
  }
}

export default inject(
  'accountingStore',
  'viewStore',
  'adminStore',
)((observer(Receipt)));

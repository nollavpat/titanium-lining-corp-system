import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
} from 'mobx';

import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dialog } from 'primereact/dialog';
import { Calendar } from 'primereact/calendar';
import { AutoComplete } from 'primereact/autocomplete';
import { SelectButton } from 'primereact/selectbutton';

import { beautifyPrice } from '../../../../utils/helper';

class Balances extends React.Component {
  @observable showBalanceAdd = false;

  @observable balance = null;

  @observable showBalancePayment = false;

  @observable payment = null;

  @observable profiles = [];

  @observable showPaymentDetails = false;

  @action
  componentDidMount() {
    const { salesStore } = this.props;

    this.profiles = salesStore.validProfiles;
  }

  @action
  addBalance = () => {
    this.showBalanceAdd = true;
    this.balance = {
      name: '',
      totalBalance: 0,
    };
  }

  @action
  clearBalance = () => {
    this.showBalanceAdd = false;
    this.balance = null;
  }

  @action
  addPayment = (e) => {
    this.balance = e.value;
    this.payment = {
      date: null,
      amount: 0,
      bank: null,
    };
    this.showBalancePayment = true;
  }

  @action
  clearPayment = () => {
    this.balance = null;
    this.payment = null;
    this.showBalancePayment = false;
  }

  @action
  showDetails = (balance) => {
    this.balance = balance;
    this.showPaymentDetails = true;
  }

  @action
  clearDetails = () => {
    this.balance = null;
    this.showPaymentDetails = false;
  }

  @action
  updateProperty = (key, value) => {
    this.balance = {
      ...this.balance,
      [key]: value,
    };
  }

  @action
  updateProperty1 = (key, value) => {
    this.payment = {
      ...this.payment,
      [key]: value,
    };
  }

  @action
  handleComplete = (event) => {
    const { salesStore } = this.props;

    const results = salesStore.validProfiles.filter(p => (
      p.name.toLowerCase().startsWith(event.query.toLowerCase())
    ));

    this.profiles = results;
  }

  itemTemplate = profile => (
    <Button label={profile.name} tooltip={profile.homeAddress} className="p-button-secondary" />
  )

  render() {
    const {
      adminStore,
      accountingStore,
      viewStore,
    } = this.props;

    const template1 = ({ totalBalance }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(totalBalance)}
      </div>
    );

    const template2 = ({ totalPayment }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(totalPayment)}
      </div>
    );

    const template3 = ({ remainingBalance }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(remainingBalance)}
      </div>
    );

    const amountTemplate = ({ amount }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(amount)}
      </div>
    );

    const dateTemplate = ({ date }) => (
      <div>
        {(new Date(date)).toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
      </div>
    );

    const actionTemplate = rowData => (
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <Button
          icon="pi pi-eye"
          onClick={() => {
            this.showDetails(rowData);
          }}
        />
      </div>
    );

    const bankOptions = adminStore.expenseBanks.map(b => ({
      value: b.value,
      label: b.value,
    }));

    const footer = (
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button
          style={{ float: 'right' }}
          label="Add"
          icon="pi pi-plus"
          onClick={this.addBalance}
        />
      </div>
    );

    const dialogFooter1 = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Add"
          icon="pi pi-plus"
          onClick={async () => {
            const finalData = {
              name: this.balance.name.name,
              totalBalance: +this.balance.totalBalance,
            };
            const result = await accountingStore.createBalance(finalData);

            if (result.success) {
              this.clearBalance();
              await adminStore.getConstants();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    const dialogFooter2 = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Add"
          icon="pi pi-plus"
          onClick={async () => {
            const finalData = {
              ...this.payment,
              amount: +this.payment.amount,
              balanceId: this.balance._id,
            };

            const result = await accountingStore.createBalancePayment(finalData);

            if (result.success) {
              this.clearPayment();
              await adminStore.getConstants();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <div style={{ height: '70vh' }}>
        <DataTable
          value={accountingStore.filteredBalances}
          rows={8}
          paginator
          footer={footer}
          selectionMode="single"
          selection={this.balance}
          onSelectionChange={this.addPayment}
        >
          <Column field="name" header="Name" />
          <Column field="totalBalance" header="Total Balance" body={template1} />
          <Column field="totalPayment" header="Total Payment" body={template2} />
          <Column field="remainingBalance" header="Remaining Balance" body={template3} />
          <Column header="Show Payments" body={actionTemplate} />
        </DataTable>
        <Dialog
          visible={this.showPaymentDetails}
          style={{ width: '500px' }}
          header="Payment Details"
          modal
          onHide={this.clearDetails}
        >
          {this.balance && (
            <DataTable
              value={this.balance.payments}
            >
              <Column field="date" header="Date" body={dateTemplate} />
              <Column field="amount" header="Amount" body={amountTemplate} />
              <Column field="bank" header="Bank" />
            </DataTable>
          )}
        </Dialog>
        <Dialog
          visible={this.showBalancePayment}
          style={{ width: '450px' }}
          header="Add Payment"
          modal
          footer={dialogFooter2}
          onHide={this.clearPayment}
        >
          {this.payment && (
            <div style={{ display: 'flex' }}>
              <div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <span style={{ fontWeight: 700 }}>Date: </span>
                  <Calendar
                    value={this.payment.date}
                    onChange={(e) => { this.updateProperty1('date', e.value); }}
                    dateFormat="MM dd, yy"
                    readOnlyInput
                    panelStyle={{ marginTop: '200px' }}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>Amount: </div>
                  <InputText
                    keyfilter="pnum"
                    value={this.payment.amount}
                    onChange={(e) => { this.updateProperty1('amount', e.target.value); }}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 600, marginRight: '8px' }}>Bank: </div>
                  <SelectButton
                    value={this.payment.bank}
                    options={bankOptions}
                    onChange={(e) => { this.updateProperty1('bank', e.value); }}
                  />
                </div>
              </div>
            </div>
          )}
        </Dialog>
        <Dialog
          visible={this.showBalanceAdd}
          style={{ width: '300px' }}
          header="Add Balance"
          modal
          footer={dialogFooter1}
          onHide={this.clearBalance}
        >
          {this.balance && (
            <div>
              <div
                style={{
                  marginBottom: '8px',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <div style={{ fontWeight: 700 }}>Name: </div>
                <AutoComplete
                  field="name"
                  value={this.balance.name}
                  onChange={(e) => { this.updateProperty('name', e.value); }}
                  suggestions={this.profiles}
                  completeMethod={this.handleComplete}
                  itemTemplate={this.itemTemplate}
                />
              </div>
              <div
                style={{
                  marginBottom: '8px',
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
              >
                <div style={{ fontWeight: 700 }}>Total Balance: </div>
                <InputText
                  value={this.balance.totalBalance}
                  onChange={(e) => { this.updateProperty('totalBalance', e.target.value); }}
                />
              </div>
            </div>
          )}
        </Dialog>
      </div>
    );
  }
}

export default inject(
  'adminStore',
  'accountingStore',
  'viewStore',
  'salesStore',
)(observer(Balances));

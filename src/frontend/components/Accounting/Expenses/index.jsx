import React from 'react';

import {
  observable,
  action,
  computed,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dialog } from 'primereact/dialog';
import { Calendar } from 'primereact/calendar';
import { AutoComplete } from 'primereact/autocomplete';
import { Growl } from 'primereact/growl';
import { SelectButton } from 'primereact/selectbutton';

import { beautifyPrice } from '../../../../utils/helper';

const defaultExpense = {
  date: new Date(),
  tinNumber: '',
  establishment: '',
  remarks: '',
  amount: 0,
  bank: '',
};

class Expenses extends React.Component {
  @observable showDialog = false;

  @observable expense = null;

  @observable tinNumberSuggestions = null;

  @observable autoCompleted = false;

  componentDidMount() {
    const { accountingStore } = this.props;

    if (accountingStore.hasNotifications) {
      accountingStore.showNotifications();
    }
  }

  @computed
  get tinNumbers() {
    const { accountingStore } = this.props;

    return [...new Set(accountingStore.expenses.map(e => e.tinNumber))];
  }

  @action
  updateProperty = (key, value) => {
    this.expense = {
      ...this.expense,
      [key]: value,
    };
  }

  @action
  suggestTinNumbers = (event) => {
    const results = this.tinNumbers.filter(t => (
      t.toLowerCase().startsWith(event.query.toLowerCase())
    ));

    this.tinNumberSuggestions = results;
  }

  @action
  autofillEstablishment = (tinNumber) => {
    const { accountingStore } = this.props;

    const expense = accountingStore.expenses.find(e => e.tinNumber === tinNumber);

    this.updateProperty('establishment', expense.establishment);
    this.autoCompleted = true;
  }

  @action
  clearExpense = () => {
    this.expense = null;
    this.showDialog = false;
  }

  render() {
    const { accountingStore, viewStore, adminStore } = this.props;

    const PriceTemplate = ({ amount }) => (
      <div>
        {beautifyPrice(amount)}
      </div>
    );

    const DateTemplate = ({ date }) => (
      <div>
        {date.toLocaleString('en-us', { day: 'numeric', month: 'long', year: 'numeric' })}
      </div>
    );

    const bankOptions = adminStore.expenseBanks.map(b => ({
      value: b.value,
      label: b.value,
    }));

    const footer = (
      <div className="p-clearfix" style={{ width: '100%' }}>
        <Button
          style={{ float: 'right' }}
          label="Add"
          icon="pi pi-plus"
          onClick={action(() => {
            this.showDialog = true;
            this.expense = defaultExpense;
          })}
        />
      </div>
    );

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Save"
          icon="pi pi-plus"
          onClick={async () => {
            const result = await accountingStore.createExpense({
              ...this.expense,
              amount: +this.expense.amount,
            });

            if (result.success) {
              this.clearExpense();
              await adminStore.getConstants();
              if (accountingStore.hasNotifications) {
                accountingStore.showNotifications();
              }
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <div style={{ height: '70vh' }}>
        <DataTable
          value={accountingStore.descendingExpenses}
          rows={8}
          paginator
          footer={footer}
        >
          <Column
            field="date"
            header="Date"
            style={{ width: '15%' }}
            body={DateTemplate}
          />
          <Column field="tinNumber" header="TIN Number" style={{ width: '15%' }} />
          <Column field="establishment" header="Establishment" style={{ width: '20%' }} />
          <Column field="remarks" header="Remarks" style={{ width: '20%' }} />
          <Column field="bank" header="Bank" style={{ width: '20%' }} />
          <Column
            field="amount"
            header="Amount"
            style={{ width: '10%' }}
            body={PriceTemplate}
          />
        </DataTable>
        <Dialog
          visible={this.showDialog}
          style={{ width: '600px' }}
          header="Add Expense"
          modal
          footer={dialogFooter}
          onHide={action(() => { this.showDialog = false; })}
        >
          {this.expense && (
            <div style={{ display: 'flex' }}>
              <div style={{ width: '55%' }}>
                <div
                  style={{
                    marginTop: '58px',
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>TIN Number: </div>
                  <AutoComplete
                    value={this.expense.tinNumber}
                    onChange={(e) => { this.updateProperty('tinNumber', e.target.value); }}
                    suggestions={this.tinNumberSuggestions}
                    completeMethod={this.suggestTinNumbers}
                    onSelect={(e) => { this.autofillEstablishment(e.value); }}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>Establishment: </div>
                  <InputText
                    value={this.expense.establishment}
                    onChange={action((e) => {
                      if (this.autoCompleted) {
                        this.updateProperty('tinNumber', '');
                        this.autoCompleted = false;
                      }
                      this.updateProperty('establishment', e.target.value);
                    })}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>Remarks: </div>
                  <InputText
                    value={this.expense.remarks}
                    onChange={(e) => { this.updateProperty('remarks', e.target.value); }}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>Amount: </div>
                  <InputText
                    keyfilter="pnum"
                    value={this.expense.amount}
                    onChange={(e) => { this.updateProperty('amount', e.target.value); }}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 600, marginRight: '8px' }}>Bank: </div>
                  <SelectButton
                    value={this.expense.bank}
                    options={bankOptions}
                    onChange={(e) => { this.updateProperty('bank', e.value); }}
                  />
                </div>
              </div>
              <div className="p-clearfix" style={{ width: '45%', float: 'right' }}>
                <div style={{ marginBottom: '24px' }}>
                  <span style={{ fontWeight: 700 }}>Date: </span>
                  <Calendar
                    value={this.expense.date}
                    onChange={(e) => { this.updateProperty('date', e.value); }}
                    dateFormat="MM dd, yy"
                    readOnlyInput
                  />
                </div>
              </div>
            </div>
          )}
        </Dialog>
        <Growl
          ref={(el) => { accountingStore.growl = el; }}
          onRemove={(m) => {
            accountingStore.readNotification(m.id);
          }}
        />
      </div>
    );
  }
}

export default inject(
  'accountingStore',
  'viewStore',
  'adminStore',
)(observer(Expenses));

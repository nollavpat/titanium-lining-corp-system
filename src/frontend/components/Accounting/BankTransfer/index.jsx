import React from 'react';

import {
  inject,
  observer,
} from 'mobx-react';
import {
  observable,
  action,
  // computed,
} from 'mobx';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { AutoComplete } from 'primereact/autocomplete';
import { InputText } from 'primereact/inputtext';

import { beautifyPrice } from '../../../../utils/helper';

class BankTransfer extends React.Component {
  @observable bt = null;

  @observable openDialog = false;

  @observable banks = [];

  @action
  componentDidMount = () => {
    const { adminStore } = this.props;

    this.banks = adminStore.banks;
  }

  @action
  addBT = () => {
    this.bt = {
      from: null,
      to: null,
      amount: 0,
    };
    this.openDialog = true;
  }

  @action
  clearBT = () => {
    this.bt = null;
    this.openDialog = false;
  }

  @action
  updateProperty = (property, value) => {
    this.bt = {
      ...this.bt,
      [property]: value,
    };
  }

  @action
  handleComplete = (event) => {
    const { adminStore } = this.props;

    const results = adminStore.banks.filter(p => (
      p.value.toLowerCase().startsWith(event.query.toLowerCase())
    ));

    this.banks = results;
  }

  render() {
    const { adminStore } = this.props;

    const BalanceTemplate = ({ balance }) => (
      <div style={{ textAlign: 'right' }}>
        {beautifyPrice(balance)}
      </div>
    );

    const header = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Transfer Amount"
          onClick={this.addBT}
        />
      </div>
    );

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Transfer"
          disabled={
            this.bt
            && (
              typeof this.bt.from !== 'object'
              || typeof this.bt.to !== 'object'
              || this.bt.amount <= 0
              || this.bt.from.value === this.bt.to.value
            )}
          onClick={async () => {
            const {
              from,
              to,
              amount,
            } = this.bt;

            await adminStore.bankTransfer({
              amount: +amount,
              fromId: from._id,
              toId: to._id,
            });

            this.clearBT();
          }}
        />
      </div>
    );

    return (
      <div style={{ height: '70vh' }}>
        <DataTable
          value={adminStore.banks}
          rows={8}
          paginator
          header={header}
        >
          <Column field="value" header="Bank" />
          <Column field="balance" header="Balance" body={BalanceTemplate} />
        </DataTable>
        <Dialog
          visible={this.openDialog}
          style={{ width: '600px' }}
          header="Transfer Amount"
          modal
          footer={dialogFooter}
          onHide={action(() => { this.openDialog = false; })}
        >
          {this.bt && (
            <div style={{ display: 'flex' }}>
              <div style={{ width: '55%' }}>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>From: </div>
                  <AutoComplete
                    field="value"
                    suggestions={this.banks}
                    value={this.bt.from}
                    onChange={action((e) => { this.updateProperty('from', e.value); })}
                    completeMethod={this.handleComplete}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>To: </div>
                  <AutoComplete
                    field="value"
                    suggestions={this.banks}
                    value={this.bt.to}
                    onChange={action((e) => { this.updateProperty('to', e.value); })}
                    completeMethod={this.handleComplete}
                  />
                </div>
                <div
                  style={{
                    marginBottom: '8px',
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <div style={{ fontWeight: 700 }}>Amount: </div>
                  <InputText
                    keyfilter="pnum"
                    value={this.bt.amount}
                    onChange={(e) => { this.updateProperty('amount', e.target.value); }}
                  />
                </div>
              </div>
            </div>
          )}
        </Dialog>
      </div>
    );
  }
}

export default inject(
  'accountingStore',
  'viewStore',
  'adminStore',
)(observer(BankTransfer));

import React from 'react';

import {
  observer,
  inject,
} from 'mobx-react';
import {
  observable,
  action,
  computed,
} from 'mobx';

import {
  TabView,
  TabPanel,
} from 'primereact/tabview';
import { Toolbar } from 'primereact/toolbar';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';

import { InputText } from 'primereact/inputtext';
import Receipt from '../Receipt';

import { beautifyPrice } from '../../../../utils/helper';

const style = {
  padding: '12px',
  border: '0.5px solid #c8c8c8',
  marginBottom: '4px',
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};

const UnpayedAmortizedLoan = ({ payment, payAmortizedLoan }) => (
  <div style={style}>
    <div>
      <div style={{ marginLeft: '14px', marginBottom: '14px' }}>Date: {payment.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}</div>
      <div style={{ marginLeft: '14px' }}>
        <div>Amount: {beautifyPrice(payment.amount)}</div>
      </div>
    </div>
    <Button
      label="Add Payment"
      onClick={() => { payAmortizedLoan(payment); }}
    />
  </div>
);

const P = ({ payment, myAction }) => (
  <div>
    <div
      style={{
        paddingLeft: '12px',
        paddingRight: '12px',
        paddingTop: '8px',
        paddingBottom: '8px',
        border: '0.5px solid #c8c8c8',
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '8px',
      }}
    >
      <div
        style={{
          display: 'flex',
          width: '100%',
        }}
      >
        <div style={{ width: '50%' }}>
          <div
            style={{
              border: '0.5px solid #c8c8c8',
              padding: '6px',
              borderRadius: '5em',
              background: '#f4f4f4',
              width: '88px',
              textAlign: 'center',
            }}
          >
            <span>No. {payment.provisionaryNumber}</span>
          </div>
        </div>
        <div style={{ width: '50%', textAlign: 'right' }}>
          {payment.date.toLocaleString('en-us', { month: 'long', year: 'numeric', day: 'numeric' })}
        </div>
      </div>
      <div
        style={{
          display: 'flex',
          width: '100%',
          marginTop: '16px',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingLeft: '14px',
        }}
      >
        <div>
          <div>
            <span>Payed in {payment.paymentOption.toUpperCase()}</span>
          </div>
          {
            payment.paymentOption === 'check' && (
              <div>
                <span>{payment.cleared ? 'Cleared' : 'Not Cleared'}</span>
              </div>
            )
          }
          <div>
            <span>{!payment.depositedBank ? 'Not Deposited' : `Deposited to ${payment.depositedBank}`}</span>
          </div>
          <div style={{ marginTop: '8px', fontSize: '18px' }}>
            <span>Amount: {beautifyPrice(payment.amount)}</span>
          </div>
        </div>
        <Button
          label="Edit Payment"
          disabled={(() => {
            if (payment.paymentOption === 'cash') {
              return payment.depositedBank;
            }

            return payment.cleared && payment.depositedBank;
          })()}
          onClick={() => { myAction(payment); }}
        />
      </div>
    </div>
  </div>
);

class Payments extends React.Component {
  @observable transaction = null;

  @observable openReceipt = false;

  @observable paymentAction = null;

  @observable receiptTitle = '';

  @observable willIncrease = false;

  @observable willCustomize = false;

  @observable terms = '';

  @observable customTerms = null;

  @observable customPayments = null;

  @action
  componentDidMount() {
    this.fetchTransaction();

    this.terms = this.transaction.terms;
  }

  @computed
  get termOptions() {
    const termOptions = [
      { label: 1, value: 1 },
      { label: 2, value: 2 },
      { label: 3, value: 3 },
      { label: 4, value: 4 },
      { label: 5, value: 5 },
      { label: 6, value: 6 },
      { label: 9, value: 9 },
      { label: 12, value: 12 },
      { label: 18, value: 18 },
      { label: 24, value: 24 },
    ];

    return termOptions.filter(t => +t.value > +this.transaction.terms);
  }

  @action
  increaseTerms = () => {
    this.willIncrease = true;
  }

  @action
  clearTerms = () => {
    this.willIncrease = false;
  }

  @action
  addCustom = () => {
    this.customPayments = this.transaction.totalPrice;
    this.customTerms = this.transaction.terms;
    this.willCustomize = true;
  }

  @action
  clearCustom = () => {
    this.willCustomize = false;
    this.customPayments = null;
    this.customTerms = null;
  }

  @action
  fetchTransaction = () => {
    const { salesStore, match } = this.props;

    this.transaction = salesStore.transactions.find(t => t._id === match.params.id);
  }

  @action
  payAmortizedLoan = (payment) => {
    this.openedPayment = payment;
    this.paymentAction = 'add';
    this.openReceipt = true;
    this.receiptTitle = 'Add Amortized Loan';
  }

  @action
  editAmortizedLoan = (payment) => {
    this.openedPayment = payment;
    this.paymentAction = 'edit';
    this.openReceipt = true;
    this.receiptTitle = 'Edit Amortized Loan';
  }

  @action
  payDownPayment = () => {
    this.openedPayment = { isDownPayment: true };
    this.paymentAction = 'add';
    this.openReceipt = true;
    this.receiptTitle = 'Add Down Payment';
  }

  @action
  editDownPayment = (payment) => {
    this.openedPayment = payment;
    this.paymentAction = 'edit';
    this.openReceipt = true;
    this.receiptTitle = 'Edit Down Payment';
  }

  @action
  closeReceipt = () => {
    this.paymentAction = null;
    this.openReceipt = false;
    this.receiptTitle = '';
  }

  render() {
    const {
      history,
      salesStore,
      viewStore,
      authStore,
    } = this.props;

    const dialogFooter = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Save"
          icon="pi pi-plus"
          onClick={async () => {
            const result = await salesStore.updateTransaction(
              this.transaction._id,
              {
                ...this.transaction,
                terms: +this.terms,
              },
            );

            if (result.success) {
              this.clearTerms();
              this.fetchTransaction();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    const dialogFooter1 = (
      <div className="ui-dialog-buttonpane p-clearfix">
        <Button
          style={{ float: 'right' }}
          label="Save"
          icon="pi pi-plus"
          onClick={async () => {
            const result = await salesStore.updateTransaction(
              this.transaction._id,
              {
                ...this.transaction,
                terms: +this.customTerms,
                customTotalPrice: +this.customPayments,
              },
              true,
              true,
            );

            if (result.success) {
              this.clearCustom();
              this.fetchTransaction();
            } else if (result.errorDetails) {
              viewStore.showValidationErrors(result.errorDetails);
            } else {
              viewStore.showError(result.error);
            }
          }}
        />
      </div>
    );

    return (
      <div>
        {this.transaction && (
          <TabView className="p-tabview">
            <TabPanel
              header={`Payments - Transaction Number: ${this.transaction.transactionNumber}`}
            >
              <Toolbar>
                <div className="p-toolbar-group-left">
                  <Button
                    label="Back"
                    icon="pi pi-angle-left"
                    onClick={() => { history.push('/accounting'); }}
                  />
                </div>
                {
                  !this.transaction.isPaymentComplete && (
                    <div className="p-toolbar-group-right">
                      <Button
                        label="Add Custom Terms & Payment"
                        style={{ marginRight: '4px' }}
                        disabled={!authStore.isAdmin}
                        onClick={this.addCustom}
                      />
                      {/* <Button
                        label="Increase Terms"
                        onClick={this.increaseTerms}
                        disabled={this.transaction.customTotalPrice}
                        style={{ marginRight: '4px' }}
                      /> */}
                      <Button
                        label="Add Down Payment"
                        disabled={this.transaction.hasDownPayment}
                        onClick={this.payDownPayment}
                      />
                    </div>
                  )
                }
              </Toolbar>
              <div
                style={{
                  width: '100%',
                  marginBottom: '8px',
                  marginTop: '8px',
                  display: 'flex',
                  justifyContent: 'space-evenly',
                  alignItems: 'center',
                  paddingBottom: '8px',
                  borderBottom: '0.5px solid #c8c8c8',
                }}
              >
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Total Balance: </b>{beautifyPrice(this.transaction.totalPrice)}
                </div>
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Total Payment: </b>{beautifyPrice(this.transaction.totalPayment)}
                </div>
                <div
                  style={{
                    border: '0.5px solid #c8c8c8',
                    borderRadius: '3px',
                    display: 'inline',
                    padding: '8px',
                    background: '#f4f4f4',
                  }}
                >
                  <b>Remaining Balance: </b>{beautifyPrice(this.transaction.remainingBalance)}
                </div>
              </div>
              <div style={{ overflowY: 'scroll', height: '60vh' }}>
                <h3>Down Payments</h3>
                {
                  this.transaction.downPayments.length > 0
                    ? (this.transaction.downPayments.map(v => (
                      <P
                        payment={v}
                        key={+v.date}
                        myAction={this.editDownPayment}
                      />
                    )))
                    : <div style={{ marginLeft: '8px' }}>no records found</div>
                }
                <h3>Payed Amortized Loan</h3>
                {
                  this.transaction.loanAmortizations.length > 0
                    ? (this.transaction.loanAmortizations.map(v => (
                      <P
                        payment={v}
                        key={+v.date}
                        myAction={this.editAmortizedLoan}
                      />
                    )))
                    : <div style={{ marginLeft: '8px' }}>no records found</div>
                }
                <h3>Unpayed Amortized Loan</h3>
                {
                  this.transaction.virtualPayments.length > 0
                    ? (this.transaction.virtualPayments.map(v => (
                      <UnpayedAmortizedLoan
                        payment={v}
                        key={+v.amortizationDate}
                        payAmortizedLoan={this.payAmortizedLoan}
                      />
                    )))
                    : <div style={{ marginLeft: '8px' }}>no records found</div>
                }
                {this.openReceipt && (
                  <Receipt
                    payment={this.openedPayment}
                    openReceipt={this.openReceipt}
                    closeReceipt={this.closeReceipt}
                    transactionId={this.transaction._id}
                    callback={this.fetchTransaction}
                    title={this.receiptTitle}
                    paymentAction={this.paymentAction}
                  />
                )}
                <Dialog
                  visible={this.willIncrease}
                  style={{ width: '200px' }}
                  header="Increase Terms"
                  modal={false}
                  footer={dialogFooter}
                  onHide={action(() => { this.willIncrease = false; })}
                >
                  {this.willIncrease && (
                    <div style={{ display: 'flex', width: '100%' }}>
                      <div style={{ fontWeight: 700, marginRight: '4px' }}>Terms: </div>
                      <Dropdown
                        value={this.terms}
                        options={this.termOptions}
                        onChange={action((e) => { this.terms = e.value; })}
                      />
                    </div>
                  )}
                </Dialog>
                <Dialog
                  visible={this.willCustomize}
                  style={{ width: '350px' }}
                  header="Increase Terms"
                  modal={false}
                  footer={dialogFooter1}
                  onHide={this.clearCustom}
                >
                  {this.willCustomize && (
                    <div>
                      <div
                        style={{
                          display: 'flex',
                          width: '100%',
                          marginBottom: '8px',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <div
                          style={{
                            fontWeight: 700,
                            marginRight: '4px',
                          }}
                        >
                            Terms:
                        </div>
                        <InputText
                          keyfilter="pnum"
                          value={this.customTerms}
                          onChange={action((e) => { this.customTerms = e.target.value; })}
                        />
                      </div>
                      <div
                        style={{
                          display: 'flex',
                          width: '100%',
                          alignItems: 'center',
                          justifyContent: 'space-between',
                        }}
                      >
                        <div
                          style={{
                            fontWeight: 700,
                            marginRight: '4px',
                          }}
                        >
                          Total Price:
                        </div>
                        <InputText
                          keyfilter="pnum"
                          value={this.customPayments}
                          onChange={action((e) => { this.customPayments = e.target.value; })}
                        />
                      </div>
                    </div>
                  )}
                </Dialog>
              </div>
            </TabPanel>
          </TabView>
        )}
      </div>
    );
  }
}

export default inject(
  'salesStore',
  'viewStore',
  'authStore',
)(observer(Payments));

import React from 'react';

import {
  observable,
  computed,
  action,
} from 'mobx';
import {
  observer,
  inject,
} from 'mobx-react';

import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Toolbar } from 'primereact/toolbar';
import { Calendar } from 'primereact/calendar';
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';

import { beautifyPrice, timeEndDate, timeStartDate } from '../../../utils/helper';

const pastTenseThis = (myAction) => {
  let finalAction = myAction;

  if (myAction === 'patch') {
    finalAction = 'update';
  } else if (myAction === 'remove') {
    finalAction = 'delete';
  }

  if (finalAction[finalAction.length - 1] === 'e') {
    return `${finalAction}d`;
  }

  return `${finalAction}ed`;
};

const Expenses = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} an `}<u>expense</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>tin number:</b></div>
        <div>{l.prevState.tinNumber}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>establishment:</b></div>
        <div>{l.prevState.establishment}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>remarks:</b></div>
        <div>{l.prevState.remarks}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>amount:</b></div>
        <div>{beautifyPrice(l.prevState.amount)}</div>
      </div>
    </div>
  </div>
);

const Items = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} an `}<u>item</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>code:</b></div>
        <div>{l.prevState.code}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>description:</b></div>
        <div>{l.prevState.description}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>quantity:</b></div>
        <div>{l.prevState.quantity}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>price:</b></div>
        <div>{beautifyPrice(+l.prevState.price)}</div>
      </div>
    </div>
  </div>
);

const Sets = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>set</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>code:</b></div>
        <div>{l.prevState.code}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>description:</b></div>
        <div>{l.prevState.description}</div>
      </div>
    </div>
  </div>
);

const DR = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>delivery receipt</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>dr number:</b></div>
        <div>{l.prevState.drNumber}</div>
      </div>
      {
        l.prevState.transactionItems.map(ti => (
          <div key={ti.transactionItemId}>
            <div style={{ display: 'flex' }}>
              <div style={{ marginRight: '4px' }}><b>description:</b></div>
              <div>{ti.description}</div>
            </div>
            <div style={{ display: 'flex' }}>
              <div style={{ marginRight: '4px' }}><b>items:</b></div>
              <div>{ti.items}</div>
            </div>
          </div>
        ))
      }
      <div style={{ display: 'flex', borderTop: '0.5px solid #c8c8c8' }}>
        <div style={{ marginRight: '4px' }}><b>transaction number:</b></div>
        <div>{l.transaction.transactionNumber}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>client:</b></div>
        <div>{l.transaction.client.name}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>consultant:</b></div>
        <div>{l.transaction.consultant.name}</div>
      </div>
    </div>
  </div>
);

const Payment = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>payment</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>provisionary number:</b></div>
        <div>{l.prevState.provisionaryNumber}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>payment:</b></div>
        <div>{l.prevState.paymentOption}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>amount:</b></div>
        <div>{beautifyPrice(+l.prevState.amount)}</div>
      </div>
      <div style={{ display: 'flex', borderTop: '0.5px solid #c8c8c8' }}>
        <div style={{ marginRight: '4px' }}><b>transaction number:</b></div>
        <div>{l.transaction.transactionNumber}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>client:</b></div>
        <div>{l.transaction.client.name}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>consultant:</b></div>
        <div>{l.transaction.consultant.name}</div>
      </div>
    </div>
  </div>
);

const Commission = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>commission</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>bank:</b></div>
        <div>{l.prevState.bank}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>amount:</b></div>
        <div>{beautifyPrice(+l.prevState.amount)}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>Profile:</b></div>
        <div>{l.prevState.profile.name}</div>
      </div>
      <div style={{ display: 'flex', borderTop: '0.5px solid #c8c8c8' }}>
        <div style={{ marginRight: '4px' }}><b>transaction number:</b></div>
        <div>{l.transaction.transactionNumber}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>client:</b></div>
        <div>{l.transaction.client.name}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>consultant:</b></div>
        <div>{l.transaction.consultant.name}</div>
      </div>
    </div>
  </div>
);

const B = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>bank transfer</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>From:</b></div>
        <div>{l.prevState.from}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>To:</b></div>
        <div>{l.prevState.to}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>amount:</b></div>
        <div>{beautifyPrice(+l.prevState.amount)}</div>
      </div>
    </div>
  </div>
);

const Balance = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>standing balance</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>total balance:</b></div>
        <div>{beautifyPrice(l.prevState.totalBalance)}</div>
      </div>
    </div>
  </div>
);

const BalancePayment = ({ l }) => (
  <div style={{ paddingLeft: '16px' }}>
    <span
      style={{ textTransform: 'uppercase' }}
    >
      {`${l.doer} ${pastTenseThis(l.method)} a `}<u>payment for a standing balance</u>
    </span>
    <div
      style={{
        marginLeft: '4px',
        paddingLeft: '6px',
        borderLeft: '0.5px solid black',
      }}
    >
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>amount:</b></div>
        <div>{beautifyPrice(l.prevState.amount)}</div>
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ marginRight: '4px' }}><b>bank:</b></div>
        <div>{l.prevState.bank}</div>
      </div>
    </div>
  </div>
);

class Logs extends React.Component {
  @observable search = '';

  @observable searchFilter = 'client';

  @observable range = null;

  @observable methodFilter = 'ALL';

  @computed
  get logs() {
    const { adminStore, myLogs } = this.props;

    let logs = adminStore.descendingLogs.filter(l => myLogs.includes(l.service));

    if (this.methodFilter !== 'ALL') {
      logs = logs.filter(l => l.method === this.methodFilter);
    }

    if (this.range) {
      const [d1, d2] = this.range;

      return logs.filter(t => (
        t.date < timeEndDate(d2) && t.date > timeStartDate(d1)
      ));
    }

    return logs;
  }

  @computed
  get result() {
    const profiles = ['client', 'consultant'];

    if (this.search === '') {
      return this.logs;
    }

    if (profiles.includes(this.searchFilter)) {
      return this.logs.filter(t => (
        t.transaction
        && t.transaction[this.searchFilter]
        && t.transaction[this.searchFilter].name.toLowerCase().includes(this.search.toLowerCase())
      ));
    }

    if (this.searchFilter === 'employee') {
      return this.logs.filter(l => l.doer.toLowerCase().includes(this.search.toLowerCase()));
    }

    return this.logs.filter(t => (
      t.transaction
      && String(t.transaction[this.searchFilter]).toLowerCase().includes(this.search.toLowerCase())
    ));
  }

  render() {
    const { adminStore, viewStore, full } = this.props;

    const searchOptions = [
      { label: 'client', value: 'client' },
      { label: 'consultant', value: 'consultant' },
      { label: 'employee', value: 'employee' },
      { label: 'transaction number', value: 'transactionNumber' },
      { label: 'terms', value: 'terms' },
    ];

    const methodOptions = [
      { label: 'All', value: 'ALL' },
      { label: 'Create', value: 'create' },
      { label: 'Update', value: 'patch' },
      { label: 'Delete', value: 'remove' },
    ];

    return (
      <Card>
        <Toolbar>
          <div className="p-toolbar-group-left">
            <div className="p-col-12 p-md-4">
              <div className="p-inputgroup" style={{ display: 'flex', alignItems: 'center' }}>
                <span style={{ display: full ? 'inline' : 'none' }}>
                  <InputText
                    value={this.search}
                    onChange={action((e) => { this.search = e.target.value; })}
                    placeholder="Search"
                  />
                  <Dropdown
                    value={this.searchFilter}
                    options={searchOptions}
                    onChange={action((e) => { this.searchFilter = e.value; })}
                  />
                </span>
                <span
                  className="p-inputgroup-addon"
                  style={{ marginLeft: full ? '32px' : '' }}
                >
                  <b>Filter by action:</b>
                </span>
                <Dropdown
                  value={this.methodFilter}
                  options={methodOptions}
                  onChange={action((e) => { this.methodFilter = e.value; })}
                />
              </div>
            </div>
          </div>
          <div className="p-toolbar-group-right">
            <Calendar
              value={this.range}
              onChange={action((e) => {
                this.range = e.value;
              })}
              selectionMode="range"
              readOnlyInput
              dateFormat="MM dd, yy"
              inputStyle={{ width: '300px', textAlign: 'center' }}
              placeholder="Date From - Date To"
              showButtonBar
            />
          </div>
        </Toolbar>
        <div style={{ overflowY: 'scroll', height: '60vh' }}>
          {
            this.result.map(l => (
              <div
                key={l._id}
                style={{
                  border: '0.5px solid #c8c8c8',
                  padding: '8px',
                  marginBottom: '8px',
                }}
              >
                <div style={{ fontWeight: 600, marginBottom: '16px' }}>
                  {l.date.toLocaleString('en-US', {
                    year: 'numeric', month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric',
                  })}
                </div>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                  {(() => {
                    if (l.service === 'expenses') {
                      return <Expenses l={l} />;
                    }

                    if (l.service === 'items') {
                      return <Items l={l} />;
                    }

                    if (l.service === 'sets') {
                      return <Sets l={l} />;
                    }

                    if (l.service === 'deliveryReceipts') {
                      return <DR l={l} />;
                    }

                    if (l.service === 'payments') {
                      return <Payment l={l} />;
                    }

                    if (l.service === 'commissions') {
                      return <Commission l={l} />;
                    }

                    if (l.service === 'bankTransfers') {
                      return <B l={l} />;
                    }

                    if (l.service === 'balance') {
                      return <Balance l={l} />;
                    }

                    if (l.service === 'balancePayment') {
                      return <BalancePayment l={l} />;
                    }

                    return <div />;
                  })()}
                  <Button
                    label="Revert"
                    onClick={async () => {
                      const result = await adminStore.revert(l);

                      if (result.success) {
                        await adminStore.removeLog(l._id);
                      } else {
                        viewStore.showError(result.error);
                      }
                    }}
                  />
                </div>
              </div>
            ))
          }
        </div>
      </Card>
    );
  }
}

export default inject(
  'adminStore',
  'viewStore',
)(observer(Logs));

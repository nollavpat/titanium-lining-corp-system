import React from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';

const Loading = () => (
  <div style={{ height: '100%', width: '100%' }}>
    <div
      style={{
        position: 'absolute',
        width: '64px',
        height: '64px',
        top: '50%',
        left: '50%',
        margin: '-32px 0 0 -32px',
        zIndex: 5,
      }}
    >
      <ProgressSpinner animationDuration=".5s" strokeWidth="8" fill="#EEEEEE" />
    </div>
  </div>
);

export default Loading;

import React from 'react';
import { observer, inject } from 'mobx-react';
import { Redirect } from 'react-router-dom';

import roles from '../../../utils/roles';

const withAuthorization = (defaultSchema, ...otherSchemas) => (
  function execute(WrappedComponent) {
    const Component = (props) => {
      const { authStore } = props;
      const role = authStore.hasUser ? authStore.currentUser.role : roles.NOT_AUTHENTICATED;
      let toBeRedirectedTo;

      if (otherSchemas.length > 0) {
        otherSchemas.forEach((schema) => {
          if (schema.allowedRoles.includes(role)) {
            toBeRedirectedTo = schema.redirectPath;
          }
        });
      }

      if (!toBeRedirectedTo) {
        const isRoleOnDefaultAllowedRoles = defaultSchema.allowedRoles.includes(role);
        const bypass = defaultSchema.nin
          ? isRoleOnDefaultAllowedRoles
          : !isRoleOnDefaultAllowedRoles;

        if (bypass) {
          toBeRedirectedTo = defaultSchema.redirectPath;
        }
      }

      if (toBeRedirectedTo) {
        return <Redirect to={toBeRedirectedTo} />;
      }

      return <WrappedComponent {...props} />;
    };

    return inject('authStore')(observer(Component));
  }
);

export default withAuthorization;

import React from 'react';
import { Switch } from 'react-router-dom';
// import Loadable from 'react-loadable';

// import Loading from './Loading';
import RouteWithSubRoutes from './RouteWithSubRoutes';
import Shu from './Shu';

import withAuthorization from './wrappers/withAuthorization';

import roles from '../../utils/roles';

import Inventory from './Inventory';
import LogIn from './Auth/LogIn';
import SignUp from './Auth/SignUp';
import Admin from './Admin';
import Sales from './Sales';
import ProfileAdd from './Sales/ProfileAdd';
import ProfileEdit from './Sales/ProfileEdit';
import DeliveryReceipts from './Sales/Transactions/DeliveryReceipts';
import Accounting from './Accounting';
import Transaction from './Accounting/Payments';

const UnAuthenticated = withAuthorization({
  allowedRoles: [roles.NOT_AUTHENTICATED],
  redirectPath: '/',
});

const ForSales = withAuthorization({
  allowedRoles: [roles.SALES, roles.SUPERADMIN, roles.ADMIN],
  redirectPath: '/login',
});

const ForInventory = withAuthorization({
  allowedRoles: [roles.INVENTORY, roles.SUPERADMIN, roles.ADMIN],
  redirectPath: '/login',
});

const ForAccounting = withAuthorization({
  allowedRoles: [roles.ACCOUNTING, roles.SUPERADMIN, roles.ADMIN],
  redirectPath: '/login',
});

const ForAdmin = withAuthorization({
  allowedRoles: [roles.SUPERADMIN, roles.ADMIN],
  redirectPath: '/login',
});

const routes = [
  {
    path: '/',
    component: Shu,
    exact: true,
  },
  {
    path: '/login',
    component: UnAuthenticated(LogIn),
  },
  {
    path: '/signup',
    component: UnAuthenticated(SignUp),
  },
  {
    path: '/inventory',
    component: ForInventory(Inventory),
  },
  {
    path: '/accounting/transaction-payments/:id',
    component: ForAccounting(Transaction),
  },
  {
    path: '/accounting',
    component: ForAccounting(Accounting),
  },
  {
    path: '/admin',
    component: ForAdmin(Admin),
  },
  {
    path: '/sales/profile-add',
    component: ForSales(ProfileAdd),
  },
  {
    path: '/sales/profile-edit/:id',
    component: ForSales(ProfileEdit),
  },
  {
    path: '/sales/transaction-delivery-receipts/:id',
    component: ForSales(DeliveryReceipts),
  },
  {
    path: '/sales',
    component: ForSales(Sales),
  },
];

const RouteConfig = () => (
  <Switch>
    {routes.map(route => (
      <RouteWithSubRoutes key={route.path} {...route} />
    ))}
  </Switch>
);

export default RouteConfig;

import React from 'react';

import { Redirect } from 'react-router-dom';

import {
  observer,
  inject,
} from 'mobx-react';

import roles from '../../utils/roles';

const Shu = ({ authStore }) => {
  const { currentUser } = authStore;
  const { role } = currentUser || roles.NONE;

  return (
    <div>
      {(() => {
        if (role === roles.INVENTORY) {
          return <Redirect to="/inventory" />;
        }
        if (role === roles.SALES) {
          return <Redirect to="/sales" />;
        }
        if (role === roles.ACCOUNTING) {
          return <Redirect to="/accounting" />;
        }
        if (role === roles.SUPERADMIN) {
          return <Redirect to="/admin" />;
        }
        return <Redirect to="/login" />;
      })()}
    </div>
  );
};

export default inject('authStore')(observer(Shu));

import {
  flow,
  observable,
  computed,
} from 'mobx';

import Set from '../../models/Set';
import Item from '../../models/Item';
import ValidationError from '../../utils/errors/ValidationError';

class InventoryStore {
  @observable inventory = undefined;

  @observable cleanSets = [];

  constructor(rootStore, client) {
    Object.assign(this, { ...rootStore, client });
  }

  @computed get inventoryInSets() {
    return this.sets.map(set => set.components).flat(1);
  }

  getInventory = flow(function* () {
    this.inventory = [];

    try {
      const itemsService = this.client.service('api/items');

      this.inventory = yield itemsService.find();
    } catch (e) {
      this.viewStore.showError(e);
    }
  })

  createItem = flow(function* (item, fromRevert = false) {
    try {
      const itemsService = this.client.service('api/items');
      const newItem = yield itemsService.create({
        ...item,
        prevQuantity: 0,
        borrowedItems: [],
      }, { meta: { fromRevert } });

      this.inventory = this.inventory.concat(newItem);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  addBorrower = async (itemId, bi) => {
    const item = this.inventory.find(i => i._id === itemId);

    return this.updateItem(
      itemId,
      {
        ...item,
        borrowedItems: item.borrowedItems.concat(bi),
      },
      false,
    );
  }

  removeBorrower = async (itemId, bi) => {
    const item = this.inventory.find(i => i._id === itemId);

    return this.updateItem(
      itemId,
      {
        ...item,
        borrowedItems: bi,
      },
      false,
    );
  }

  updateItem = flow(function* (id, item, log = true, fromRevert = false) {
    try {
      const itemsService = this.client.service('api/items');
      const thisItem = this.inventory.find(i => i._id === id);
      console.log(thisItem.availableQuantity);
      let updatedItem;

      if (log) {
        updatedItem = yield itemsService.patch(id, item, { meta: { fromRevert } });
      } else {
        updatedItem = yield itemsService.update(id, item, { meta: { fromRevert } });
      }

      this.inventory = this.inventory.map((i) => {
        if (i._id === id) {
          return updatedItem;
        }

        return i;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  deleteItem = flow(function* (id, fromRevert = false) {
    try {
      const itemsService = this.client.service('api/items');
      yield itemsService.remove(id, { meta: { fromRevert } });
      yield this.getSets();

      this.inventory = this.inventory.filter(item => item._id !== id);

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  getSets = flow(function* () {
    this.cleanSets = [];

    try {
      const setsService = this.client.service('api/sets');
      const s = yield setsService.find();

      this.cleanSets = s;
    } catch (e) {
      this.viewStore.showError(e);
    }
  })

  createSet = flow(function* (set, fromRevert = false) {
    try {
      const setsService = this.client.service('api/sets');
      const newSet = yield setsService.create(set, { meta: { fromRevert } });

      this.cleanSets = this.cleanSets.concat(newSet);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  updateSet= flow(function* (id, set, log = true, fromRevert = false) {
    try {
      const setsService = this.client.service('api/sets');
      let updatedSet;

      if (log) {
        updatedSet = yield setsService.patch(id, set, { meta: { fromRevert } });
      } else {
        updatedSet = yield setsService.update(id, set, { meta: { fromRevert } });
      }

      this.cleanSets = this.cleanSets.map((s) => {
        if (s._id === id) {
          return updatedSet;
        }

        return s;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  deleteSet = flow(function* (id, fromRevert = false) {
    try {
      const setsService = this.client.service('api/sets');
      yield setsService.remove(id, { meta: { fromRevert } });

      this.cleanSets = this.cleanSets.filter(set => set._id !== id);

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  updateInventory = flow(function* (transactionItems) {
    try {
      yield this.getInventory();
      yield Promise.all(transactionItems.map(async (ti) => {
        const itemsService = this.client.service('api/items');
        let item;

        if (ti.isSet) {
          item = this.cleanSets.find(i => i._id === ti.transactionItemId);

          return Promise.all(item.componentsMap.map(async (c) => {
            const { itemId } = c;
            const cItem = this.inventory.find(i => i._id === itemId);

            return itemsService.update(cItem._id, {
              ...cItem, quantity: cItem.quantity - (ti.items * c.quantity),
            });
          }));
        }

        item = this.inventory.find(i => i._id === ti.transactionItemId);

        return itemsService.update(item._id, { ...item, quantity: item.quantity - ti.items });
      }));
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get sets() {
    return this.cleanSets.map(set => (new Set({
      ...set,
      components: set.componentsMap.map(c => new Item({
        ...this.inventory.find(i => i._id === c.itemId),
        setQuantity: c.quantity,
        setId: set._id,
      })),
    })));
  }

  @computed
  get inventoryReport() {
    const inv = [
      ...this.inventory,
      ...this.sets.map(s => ({
        _id: s._id,
        code: s.code,
        description: s.description,
        price: s.lessedPrice,
      })),
    ];

    return inv;
  }
}

export default InventoryStore;

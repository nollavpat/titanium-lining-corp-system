import {
  observable,
  flow,
  computed,
  action,
} from 'mobx';

import roles from '../../utils/roles';
import NoRoleError from '../../utils/errors/NoRoleError';
import ValidationError from '../../utils/errors/ValidationError';

class AuthStore {
  @observable currentUser = undefined;

  @observable doneAuthenticating = false;

  @observable users = undefined;

  constructor(rootStore, client) {
    Object.assign(this, { ...rootStore, client });
  }

  @computed
  get userRole() {
    return this.currentUser.role;
  }

  @computed
  get hasUser() {
    return !!this.currentUser;
  }

  get isAdmin() {
    return this.currentUser.role === roles.SUPERADMIN;
  }

  @action
  finishAuthenticating = () => {
    this.doneAuthenticating = true;
  }

  retrieveAll = flow(function* () {
    if (this.hasUser) {
      yield this.inventoryStore.getInventory();
      // yield this.inventoryStore.getSets();
      yield this.adminStore.getConstants();
      yield this.salesStore.getProfiles();
      yield this.accountingStore.getExpenses();
      yield this.adminStore.getLogs();
      yield this.salesStore.getTransactions();
      yield this.accountingStore.getBalances();
    }
  })

  createUser = flow(function* (user) {
    try {
      const usersService = this.client.service('api/users');

      yield usersService.create({
        ...user,
        pin: user.password,
        role: roles.NONE,
      });

      // window.location.replace(`${window.location.host}/#/login`);
      // window.location.replace('http://localhost:3030/#/login');

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  login = flow(function* (user = null) {
    try {
      const usersService = this.client.service('api/users');

      const credentials = user
        ? {
          strategy: 'local',
          username: user.username,
          password: user.password,
        }
        : undefined;

      const { accessToken } = yield this.client.authenticate(credentials);
      const { userId } = yield this.client.passport.verifyJWT(accessToken);

      const loggedUser = yield usersService.get(userId);

      if (loggedUser.role === roles.NONE) {
        yield this.logout();
        throw new NoRoleError('You have no role. Request change from Admin.');
      }

      this.client.set('user', loggedUser);
      this.currentUser = loggedUser;

      yield this.retrieveAll();

      // window.location.replace('http://localhost:3030/#/');
      // window.location.replace(`${window.location.host}/#/`);
    } catch (error) {
      if (error instanceof NoRoleError) {
        this.viewStore.showWarning(error);
      } else {
        this.viewStore.showError(error);
      }
    }
  })

  logout = flow(function* () {
    try {
      yield this.client.logout();
      this.currentUser = undefined;

      // window.location.replace('http://localhost:3030/#/');
      // window.location.replace(`${window.location.host}/#/login`);
    } catch (error) {
      this.viewStore.showError(error);
    }
  })
}

export default AuthStore;

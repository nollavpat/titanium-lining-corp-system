import {
  observable,
  flow,
  computed,
} from 'mobx';

import TransactionItem from '../../models/TransactionItem';
import Transaction from '../../models/Transaction';

import ValidationError from '../../utils/errors/ValidationError';

class SalesStore {
  @observable profiles = undefined;

  @observable transactions = [];

  constructor(rootStore, client) {
    Object.assign(this, { ...rootStore, client });
  }

  createProfiles = flow(function* (profile) {
    try {
      const profilesService = this.client.service('api/profiles');
      const newProfile = yield profilesService.create(profile);

      this.profiles = this.profiles.concat(newProfile);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  updateProfile= flow(function* (id, profile) {
    try {
      const profilesService = this.client.service('api/profiles');
      const updatedProfile = yield profilesService.patch(id, profile);

      this.profiles = this.profiles.map((p) => {
        if (p._id === id) {
          return updatedProfile;
        }

        return p;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  deleteProfile= flow(function* (id, profile) {
    try {
      const profilesService = this.client.service('api/profiles');
      const updatedProfile = yield profilesService.patch(id, profile);

      this.profiles = this.profiles.map((p) => {
        if (p._id === id) {
          return updatedProfile;
        }

        return p;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  uploadImage = async (reader) => {
    try {
      const uploadService = this.client.service('api/uploads');

      return uploadService.create({ uri: reader.result });
    } catch (error) {
      this.viewStore.showError(error);

      return null;
    }
  }

  getProfiles = flow(function* () {
    this.profiles = [];
    try {
      const profilesService = this.client.service('api/profiles');
      const profiles = yield profilesService.find();

      this.profiles = profiles;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get validProfiles() {
    return this.profiles.filter(p => !p.isRemoved);
  }

  @computed
  get transactionItems() {
    const { inventory, sets } = this.inventoryStore;

    const items = [
      ...inventory.map(i => ({
        transactionItemId: i._id,
        description: i.description,
        price: i.price,
        isSet: false,
        isGift: false,
        items: 0,
        currentQuantity: i.availableQuantity,
      })),
      ...sets.map(s => ({
        transactionItemId: s._id,
        description: s.description,
        price: s.lessedPrice,
        isSet: true,
        isGift: false,
        items: 0,
        currentQuantity: s.quantity,
      })),
    ];

    return items.map(i => new TransactionItem(i));
  }

  updateTransactionError = errorDetails => (
    errorDetails.map((detail) => {
      if (detail.path[0] === 'clientId') {
        return {
          ...detail,
          patch: ['client'],
          message: '"Client" is required.',
        };
      }

      if (detail.path[0] === 'consultantId') {
        return {
          ...detail,
          patch: ['consultant'],
          message: '"Consultant" is required.',
        };
      }

      return detail;
    })
  )

  createTransaction = flow(function* (transaction) {
    try {
      const transactionsService = this.client.service('api/transactions');
      const newTransaction = yield transactionsService.create(transaction);

      this.transactions = this.transactions.concat(newTransaction);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: this.updateTransactionError(error.errorDetails),
        };
      }

      return { error, success: false };
    }
  })

  updateTransaction = flow(function* (id, transaction, log = true, fromRevert = false) {
    try {
      const transactionsService = this.client.service('api/transactions');
      let updatedTransaction;
      if (log) {
        updatedTransaction = yield transactionsService.patch(id, transaction, { meta: { fromRevert } });
      } else {
        updatedTransaction = yield transactionsService.update(id, transaction, { meta: { fromRevert } });
      }

      this.transactions = this.transactions.map((t) => {
        if (t._id === id) {
          return updatedTransaction;
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: this.updateTransactionError(error.errorDetails),
        };
      }

      return { error, success: false };
    }
  })

  getTransactions = flow(function* () {
    this.transactions = [];

    try {
      const transactionsService = this.client.service('api/transactions');
      const transactions = yield transactionsService.find();

      this.transactions = transactions;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get descendingTransactions() {
    return [...this.transactions].sort((a, b) => {
      if (a.date > b.date) {
        return -1;
      }

      if (a.date < b.date) {
        return 1;
      }

      return 0;
    });
  }

  @computed
  get archivedTransactions() {
    return this.descendingTransactions.filter(t => t.archived);
  }

  @computed
  get normalTransactions() {
    return this.descendingTransactions.filter(t => !t.archived);
  }

  @computed
  get completelyPayedTransactions() {
    return this.normalTransactions.filter(t => t.isPaymentComplete);
  }

  @computed
  get incompletelyPayedTransactions() {
    return this.normalTransactions.filter(t => !t.isPaymentComplete);
  }

  createDR = flow(function* (dr) {
    try {
      const { transactionId } = dr;
      const drsService = this.client.service('api/deliveryReceipts');
      const newDr = yield drsService.create(dr);

      this.transactions = this.transactions.map((t) => {
        if (t._id === transactionId) {
          return new Transaction({
            ...t,
            deliveryReceipts: t.deliveryReceipts.concat(newDr),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeDR = flow(function* (id) {
    try {
      const drsService = this.client.service('api/deliveryReceipts');
      const dr = yield drsService.remove(id, { meta: { fromRevert: true } });

      this.transactions = this.transactions.map((t) => {
        if (t._id === dr.transactionId) {
          return new Transaction({
            ...t,
            deliveryReceipts: t.deliveryReceipts.filter(d => d._id !== id),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  createCommissionExpense = flow(function* (expense, fromRevert = false) {
    try {
      const ceService = this.client.service('api/commissionPayments');
      yield ceService.create(expense, { meta: { fromRevert } });

      yield this.getTransactions();
      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeCommissionExpense = flow(function* (id) {
    try {
      const ceService = this.client.service('api/commissionPayments');
      yield ceService.remove(id, { meta: { fromRevert: true } });

      yield this.getTransactions();

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  addCommission = flow(function* (commission) {
    try {
      const commissionsService = this.client.service('api/commissions');

      const newCommision = yield commissionsService.create(commission);

      this.transactions = this.transactions.map((t) => {
        if (t._id === commission.transactionId) {
          return new Transaction({
            ...t,
            commision: newCommision,
          });
        }

        return t;
      });

      yield this.getTransactions();

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  updateCommission = flow(function* (id, commission, log = true, fromRevert = false) {
    try {
      const commissionsService = this.client.service('api/commissions');

      let newCommision;
      if (log) {
        newCommision = yield commissionsService.patch(id, commission, { meta: { fromRevert } });
      } else {
        newCommision = yield commissionsService.update(id, commission, { meta: { fromRevert } });
        console.log(newCommision);
      }

      this.transactions = this.transactions.map((t) => {
        if (t._id === commission.transactionId) {
          return new Transaction({
            ...t,
            commissions: t.commissions.map((p) => {
              if (p._id === id) {
                return newCommision;
              }

              return p;
            }),
          });
        }

        return t;
      });

      yield this.getTransactions();

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeCommission = flow(function* (id) {
    try {
      const commissionsService = this.client.service('api/commissions');

      const newCommision = yield commissionsService.remove(id, { meta: { fromRevert: true } });

      this.transactions = this.transactions.map((t) => {
        if (t._id === newCommision.transactionId) {
          return new Transaction({
            ...t,
            commision: null,
          });
        }

        return t;
      });

      yield this.getTransactions();
      yield this.adminStore.getConstants();

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })
}

export default SalesStore;

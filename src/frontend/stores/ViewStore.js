import {
  observable,
  action,
} from 'mobx';

import { beautifyPropertyName } from '../../utils/helper';

class ViewStore {
  @observable title = undefined;

  @action
  changeTitle = (title) => {
    this.title = title;
  }

  showError = (error) => {
    this.growl.show({ severity: 'error', summary: beautifyPropertyName(error.name), detail: error.message });
  }

  showWarning = (error) => {
    this.growl.show({ severity: 'warn', summary: beautifyPropertyName(error.name), detail: error.message });
  }

  showValidationErrors = (errorDetails) => {
    const beautifiedErrorDetails = errorDetails.map(detail => ({
      ...detail,
      message: detail.message.replace(detail.path[0], beautifyPropertyName(detail.path[0])),
    }));

    this.growl.show(beautifiedErrorDetails.map(detail => ({
      severity: 'error',
      summary: 'Invalid Data',
      detail: detail.message,
    })));
  }
}

export default ViewStore;

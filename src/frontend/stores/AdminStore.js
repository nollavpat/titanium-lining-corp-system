import {
  observable,
  flow,
  computed,
} from 'mobx';

import roles from '../../utils/roles';
import ValidationError from '../../utils/errors/ValidationError';

class AdminStore {
  @observable users = null;

  @observable constants = null;

  @observable logs = [];

  @observable bankTransfers = [];

  constructor(rootStore, client) {
    Object.assign(this, { ...rootStore, client });
  }

  bankTransfer = flow(function* (bt) {
    try {
      const constantsService = this.client.service('api/constants');
      const btService = this.client.service('api/bankTransfers');
      const from = yield constantsService.patch(bt.fromId, { $inc: { balance: -bt.amount } });
      const to = yield constantsService.patch(bt.toId, { $inc: { balance: +bt.amount } });
      yield btService.create(bt);

      this.constants = this.constants.map((c) => {
        if (c._id === bt.fromId) {
          return from;
        }

        if (c._id === bt.toId) {
          return to;
        }

        return c;
      });
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  reverBankTransfer = flow(function* (bt) {
    try {
      const constantsService = this.client.service('api/constants');
      const btService = this.client.service('api/bankTransfers');
      const from = yield constantsService.patch(bt.fromId, { $inc: { balance: +bt.amount } }, { meta: { fromRevert: true } });
      const to = yield constantsService.patch(bt.toId, { $inc: { balance: -bt.amount } }, { meta: { fromRevert: true } });
      yield btService.remove(bt._id, { meta: { fromRevert: true } });

      this.constants = this.constants.map((c) => {
        if (c._id === bt.fromId) {
          return from;
        }

        if (c._id === bt.toId) {
          return to;
        }

        return c;
      });
      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  getConstants = flow(function* () {
    this.constants = [];

    try {
      const constantsService = this.client.service('api/constants');

      const constants = yield constantsService.find();

      this.constants = constants;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  addConstant = flow(function* (constant) {
    try {
      const constantsService = this.client.service('api/constants');

      const newConstant = yield constantsService.create(constant);
      this.constants = this.constants.concat(newConstant);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  updateConstant = flow(function* (id, constant) {
    try {
      const constantsService = this.client.service('api/constants');

      const patchedConstant = yield constantsService.patch(id, {
        ...constant,
      });

      this.constants = this.constants.map((u) => {
        if (u._id === id) {
          return patchedConstant;
        }

        return u;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeConstant = flow(function* (id) {
    try {
      const constantsService = this.client.service('api/constants');

      yield constantsService.remove(id);

      this.constants = this.constants.filter(u => u._id !== id);
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get banks() {
    return this.constants.filter(c => c.type === 'BANK' || c.type === 'BUILT_IN_BANK');
  }

  @computed
  get adminBanks() {
    return this.constants.filter(c => c.type === 'BANK');
  }

  @computed
  get paymentBanks() {
    return this.constants.filter(c => c.type === 'BANK' || c.value === 'LAY AWAY');
  }

  @computed
  get expenseBanks() {
    return this.constants.filter(c => c.type === 'BANK' || c.value === 'PETTY CASH');
  }

  getNonSuperAdmin = flow(function* () {
    this.users = [];

    try {
      const usersService = this.client.service('api/users');

      const users = yield usersService.find({ query: { role: { $ne: roles.SUPERADMIN } } });

      this.users = users;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  updateUser = flow(function* (id, user) {
    try {
      const usersService = this.client.service('api/users');

      const patchedUser = yield usersService.patch(id, {
        ...user,
        password: user.pin,
      });

      this.users = this.users.map((u) => {
        if (u._id === id) {
          return patchedUser;
        }

        return u;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeUser = flow(function* (id) {
    try {
      const usersService = this.client.service('api/users');

      yield usersService.remove(id);

      this.users = this.users.filter(u => u._id !== id);
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  getLogs = flow(function* () {
    this.logs = [];
    try {
      const logsService = this.client.service('api/logs');
      const logs = yield logsService.find();

      this.logs = logs;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  removeLog = flow(function* (id) {
    try {
      const logsService = this.client.service('api/logs');
      yield logsService.remove(id);

      this.logs = this.logs.filter(l => l._id !== id);
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get descendingLogs() {
    return [...this.logs].sort((a, b) => {
      if (a.date > b.date) {
        return -1;
      }

      if (a.date < b.date) {
        return 1;
      }

      return 0;
    });
  }

  revert = flow(function* (log) {
    try {
      let result;

      if (log.service === 'expenses') {
        if (log.method === 'create') {
          result = yield this.accountingStore.removeExpense(log.prevState._id, true);
        }
      } else if (log.service === 'items') {
        if (log.method === 'create') {
          result = yield this.inventoryStore.deleteItem(log.prevState._id, true);
        } else if (log.method === 'patch') {
          result = yield this.inventoryStore.updateItem(log.prevState._id, log.prevState, false, true);
        } else if (log.method === 'remove') {
          result = yield this.inventoryStore.createItem(log.prevState, true);
        }
      } else if (log.service === 'sets') {
        if (log.method === 'create') {
          result = yield this.inventoryStore.deleteSet(log.prevState._id, true);
        } else if (log.method === 'patch') {
          result = yield this.inventoryStore.updateSet(log.prevState._id, log.prevState, false, true);
        } else if (log.method === 'remove') {
          result = yield this.inventoryStore.createSet(log.prevState, true);
        }
      } else if (log.service === 'deliveryReceipts') {
        if (log.method === 'create') {
          result = yield this.salesStore.removeDR(log.prevState._id);
        }
      } else if (log.service === 'payments') {
        if (log.method === 'create') {
          result = yield this.accountingStore.removePayment(log.prevState._id);
        } else if (log.method === 'patch') {
          result = yield this.accountingStore.updatePayment(log.prevState._id, log.prevState, false, true);
        }
      } else if (log.service === 'commissions') {
        if (log.method === 'create') {
          result = yield this.salesStore.removeCommissionExpense(log.prevState._id);
        }
      } else if (log.service === 'bankTransfers') {
        if (log.method === 'create') {
          result = yield this.reverBankTransfer(log.prevState);
        }
      } else if (log.service === 'balance') {
        if (log.method === 'create') {
          result = yield this.accountingStore.removeBalance(log.prevState._id);
        }
      } else if (log.service === 'balancePayment') {
        if (log.method === 'create') {
          result = yield this.accountingStore.removeBalancePayment(log.prevState._id);
        }
      }
      yield this.getConstants();
      return result;
    } catch (error) {
      return { error, success: false };
    }
  });
}

export default AdminStore;

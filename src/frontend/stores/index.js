import InventoryStore from './InventoryStore';
import ViewStore from './ViewStore';
import AuthStore from './AuthStore';
import SalesStore from './SalesStore';
import AccountingStore from './AccountingStore';
import AdminStore from './AdminStore';

import client from '../client';

class RootStore {
  constructor() {
    this.viewStore = new ViewStore(this);
    this.inventoryStore = new InventoryStore(this, client);
    this.salesStore = new SalesStore(this, client);
    this.accountingStore = new AccountingStore(this, client);
    this.adminStore = new AdminStore(this, client);
    this.authStore = new AuthStore(this, client);
  }
}

const rootStore = new RootStore();

export default rootStore;

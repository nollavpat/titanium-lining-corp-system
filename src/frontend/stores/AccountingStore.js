import {
  flow,
  observable,
  // action,
  computed,
} from 'mobx';

import ValidationError from '../../utils/errors/ValidationError';

import Transaction from '../../models/Transaction';
import Balance from '../../models/Balance';

class AccountingStore {
  @observable expenses = null;

  @observable balances = null;

  constructor(rootStore, client) {
    Object.assign(this, { ...rootStore, client });
  }

  createBalancePayment = flow(function* (payment, fromRevert = false) {
    try {
      const { balanceId } = payment;
      const paymentsService = this.client.service('api/balancePayments');
      const newPayment = yield paymentsService.create(payment, { meta: { fromRevert } });

      this.balances = this.balances.map((t) => {
        if (t._id === balanceId) {
          return new Balance({
            ...t,
            payments: t.payments.concat(newPayment),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeBalancePayment = flow(function* (id) {
    try {
      const paymentsService = this.client.service('api/balancePayments');
      const payment = yield paymentsService.remove(id, { meta: { fromRevert: true } });

      this.balances = this.balances.map((t) => {
        if (t._id === payment.balanceId) {
          return new Balance({
            ...t,
            payments: t.payments.filter(d => d._id !== id),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  getBalances = flow(function* () {
    this.balances = [];
    try {
      const balancesService = this.client.service('api/balances');
      const balances = yield balancesService.find();

      this.balances = balances;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  @computed
  get filteredBalances() {
    return this.balances.filter(b => !b.isComplete);
  }

  createBalance = flow(function* (balance) {
    try {
      const balancesService = this.client.service('api/balances');
      const newBalance = yield balancesService.create(balance, { meta: { fromRevert: false } });

      this.balances = this.balances.concat(newBalance);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeBalance = flow(function* (id) {
    try {
      const balancesService = this.client.service('api/balances');
      yield balancesService.remove(id, { meta: { fromRevert: true } });

      this.balances = this.balances.filter(b => b._id !== id);

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  createPayment = flow(function* (payment, fromRevert = false) {
    try {
      const { transactionId } = payment;
      const paymentsService = this.client.service('api/payments');
      const newPayment = yield paymentsService.create(payment, { meta: { fromRevert } });

      this.salesStore.transactions = this.salesStore.transactions.map((t) => {
        if (t._id === transactionId) {
          return new Transaction({
            ...t,
            payments: t.payments.concat(newPayment),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removePayment = flow(function* (id) {
    try {
      const paymentsService = this.client.service('api/payments');
      const payment = yield paymentsService.remove(id, { meta: { fromRevert: true } });

      this.salesStore.transactions = this.salesStore.transactions.map((t) => {
        if (t._id === payment.transactionId) {
          return new Transaction({
            ...t,
            payments: t.payments.filter(d => d._id !== id),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  updatePayment = flow(function* (id, payment, log = true, fromRevert = false) {
    try {
      const paymentsService = this.client.service('api/payments');
      let updatedPayment;

      if (log) {
        updatedPayment = yield paymentsService.patch(id, payment, { meta: { fromRevert } });
      } else {
        updatedPayment = yield paymentsService.update(id, payment, { meta: { fromRevert } });
      }

      this.salesStore.transactions = this.salesStore.transactions.map((t) => {
        if (t._id === payment.transactionId) {
          return new Transaction({
            ...t,
            payments: t.payments.map((p) => {
              if (p._id === id) {
                return updatedPayment;
              }

              return p;
            }),
          });
        }

        return t;
      });

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  getExpenses = flow(function* () {
    this.expenses = [];
    this.notifications = [];

    try {
      const expensesService = this.client.service('api/expenses');
      const expenseNotificationsService = this.client.service('api/expenseNotifications');
      const expenses = yield expensesService.find();
      const notifications = yield expenseNotificationsService.find({ query: { cleared: false } });

      this.notifications = notifications;
      this.expenses = expenses;
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  createExpense = flow(function* (expense, fromRevert = false) {
    try {
      const expensesService = this.client.service('api/expenses');
      const expenseNotificationsService = this.client.service('api/expenseNotifications');

      const newExpense = yield expensesService.create(expense, { meta: { fromRevert } });
      const notifications = yield expenseNotificationsService.find({ query: { cleared: false } });

      this.notifications = notifications;
      this.expenses = this.expenses.concat(newExpense);

      return { success: true };
    } catch (error) {
      if (error instanceof ValidationError) {
        return {
          error,
          success: false,
          errorDetails: error.errorDetails,
        };
      }

      return { error, success: false };
    }
  })

  removeExpense = flow(function* (id, fromRevert = false) {
    try {
      const expensesService = this.client.service('api/expenses');
      yield expensesService.remove(id, { meta: { fromRevert } });

      this.expenses = this.expenses.filter(l => l._id !== id);

      return { success: true };
    } catch (error) {
      return { error, success: false };
    }
  })

  readNotification = flow(function* (i) {
    try {
      const expenseNotificationsService = this.client.service('api/expenseNotifications');
      const { _id } = this.notifications[i];

      yield expenseNotificationsService.patch(_id, { cleared: true });

      this.notifications = this.notifications.filter(n => n._id !== _id);
    } catch (error) {
      this.viewStore.showError(error);
    }
  })

  showNotifications = () => {
    this.growl.clear();
    this.growl.show(this.notifications.map(n => ({
      sticky: true,
      severity: 'info',
      summary: 'Notification',
      detail: n.message,
    })));
  }

  @computed
  get hasNotifications() {
    return this.notifications.length > 0;
  }

  @computed
  get descendingExpenses() {
    return [...this.expenses].sort((a, b) => {
      if (a.date > b.date) {
        return -1;
      }

      if (a.date < b.date) {
        return 1;
      }

      return 0;
    });
  }

  @computed
  get allPayments() {
    return this.salesStore.transactions.reduce((acc, t) => [...acc, ...t.payments], []);
  }
}

export default AccountingStore;

const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const publicPath = path.resolve(`${process.cwd()}/dist/`);

module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [/node_modules/, /joi-browser/],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          },
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
          },
        ],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
            },
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader'],
      },
    ],
  },
  entry: './src/frontend/index.jsx',
  output: {
    filename: '[name].[contentHash].js',
    chunkFilename: '[name].[contentHash].js',
    path: path.join(`${publicPath}/js`),
    publicPath: 'js/',
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
    alias: {
      joi: 'joi-browser',
    },
  },
  plugins: [
    new CleanWebpackPlugin(['dist/js']),
    new HtmlWebPackPlugin({
      template: './templates/index.html',
      filename: './../index.html',
    }),
  ],
};

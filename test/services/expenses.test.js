const app = require('../../src/backend/app');

describe('\'expenses\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/expenses');
    expect(service).toBeTruthy();
  });
});

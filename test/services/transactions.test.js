const app = require('../../src/backend/app');

describe('\'transactions\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/transactions');
    expect(service).toBeTruthy();
  });
});

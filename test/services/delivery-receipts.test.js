const app = require('../../src/backend/app');

describe('\'deliveryReceipts\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/deliveryReceipts');
    expect(service).toBeTruthy();
  });
});

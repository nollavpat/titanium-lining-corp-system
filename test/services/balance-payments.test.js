const app = require('../../src/backend/app');

describe('\'balancePayments\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/balancePayments');
    expect(service).toBeTruthy();
  });
});

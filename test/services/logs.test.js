const app = require('../../src/backend/app');

describe('\'logs\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/logs');
    expect(service).toBeTruthy();
  });
});

const app = require('../../src/backend/app');

describe('\'payments\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/payments');
    expect(service).toBeTruthy();
  });
});

const app = require('../../src/backend/app');

describe('\'expenseNotifications\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/expenseNotifications');
    expect(service).toBeTruthy();
  });
});

const app = require('../../src/backend/app');

describe('\'constants\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/constants');
    expect(service).toBeTruthy();
  });
});

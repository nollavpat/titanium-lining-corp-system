const app = require('../../src/backend/app');

describe('\'sets\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/sets');
    expect(service).toBeTruthy();
  });
});

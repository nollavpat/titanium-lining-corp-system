const app = require('../../src/backend/app');

describe('\'balances\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/balances');
    expect(service).toBeTruthy();
  });
});

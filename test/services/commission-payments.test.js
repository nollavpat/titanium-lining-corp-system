const app = require('../../src/backend/app');

describe('\'commissionPayments\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/commissionPayments');
    expect(service).toBeTruthy();
  });
});

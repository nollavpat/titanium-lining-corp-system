const app = require('../../src/backend/app');

describe('\'commissions\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/commissions');
    expect(service).toBeTruthy();
  });
});

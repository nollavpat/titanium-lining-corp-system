const app = require('../../src/backend/app');

describe('\'profiles\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/profiles');
    expect(service).toBeTruthy();
  });
});
